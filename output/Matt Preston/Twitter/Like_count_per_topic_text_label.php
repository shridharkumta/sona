<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 256),
		array("label" => "Business", "y" => 3826),
		array("label" => "Fashion", "y" => 3567),
		array("label" => "Food", "y" => 17723),
		array("label" => "Gaming", "y" => 3080),
		array("label" => "Medicine", "y" => 1523),
		array("label" => "Movie", "y" => 3833),
		array("label" => "Music", "y" => 3402),
		array("label" => "Politics", "y" => 1251),
		array("label" => "Space", "y" => 1424),
		array("label" => "Sports", "y" => 6305),
		array("label" => "Technology", "y" => 551),
		array("label" => "Travel", "y" => 3459),
		array("label" => "Vehicles", "y" => 1241)
	);
?>
<?php 
	$positive = array(
		array("label" => "Food", "y" => 1175),
		array("label" => "Movie", "y" => 259),
		array("label" => "Travel", "y" => 135),
		array("label" => "Business", "y" => 160),
		array("label" => "Sports", "y" => 201),
		array("label" => "Politics", "y" => 72),
		array("label" => "Music", "y" => 173),
		array("label" => "Medicine", "y" => 45),
		array("label" => "Fashion", "y" => 147),
		array("label" => "Vehicles", "y" => 31),
		array("label" => "Gaming", "y" => 128),
		array("label" => "Space", "y" => 60),
		array("label" => "Books", "y" => 30),
		array("label" => "Technology", "y" => 47)
	);
	$negative = array(
		array("label" => "Food", "y" => 144),
		array("label" => "Movie", "y" => 33),
		array("label" => "Travel", "y" => 82),
		array("label" => "Business", "y" => 41),
		array("label" => "Sports", "y" => 78),
		array("label" => "Politics", "y" => 24),
		array("label" => "Music", "y" => 28),
		array("label" => "Medicine", "y" => 27),
		array("label" => "Fashion", "y" => 41),
		array("label" => "Vehicles", "y" => 11),
		array("label" => "Gaming", "y" => 26),
		array("label" => "Space", "y" => 18),
		array("label" => "Books", "y" => 2),
		array("label" => "Technology", "y" => 11)
	);
	$neutral = array(
		array("label" => "Food", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Technology", "y" => 0)
	);

?>
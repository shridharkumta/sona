<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 32),
		array("label" => "Business", "y" => 201),
		array("label" => "Fashion", "y" => 188),
		array("label" => "Food", "y" => 1319),
		array("label" => "Gaming", "y" => 154),
		array("label" => "Medicine", "y" => 72),
		array("label" => "Movie", "y" => 292),
		array("label" => "Music", "y" => 201),
		array("label" => "Politics", "y" => 96),
		array("label" => "Space", "y" => 78),
		array("label" => "Sports", "y" => 279),
		array("label" => "Technology", "y" => 58),
		array("label" => "Travel", "y" => 217),
		array("label" => "Vehicles", "y" => 42)
	);
?>
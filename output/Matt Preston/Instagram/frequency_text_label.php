<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 15),
		array("label" => "Business", "y" => 103),
		array("label" => "Fashion", "y" => 104),
		array("label" => "Food", "y" => 2106),
		array("label" => "Gaming", "y" => 95),
		array("label" => "Medicine", "y" => 30),
		array("label" => "Movie", "y" => 258),
		array("label" => "Music", "y" => 91),
		array("label" => "Others", "y" => 8),
		array("label" => "Politics", "y" => 38),
		array("label" => "Space", "y" => 48),
		array("label" => "Sports", "y" => 206),
		array("label" => "Technology", "y" => 12),
		array("label" => "Travel", "y" => 182),
		array("label" => "Vehicles", "y" => 24)
	);
?>
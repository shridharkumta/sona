<?php 
	$positive = array(
		array("label" => "Food", "y" => 1957),
		array("label" => "Technology", "y" => 9),
		array("label" => "Movie", "y" => 226),
		array("label" => "Gaming", "y" => 76),
		array("label" => "Music", "y" => 77),
		array("label" => "Business", "y" => 92),
		array("label" => "Politics", "y" => 24),
		array("label" => "Sports", "y" => 150),
		array("label" => "Travel", "y" => 120),
		array("label" => "Books", "y" => 11),
		array("label" => "Medicine", "y" => 15),
		array("label" => "Fashion", "y" => 84),
		array("label" => "Space", "y" => 37),
		array("label" => "Vehicles", "y" => 15),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Food", "y" => 149),
		array("label" => "Technology", "y" => 3),
		array("label" => "Movie", "y" => 32),
		array("label" => "Gaming", "y" => 19),
		array("label" => "Music", "y" => 14),
		array("label" => "Business", "y" => 11),
		array("label" => "Politics", "y" => 14),
		array("label" => "Sports", "y" => 56),
		array("label" => "Travel", "y" => 62),
		array("label" => "Books", "y" => 4),
		array("label" => "Medicine", "y" => 15),
		array("label" => "Fashion", "y" => 20),
		array("label" => "Space", "y" => 11),
		array("label" => "Vehicles", "y" => 9),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Food", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Others", "y" => 8)
	);

?>
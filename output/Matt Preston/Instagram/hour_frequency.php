<?php 
	$hour_frequency = array(
		array("label" => "0:00 - 1:00", "y" => 178),
		array("label" => "1:00 - 2:00", "y" => 139),
		array("label" => "2:00 - 3:00", "y" => 129),
		array("label" => "3:00 - 4:00", "y" => 115),
		array("label" => "4:00 - 5:00", "y" => 121),
		array("label" => "5:00 - 6:00", "y" => 129),
		array("label" => "6:00 - 7:00", "y" => 156),
		array("label" => "7:00 - 8:00", "y" => 227),
		array("label" => "8:00 - 9:00", "y" => 299),
		array("label" => "9:00 - 10:00", "y" => 342),
		array("label" => "10:00 - 11:00", "y" => 336),
		array("label" => "11:00 - 12:00", "y" => 221),
		array("label" => "12:00 - 13:00", "y" => 125),
		array("label" => "13:00 - 14:00", "y" => 92),
		array("label" => "14:00 - 15:00", "y" => 42),
		array("label" => "15:00 - 16:00", "y" => 43),
		array("label" => "16:00 - 17:00", "y" => 25),
		array("label" => "17:00 - 18:00", "y" => 15),
		array("label" => "18:00 - 19:00", "y" => 9),
		array("label" => "19:00 - 20:00", "y" => 23),
		array("label" => "20:00 - 21:00", "y" => 69),
		array("label" => "21:00 - 22:00", "y" => 126),
		array("label" => "22:00 - 23:00", "y" => 185),
		array("label" => "23:00 - 00:00", "y" => 174)
	);
?>
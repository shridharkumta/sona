<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 37202),
		array("label" => "Business", "y" => 214858),
		array("label" => "Fashion", "y" => 233817),
		array("label" => "Food", "y" => 3840409),
		array("label" => "Gaming", "y" => 167077),
		array("label" => "Medicine", "y" => 56171),
		array("label" => "Movie", "y" => 616328),
		array("label" => "Music", "y" => 217379),
		array("label" => "Others", "y" => 10726),
		array("label" => "Politics", "y" => 87266),
		array("label" => "Space", "y" => 135636),
		array("label" => "Sports", "y" => 404816),
		array("label" => "Technology", "y" => 18653),
		array("label" => "Travel", "y" => 386544),
		array("label" => "Vehicles", "y" => 42013)
	);
?>
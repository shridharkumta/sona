<?php 
	$likeCountPerTopic = array(
		array("label" => "Movie", "y" => 620161),
		array("label" => "Space", "y" => 137060),
		array("label" => "Medicine", "y" => 57694),
		array("label" => "Travel", "y" => 390003),
		array("label" => "Music", "y" => 220781),
		array("label" => "Business", "y" => 218684),
		array("label" => "Gaming", "y" => 170157),
		array("label" => "Sports", "y" => 411121),
		array("label" => "Vehicles", "y" => 43254),
		array("label" => "Politics", "y" => 88517),
		array("label" => "Food", "y" => 3858132),
		array("label" => "Fashion", "y" => 237384),
		array("label" => "Technology", "y" => 19204),
		array("label" => "Books", "y" => 37458),
		array("label" => "Others", "y" => 10726)
	);
?>
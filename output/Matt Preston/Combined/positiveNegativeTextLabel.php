<?php 
	$positive = array(
		array("label" => "Movie", "y" => 485),
		array("label" => "Space", "y" => 97),
		array("label" => "Medicine", "y" => 60),
		array("label" => "Travel", "y" => 255),
		array("label" => "Music", "y" => 250),
		array("label" => "Business", "y" => 252),
		array("label" => "Gaming", "y" => 204),
		array("label" => "Sports", "y" => 351),
		array("label" => "Vehicles", "y" => 46),
		array("label" => "Politics", "y" => 96),
		array("label" => "Food", "y" => 3132),
		array("label" => "Technology", "y" => 56),
		array("label" => "Fashion", "y" => 231),
		array("label" => "Books", "y" => 41),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Movie", "y" => 65),
		array("label" => "Space", "y" => 29),
		array("label" => "Medicine", "y" => 42),
		array("label" => "Travel", "y" => 144),
		array("label" => "Music", "y" => 42),
		array("label" => "Business", "y" => 52),
		array("label" => "Gaming", "y" => 45),
		array("label" => "Sports", "y" => 134),
		array("label" => "Vehicles", "y" => 20),
		array("label" => "Politics", "y" => 38),
		array("label" => "Food", "y" => 293),
		array("label" => "Technology", "y" => 14),
		array("label" => "Fashion", "y" => 61),
		array("label" => "Books", "y" => 6),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 8)
	);

?>
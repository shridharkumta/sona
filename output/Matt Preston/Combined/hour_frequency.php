<?php 
	$hour_frequency = array(
		array("label" => "0:00 - 1:00", "y" => 342),
		array("label" => "1:00 - 2:00", "y" => 264),
		array("label" => "2:00 - 3:00", "y" => 255),
		array("label" => "3:00 - 4:00", "y" => 211),
		array("label" => "4:00 - 5:00", "y" => 227),
		array("label" => "5:00 - 6:00", "y" => 225),
		array("label" => "6:00 - 7:00", "y" => 256),
		array("label" => "7:00 - 8:00", "y" => 367),
		array("label" => "8:00 - 9:00", "y" => 468),
		array("label" => "9:00 - 10:00", "y" => 587),
		array("label" => "10:00 - 11:00", "y" => 675),
		array("label" => "11:00 - 12:00", "y" => 417),
		array("label" => "12:00 - 13:00", "y" => 285),
		array("label" => "13:00 - 14:00", "y" => 256),
		array("label" => "14:00 - 15:00", "y" => 168),
		array("label" => "15:00 - 16:00", "y" => 112),
		array("label" => "16:00 - 17:00", "y" => 66),
		array("label" => "17:00 - 18:00", "y" => 50),
		array("label" => "18:00 - 19:00", "y" => 34),
		array("label" => "19:00 - 20:00", "y" => 55),
		array("label" => "20:00 - 21:00", "y" => 161),
		array("label" => "21:00 - 22:00", "y" => 290),
		array("label" => "22:00 - 23:00", "y" => 445),
		array("label" => "23:00 - 00:00", "y" => 333)
	);
?>
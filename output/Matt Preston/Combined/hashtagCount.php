<?php 
	$hashtagCount = array(
		array("label" => "#masterchefau", "y" => 436),
		array("label" => "#repost", "y" => 142),
		array("label" => "#masterchef", "y" => 113),
		array("label" => "#mattpreston", "y" => 72),
		array("label" => "#makeitdelicious", "y" => 71),
		array("label" => "#yummyeasyquick", "y" => 55),
		array("label" => "#deliciousonsunday", "y" => 45),
		array("label" => "#instafood", "y" => 44),
		array("label" => "#lockdownkitchenau", "y" => 44),
		array("label" => "#aroundtheworld", "y" => 42),
	);
?>
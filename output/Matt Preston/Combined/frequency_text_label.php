<?php 
	$frequency_label = array(
		array("label" => "Food", "y" => 3425),
		array("label" => "Movie", "y" => 550),
		array("label" => "Sports", "y" => 485),
		array("label" => "Travel", "y" => 399),
		array("label" => "Business", "y" => 304),
		array("label" => "Music", "y" => 292),
		array("label" => "Fashion", "y" => 292),
		array("label" => "Gaming", "y" => 249),
		array("label" => "Politics", "y" => 134),
		array("label" => "Space", "y" => 126),
		array("label" => "Medicine", "y" => 102),
		array("label" => "Technology", "y" => 70),
		array("label" => "Vehicles", "y" => 66),
		array("label" => "Books", "y" => 47),
		array("label" => "Others", "y" => 8),
	);
?>
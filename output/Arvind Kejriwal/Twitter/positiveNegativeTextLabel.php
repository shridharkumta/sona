<?php 
	$positive = array(
		array("label" => "Politics", "y" => 697),
		array("label" => "Travel", "y" => 51),
		array("label" => "Music", "y" => 56),
		array("label" => "Medicine", "y" => 67),
		array("label" => "Business", "y" => 74),
		array("label" => "Space", "y" => 16),
		array("label" => "Technology", "y" => 49),
		array("label" => "Movie", "y" => 58),
		array("label" => "Others", "y" => 0),
		array("label" => "Sports", "y" => 43),
		array("label" => "Food", "y" => 39),
		array("label" => "Gaming", "y" => 11),
		array("label" => "Fashion", "y" => 6),
		array("label" => "Vehicles", "y" => 5),
		array("label" => "Books", "y" => 3)
	);
	$negative = array(
		array("label" => "Politics", "y" => 863),
		array("label" => "Travel", "y" => 796),
		array("label" => "Music", "y" => 16),
		array("label" => "Medicine", "y" => 71),
		array("label" => "Business", "y" => 34),
		array("label" => "Space", "y" => 21),
		array("label" => "Technology", "y" => 9),
		array("label" => "Movie", "y" => 11),
		array("label" => "Others", "y" => 0),
		array("label" => "Sports", "y" => 38),
		array("label" => "Food", "y" => 17),
		array("label" => "Gaming", "y" => 22),
		array("label" => "Fashion", "y" => 9),
		array("label" => "Vehicles", "y" => 53),
		array("label" => "Books", "y" => 3)
	);
	$neutral = array(
		array("label" => "Politics", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Others", "y" => 124),
		array("label" => "Sports", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Books", "y" => 0)
	);

?>
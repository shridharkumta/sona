<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 39019),
		array("label" => "Business", "y" => 440837),
		array("label" => "Fashion", "y" => 95201),
		array("label" => "Food", "y" => 318623),
		array("label" => "Gaming", "y" => 143622),
		array("label" => "Medicine", "y" => 685386),
		array("label" => "Movie", "y" => 242573),
		array("label" => "Music", "y" => 295633),
		array("label" => "Others", "y" => 587956),
		array("label" => "Politics", "y" => 5638009),
		array("label" => "Space", "y" => 150374),
		array("label" => "Sports", "y" => 308035),
		array("label" => "Technology", "y" => 138713),
		array("label" => "Travel", "y" => 3492390),
		array("label" => "Vehicles", "y" => 242574)
	);
?>
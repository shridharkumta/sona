<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 6),
		array("label" => "Business", "y" => 108),
		array("label" => "Fashion", "y" => 15),
		array("label" => "Food", "y" => 56),
		array("label" => "Gaming", "y" => 33),
		array("label" => "Medicine", "y" => 138),
		array("label" => "Movie", "y" => 69),
		array("label" => "Music", "y" => 72),
		array("label" => "Others", "y" => 124),
		array("label" => "Politics", "y" => 1560),
		array("label" => "Space", "y" => 37),
		array("label" => "Sports", "y" => 81),
		array("label" => "Technology", "y" => 58),
		array("label" => "Travel", "y" => 847),
		array("label" => "Vehicles", "y" => 58)
	);
?>
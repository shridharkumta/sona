<?php 
	$frequency_label = array(
		array("label" => "Politics", "y" => 1760),
		array("label" => "Travel", "y" => 871),
		array("label" => "Others", "y" => 233),
		array("label" => "Medicine", "y" => 161),
		array("label" => "Business", "y" => 117),
		array("label" => "Sports", "y" => 91),
		array("label" => "Movie", "y" => 81),
		array("label" => "Music", "y" => 76),
		array("label" => "Vehicles", "y" => 72),
		array("label" => "Food", "y" => 62),
		array("label" => "Technology", "y" => 61),
		array("label" => "Space", "y" => 39),
		array("label" => "Gaming", "y" => 38),
		array("label" => "Fashion", "y" => 16),
		array("label" => "Books", "y" => 6),
	);
?>
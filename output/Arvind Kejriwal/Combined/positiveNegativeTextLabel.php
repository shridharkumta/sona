<?php 
	$positive = array(
		array("label" => "Movie", "y" => 66),
		array("label" => "Space", "y" => 18),
		array("label" => "Medicine", "y" => 79),
		array("label" => "Travel", "y" => 55),
		array("label" => "Music", "y" => 59),
		array("label" => "Business", "y" => 82),
		array("label" => "Gaming", "y" => 14),
		array("label" => "Sports", "y" => 46),
		array("label" => "Vehicles", "y" => 7),
		array("label" => "Politics", "y" => 790),
		array("label" => "Food", "y" => 43),
		array("label" => "Technology", "y" => 51),
		array("label" => "Fashion", "y" => 6),
		array("label" => "Books", "y" => 3),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Movie", "y" => 15),
		array("label" => "Space", "y" => 21),
		array("label" => "Medicine", "y" => 82),
		array("label" => "Travel", "y" => 816),
		array("label" => "Music", "y" => 17),
		array("label" => "Business", "y" => 35),
		array("label" => "Gaming", "y" => 24),
		array("label" => "Sports", "y" => 45),
		array("label" => "Vehicles", "y" => 65),
		array("label" => "Politics", "y" => 970),
		array("label" => "Food", "y" => 19),
		array("label" => "Technology", "y" => 10),
		array("label" => "Fashion", "y" => 10),
		array("label" => "Books", "y" => 3),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 233)
	);

?>
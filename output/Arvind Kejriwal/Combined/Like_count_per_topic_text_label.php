<?php 
	$likeCountPerTopic = array(
		array("label" => "Movie", "y" => 454165),
		array("label" => "Space", "y" => 170750),
		array("label" => "Medicine", "y" => 1142546),
		array("label" => "Travel", "y" => 3872969),
		array("label" => "Music", "y" => 343635),
		array("label" => "Business", "y" => 552144),
		array("label" => "Gaming", "y" => 254890),
		array("label" => "Sports", "y" => 409802),
		array("label" => "Vehicles", "y" => 474781),
		array("label" => "Politics", "y" => 8397980),
		array("label" => "Food", "y" => 380710),
		array("label" => "Fashion", "y" => 103694),
		array("label" => "Technology", "y" => 244975),
		array("label" => "Books", "y" => 39019),
		array("label" => "Others", "y" => 2788347)
	);
?>
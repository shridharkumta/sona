<?php 
	$positive = array(
		array("label" => "Medicine", "y" => 12),
		array("label" => "Politics", "y" => 93),
		array("label" => "Business", "y" => 8),
		array("label" => "Space", "y" => 2),
		array("label" => "Travel", "y" => 4),
		array("label" => "Others", "y" => 0),
		array("label" => "Sports", "y" => 3),
		array("label" => "Music", "y" => 3),
		array("label" => "Gaming", "y" => 3),
		array("label" => "Vehicles", "y" => 2),
		array("label" => "Technology", "y" => 2),
		array("label" => "Movie", "y" => 8),
		array("label" => "Food", "y" => 4),
		array("label" => "Fashion", "y" => 0)
	);
	$negative = array(
		array("label" => "Medicine", "y" => 11),
		array("label" => "Politics", "y" => 107),
		array("label" => "Business", "y" => 1),
		array("label" => "Space", "y" => 0),
		array("label" => "Travel", "y" => 20),
		array("label" => "Others", "y" => 0),
		array("label" => "Sports", "y" => 7),
		array("label" => "Music", "y" => 1),
		array("label" => "Gaming", "y" => 2),
		array("label" => "Vehicles", "y" => 12),
		array("label" => "Technology", "y" => 1),
		array("label" => "Movie", "y" => 4),
		array("label" => "Food", "y" => 2),
		array("label" => "Fashion", "y" => 1)
	);
	$neutral = array(
		array("label" => "Medicine", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Others", "y" => 109),
		array("label" => "Sports", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Fashion", "y" => 0)
	);

?>
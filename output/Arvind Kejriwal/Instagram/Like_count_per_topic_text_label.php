<?php 
	$likeCountPerTopic = array(
		array("label" => "Business", "y" => 111307),
		array("label" => "Fashion", "y" => 8493),
		array("label" => "Food", "y" => 62087),
		array("label" => "Gaming", "y" => 111268),
		array("label" => "Medicine", "y" => 457160),
		array("label" => "Movie", "y" => 211592),
		array("label" => "Music", "y" => 48002),
		array("label" => "Others", "y" => 2200391),
		array("label" => "Politics", "y" => 2759971),
		array("label" => "Space", "y" => 20376),
		array("label" => "Sports", "y" => 101767),
		array("label" => "Technology", "y" => 106262),
		array("label" => "Travel", "y" => 380579),
		array("label" => "Vehicles", "y" => 232207)
	);
?>
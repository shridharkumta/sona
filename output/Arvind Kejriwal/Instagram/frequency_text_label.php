<?php 
	$frequency_label = array(
		array("label" => "Business", "y" => 9),
		array("label" => "Fashion", "y" => 1),
		array("label" => "Food", "y" => 6),
		array("label" => "Gaming", "y" => 5),
		array("label" => "Medicine", "y" => 23),
		array("label" => "Movie", "y" => 12),
		array("label" => "Music", "y" => 4),
		array("label" => "Others", "y" => 109),
		array("label" => "Politics", "y" => 200),
		array("label" => "Space", "y" => 2),
		array("label" => "Sports", "y" => 10),
		array("label" => "Technology", "y" => 3),
		array("label" => "Travel", "y" => 24),
		array("label" => "Vehicles", "y" => 14)
	);
?>
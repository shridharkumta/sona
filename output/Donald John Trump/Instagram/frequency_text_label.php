<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 3),
		array("label" => "Business", "y" => 552),
		array("label" => "Fashion", "y" => 41),
		array("label" => "Food", "y" => 143),
		array("label" => "Gaming", "y" => 44),
		array("label" => "Medicine", "y" => 387),
		array("label" => "Movie", "y" => 491),
		array("label" => "Music", "y" => 108),
		array("label" => "Others", "y" => 162),
		array("label" => "Politics", "y" => 3052),
		array("label" => "Space", "y" => 115),
		array("label" => "Sports", "y" => 252),
		array("label" => "Technology", "y" => 34),
		array("label" => "Travel", "y" => 265),
		array("label" => "Vehicles", "y" => 33)
	);
?>
<?php 
	$hour_frequency = array(
		array("label" => "0:00 - 1:00", "y" => 335),
		array("label" => "1:00 - 2:00", "y" => 385),
		array("label" => "2:00 - 3:00", "y" => 365),
		array("label" => "3:00 - 4:00", "y" => 290),
		array("label" => "4:00 - 5:00", "y" => 193),
		array("label" => "5:00 - 6:00", "y" => 89),
		array("label" => "6:00 - 7:00", "y" => 36),
		array("label" => "7:00 - 8:00", "y" => 17),
		array("label" => "8:00 - 9:00", "y" => 26),
		array("label" => "9:00 - 10:00", "y" => 11),
		array("label" => "10:00 - 11:00", "y" => 18),
		array("label" => "11:00 - 12:00", "y" => 28),
		array("label" => "12:00 - 13:00", "y" => 65),
		array("label" => "13:00 - 14:00", "y" => 93),
		array("label" => "14:00 - 15:00", "y" => 173),
		array("label" => "15:00 - 16:00", "y" => 233),
		array("label" => "16:00 - 17:00", "y" => 365),
		array("label" => "17:00 - 18:00", "y" => 356),
		array("label" => "18:00 - 19:00", "y" => 419),
		array("label" => "19:00 - 20:00", "y" => 453),
		array("label" => "20:00 - 21:00", "y" => 422),
		array("label" => "21:00 - 22:00", "y" => 495),
		array("label" => "22:00 - 23:00", "y" => 469),
		array("label" => "23:00 - 00:00", "y" => 346)
	);
?>
<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 611174),
		array("label" => "Business", "y" => 75829720),
		array("label" => "Fashion", "y" => 10142278),
		array("label" => "Food", "y" => 22616199),
		array("label" => "Gaming", "y" => 8427330),
		array("label" => "Medicine", "y" => 33254560),
		array("label" => "Movie", "y" => 82544703),
		array("label" => "Music", "y" => 19777145),
		array("label" => "Others", "y" => 74760571),
		array("label" => "Politics", "y" => 438675301),
		array("label" => "Space", "y" => 15521002),
		array("label" => "Sports", "y" => 37349182),
		array("label" => "Technology", "y" => 8155445),
		array("label" => "Travel", "y" => 42729359),
		array("label" => "Vehicles", "y" => 6670697)
	);
?>
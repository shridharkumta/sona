<?php 
	$positive = array(
		array("label" => "Others", "y" => 0),
		array("label" => "Business", "y" => 446),
		array("label" => "Space", "y" => 65),
		array("label" => "Politics", "y" => 2247),
		array("label" => "Movie", "y" => 434),
		array("label" => "Travel", "y" => 143),
		array("label" => "Medicine", "y" => 294),
		array("label" => "Sports", "y" => 219),
		array("label" => "Music", "y" => 91),
		array("label" => "Food", "y" => 133),
		array("label" => "Fashion", "y" => 32),
		array("label" => "Gaming", "y" => 30),
		array("label" => "Technology", "y" => 17),
		array("label" => "Vehicles", "y" => 24),
		array("label" => "Books", "y" => 2)
	);
	$negative = array(
		array("label" => "Others", "y" => 0),
		array("label" => "Business", "y" => 106),
		array("label" => "Space", "y" => 50),
		array("label" => "Politics", "y" => 805),
		array("label" => "Movie", "y" => 57),
		array("label" => "Travel", "y" => 122),
		array("label" => "Medicine", "y" => 93),
		array("label" => "Sports", "y" => 33),
		array("label" => "Music", "y" => 17),
		array("label" => "Food", "y" => 10),
		array("label" => "Fashion", "y" => 9),
		array("label" => "Gaming", "y" => 14),
		array("label" => "Technology", "y" => 17),
		array("label" => "Vehicles", "y" => 9),
		array("label" => "Books", "y" => 1)
	);
	$neutral = array(
		array("label" => "Others", "y" => 162),
		array("label" => "Business", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Books", "y" => 0)
	);

?>
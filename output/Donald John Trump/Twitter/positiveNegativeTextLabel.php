<?php 
	$positive = array(
		array("label" => "Politics", "y" => 1021),
		array("label" => "Business", "y" => 303),
		array("label" => "Medicine", "y" => 77),
		array("label" => "Movie", "y" => 109),
		array("label" => "Travel", "y" => 25),
		array("label" => "Space", "y" => 47),
		array("label" => "Fashion", "y" => 6),
		array("label" => "Music", "y" => 65),
		array("label" => "Sports", "y" => 92),
		array("label" => "Vehicles", "y" => 9),
		array("label" => "Books", "y" => 11),
		array("label" => "Food", "y" => 48),
		array("label" => "Gaming", "y" => 21),
		array("label" => "Technology", "y" => 17)
	);
	$negative = array(
		array("label" => "Politics", "y" => 793),
		array("label" => "Business", "y" => 190),
		array("label" => "Medicine", "y" => 58),
		array("label" => "Movie", "y" => 42),
		array("label" => "Travel", "y" => 202),
		array("label" => "Space", "y" => 22),
		array("label" => "Fashion", "y" => 4),
		array("label" => "Music", "y" => 20),
		array("label" => "Sports", "y" => 43),
		array("label" => "Vehicles", "y" => 8),
		array("label" => "Books", "y" => 2),
		array("label" => "Food", "y" => 11),
		array("label" => "Gaming", "y" => 18),
		array("label" => "Technology", "y" => 4)
	);
	$neutral = array(
		array("label" => "Politics", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Technology", "y" => 0)
	);

?>
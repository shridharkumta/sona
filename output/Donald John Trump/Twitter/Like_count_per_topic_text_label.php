<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 917445),
		array("label" => "Business", "y" => 29838091),
		array("label" => "Fashion", "y" => 914297),
		array("label" => "Food", "y" => 3359570),
		array("label" => "Gaming", "y" => 1970887),
		array("label" => "Medicine", "y" => 6489484),
		array("label" => "Movie", "y" => 10533675),
		array("label" => "Music", "y" => 4509696),
		array("label" => "Politics", "y" => 118895308),
		array("label" => "Space", "y" => 3363294),
		array("label" => "Sports", "y" => 7458954),
		array("label" => "Technology", "y" => 1597869),
		array("label" => "Travel", "y" => 13397624),
		array("label" => "Vehicles", "y" => 1389330)
	);
?>
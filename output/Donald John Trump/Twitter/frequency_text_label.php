<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 13),
		array("label" => "Business", "y" => 493),
		array("label" => "Fashion", "y" => 10),
		array("label" => "Food", "y" => 59),
		array("label" => "Gaming", "y" => 39),
		array("label" => "Medicine", "y" => 135),
		array("label" => "Movie", "y" => 151),
		array("label" => "Music", "y" => 85),
		array("label" => "Politics", "y" => 1814),
		array("label" => "Space", "y" => 69),
		array("label" => "Sports", "y" => 135),
		array("label" => "Technology", "y" => 21),
		array("label" => "Travel", "y" => 227),
		array("label" => "Vehicles", "y" => 17)
	);
?>
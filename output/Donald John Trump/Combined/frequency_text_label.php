<?php 
	$frequency_label = array(
		array("label" => "Politics", "y" => 4866),
		array("label" => "Business", "y" => 1045),
		array("label" => "Movie", "y" => 642),
		array("label" => "Medicine", "y" => 522),
		array("label" => "Travel", "y" => 492),
		array("label" => "Sports", "y" => 387),
		array("label" => "Food", "y" => 202),
		array("label" => "Music", "y" => 193),
		array("label" => "Space", "y" => 184),
		array("label" => "Others", "y" => 162),
		array("label" => "Gaming", "y" => 83),
		array("label" => "Technology", "y" => 55),
		array("label" => "Fashion", "y" => 51),
		array("label" => "Vehicles", "y" => 50),
		array("label" => "Books", "y" => 16),
	);
?>
<?php 
	$likeCountPerTopic = array(
		array("label" => "Movie", "y" => 93078378),
		array("label" => "Space", "y" => 18884296),
		array("label" => "Medicine", "y" => 39744044),
		array("label" => "Travel", "y" => 56126983),
		array("label" => "Music", "y" => 24286841),
		array("label" => "Business", "y" => 105667811),
		array("label" => "Gaming", "y" => 10398217),
		array("label" => "Sports", "y" => 44808136),
		array("label" => "Vehicles", "y" => 8060027),
		array("label" => "Politics", "y" => 557570609),
		array("label" => "Food", "y" => 25975769),
		array("label" => "Fashion", "y" => 11056575),
		array("label" => "Technology", "y" => 9753314),
		array("label" => "Books", "y" => 1528619),
		array("label" => "Others", "y" => 74760571)
	);
?>
<?php 
	$positive = array(
		array("label" => "Movie", "y" => 543),
		array("label" => "Space", "y" => 112),
		array("label" => "Medicine", "y" => 371),
		array("label" => "Travel", "y" => 168),
		array("label" => "Music", "y" => 156),
		array("label" => "Business", "y" => 749),
		array("label" => "Gaming", "y" => 51),
		array("label" => "Sports", "y" => 311),
		array("label" => "Vehicles", "y" => 33),
		array("label" => "Politics", "y" => 3268),
		array("label" => "Food", "y" => 181),
		array("label" => "Fashion", "y" => 38),
		array("label" => "Technology", "y" => 34),
		array("label" => "Books", "y" => 13),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Movie", "y" => 99),
		array("label" => "Space", "y" => 72),
		array("label" => "Medicine", "y" => 151),
		array("label" => "Travel", "y" => 324),
		array("label" => "Music", "y" => 37),
		array("label" => "Business", "y" => 296),
		array("label" => "Gaming", "y" => 32),
		array("label" => "Sports", "y" => 76),
		array("label" => "Vehicles", "y" => 17),
		array("label" => "Politics", "y" => 1598),
		array("label" => "Food", "y" => 21),
		array("label" => "Fashion", "y" => 13),
		array("label" => "Technology", "y" => 21),
		array("label" => "Books", "y" => 3),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 162)
	);

?>
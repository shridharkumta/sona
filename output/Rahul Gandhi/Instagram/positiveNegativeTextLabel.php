<?php 
	$positive = array(
		array("label" => "Business", "y" => 13),
		array("label" => "Others", "y" => 0),
		array("label" => "Politics", "y" => 108),
		array("label" => "Movie", "y" => 9),
		array("label" => "Sports", "y" => 6),
		array("label" => "Travel", "y" => 20),
		array("label" => "Gaming", "y" => 2),
		array("label" => "Fashion", "y" => 1),
		array("label" => "Medicine", "y" => 6),
		array("label" => "Space", "y" => 5),
		array("label" => "Food", "y" => 7),
		array("label" => "Vehicles", "y" => 1),
		array("label" => "Music", "y" => 1),
		array("label" => "Technology", "y" => 3)
	);
	$negative = array(
		array("label" => "Business", "y" => 1),
		array("label" => "Others", "y" => 0),
		array("label" => "Politics", "y" => 34),
		array("label" => "Movie", "y" => 3),
		array("label" => "Sports", "y" => 2),
		array("label" => "Travel", "y" => 8),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Medicine", "y" => 2),
		array("label" => "Space", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Vehicles", "y" => 3),
		array("label" => "Music", "y" => 0),
		array("label" => "Technology", "y" => 0)
	);
	$neutral = array(
		array("label" => "Business", "y" => 0),
		array("label" => "Others", "y" => 23),
		array("label" => "Politics", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Technology", "y" => 0)
	);

?>
<?php 
	$likeCountPerTopic = array(
		array("label" => "Business", "y" => 609772),
		array("label" => "Fashion", "y" => 72922),
		array("label" => "Food", "y" => 116897),
		array("label" => "Gaming", "y" => 125096),
		array("label" => "Medicine", "y" => 120858),
		array("label" => "Movie", "y" => 491301),
		array("label" => "Music", "y" => 15630),
		array("label" => "Others", "y" => 642996),
		array("label" => "Politics", "y" => 6717553),
		array("label" => "Space", "y" => 47536),
		array("label" => "Sports", "y" => 456110),
		array("label" => "Technology", "y" => 27400),
		array("label" => "Travel", "y" => 1054639),
		array("label" => "Vehicles", "y" => 72417)
	);
?>
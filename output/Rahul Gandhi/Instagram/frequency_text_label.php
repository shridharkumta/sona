<?php 
	$frequency_label = array(
		array("label" => "Business", "y" => 14),
		array("label" => "Fashion", "y" => 1),
		array("label" => "Food", "y" => 7),
		array("label" => "Gaming", "y" => 2),
		array("label" => "Medicine", "y" => 8),
		array("label" => "Movie", "y" => 12),
		array("label" => "Music", "y" => 1),
		array("label" => "Others", "y" => 23),
		array("label" => "Politics", "y" => 142),
		array("label" => "Space", "y" => 5),
		array("label" => "Sports", "y" => 8),
		array("label" => "Technology", "y" => 3),
		array("label" => "Travel", "y" => 28),
		array("label" => "Vehicles", "y" => 4)
	);
?>
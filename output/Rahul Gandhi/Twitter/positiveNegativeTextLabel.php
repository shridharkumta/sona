<?php 
	$positive = array(
		array("label" => "Politics", "y" => 999),
		array("label" => "Travel", "y" => 31),
		array("label" => "Business", "y" => 83),
		array("label" => "Others", "y" => 0),
		array("label" => "Medicine", "y" => 25),
		array("label" => "Music", "y" => 79),
		array("label" => "Vehicles", "y" => 2),
		array("label" => "Movie", "y" => 78),
		array("label" => "Gaming", "y" => 25),
		array("label" => "Sports", "y" => 72),
		array("label" => "Food", "y" => 49),
		array("label" => "Technology", "y" => 64),
		array("label" => "Fashion", "y" => 3),
		array("label" => "Space", "y" => 21),
		array("label" => "Books", "y" => 4)
	);
	$negative = array(
		array("label" => "Politics", "y" => 835),
		array("label" => "Travel", "y" => 417),
		array("label" => "Business", "y" => 37),
		array("label" => "Others", "y" => 0),
		array("label" => "Medicine", "y" => 36),
		array("label" => "Music", "y" => 24),
		array("label" => "Vehicles", "y" => 16),
		array("label" => "Movie", "y" => 37),
		array("label" => "Gaming", "y" => 10),
		array("label" => "Sports", "y" => 20),
		array("label" => "Food", "y" => 12),
		array("label" => "Technology", "y" => 13),
		array("label" => "Fashion", "y" => 1),
		array("label" => "Space", "y" => 15),
		array("label" => "Books", "y" => 0)
	);
	$neutral = array(
		array("label" => "Politics", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Others", "y" => 224),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Books", "y" => 0)
	);

?>
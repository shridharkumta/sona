<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 36501),
		array("label" => "Business", "y" => 2227289),
		array("label" => "Fashion", "y" => 114187),
		array("label" => "Food", "y" => 1465319),
		array("label" => "Gaming", "y" => 453635),
		array("label" => "Medicine", "y" => 1311771),
		array("label" => "Movie", "y" => 1710102),
		array("label" => "Music", "y" => 1986573),
		array("label" => "Others", "y" => 2356066),
		array("label" => "Politics", "y" => 31190492),
		array("label" => "Space", "y" => 359395),
		array("label" => "Sports", "y" => 1288458),
		array("label" => "Technology", "y" => 544447),
		array("label" => "Travel", "y" => 6903073),
		array("label" => "Vehicles", "y" => 363108)
	);
?>
<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 4),
		array("label" => "Business", "y" => 120),
		array("label" => "Fashion", "y" => 4),
		array("label" => "Food", "y" => 61),
		array("label" => "Gaming", "y" => 35),
		array("label" => "Medicine", "y" => 61),
		array("label" => "Movie", "y" => 115),
		array("label" => "Music", "y" => 103),
		array("label" => "Others", "y" => 224),
		array("label" => "Politics", "y" => 1834),
		array("label" => "Space", "y" => 36),
		array("label" => "Sports", "y" => 92),
		array("label" => "Technology", "y" => 77),
		array("label" => "Travel", "y" => 448),
		array("label" => "Vehicles", "y" => 18)
	);
?>
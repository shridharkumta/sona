<?php 
	$likeCountPerTopic = array(
		array("label" => "Movie", "y" => 2201403),
		array("label" => "Space", "y" => 406931),
		array("label" => "Medicine", "y" => 1432629),
		array("label" => "Travel", "y" => 7957712),
		array("label" => "Music", "y" => 2002203),
		array("label" => "Business", "y" => 2837061),
		array("label" => "Gaming", "y" => 578731),
		array("label" => "Sports", "y" => 1744568),
		array("label" => "Vehicles", "y" => 435525),
		array("label" => "Politics", "y" => 37908045),
		array("label" => "Food", "y" => 1582216),
		array("label" => "Fashion", "y" => 187109),
		array("label" => "Technology", "y" => 571847),
		array("label" => "Books", "y" => 36501),
		array("label" => "Others", "y" => 2999062)
	);
?>
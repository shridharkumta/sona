<?php 
	$frequency_label = array(
		array("label" => "Politics", "y" => 1976),
		array("label" => "Travel", "y" => 476),
		array("label" => "Others", "y" => 247),
		array("label" => "Business", "y" => 134),
		array("label" => "Movie", "y" => 127),
		array("label" => "Music", "y" => 104),
		array("label" => "Sports", "y" => 100),
		array("label" => "Technology", "y" => 80),
		array("label" => "Medicine", "y" => 69),
		array("label" => "Food", "y" => 68),
		array("label" => "Space", "y" => 41),
		array("label" => "Gaming", "y" => 37),
		array("label" => "Vehicles", "y" => 22),
		array("label" => "Fashion", "y" => 5),
		array("label" => "Books", "y" => 4),
	);
?>
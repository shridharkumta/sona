<?php 
	$positive = array(
		array("label" => "Movie", "y" => 87),
		array("label" => "Space", "y" => 26),
		array("label" => "Medicine", "y" => 31),
		array("label" => "Travel", "y" => 51),
		array("label" => "Music", "y" => 80),
		array("label" => "Business", "y" => 96),
		array("label" => "Gaming", "y" => 27),
		array("label" => "Sports", "y" => 78),
		array("label" => "Vehicles", "y" => 3),
		array("label" => "Politics", "y" => 1107),
		array("label" => "Food", "y" => 56),
		array("label" => "Fashion", "y" => 4),
		array("label" => "Technology", "y" => 67),
		array("label" => "Books", "y" => 4),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Movie", "y" => 40),
		array("label" => "Space", "y" => 15),
		array("label" => "Medicine", "y" => 38),
		array("label" => "Travel", "y" => 425),
		array("label" => "Music", "y" => 24),
		array("label" => "Business", "y" => 38),
		array("label" => "Gaming", "y" => 10),
		array("label" => "Sports", "y" => 22),
		array("label" => "Vehicles", "y" => 19),
		array("label" => "Politics", "y" => 869),
		array("label" => "Food", "y" => 12),
		array("label" => "Fashion", "y" => 1),
		array("label" => "Technology", "y" => 13),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 247)
	);

?>
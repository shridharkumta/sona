<?php 
	$positive = array(
		array("label" => "Business", "y" => 114),
		array("label" => "Politics", "y" => 125),
		array("label" => "Medicine", "y" => 47),
		array("label" => "Movie", "y" => 20),
		array("label" => "Technology", "y" => 3),
		array("label" => "Others", "y" => 0),
		array("label" => "Gaming", "y" => 10),
		array("label" => "Music", "y" => 10),
		array("label" => "Space", "y" => 7),
		array("label" => "Sports", "y" => 24),
		array("label" => "Food", "y" => 15),
		array("label" => "Fashion", "y" => 2),
		array("label" => "Travel", "y" => 4),
		array("label" => "Vehicles", "y" => 0)
	);
	$negative = array(
		array("label" => "Business", "y" => 28),
		array("label" => "Politics", "y" => 53),
		array("label" => "Medicine", "y" => 37),
		array("label" => "Movie", "y" => 6),
		array("label" => "Technology", "y" => 1),
		array("label" => "Others", "y" => 0),
		array("label" => "Gaming", "y" => 2),
		array("label" => "Music", "y" => 2),
		array("label" => "Space", "y" => 3),
		array("label" => "Sports", "y" => 8),
		array("label" => "Food", "y" => 4),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Travel", "y" => 2),
		array("label" => "Vehicles", "y" => 1)
	);
	$neutral = array(
		array("label" => "Business", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Others", "y" => 19),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Vehicles", "y" => 0)
	);

?>
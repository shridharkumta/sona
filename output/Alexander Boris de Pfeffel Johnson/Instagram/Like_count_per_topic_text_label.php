<?php 
	$likeCountPerTopic = array(
		array("label" => "Business", "y" => 2352475),
		array("label" => "Fashion", "y" => 77651),
		array("label" => "Food", "y" => 390699),
		array("label" => "Gaming", "y" => 386168),
		array("label" => "Medicine", "y" => 2135550),
		array("label" => "Movie", "y" => 634811),
		array("label" => "Music", "y" => 254522),
		array("label" => "Others", "y" => 433557),
		array("label" => "Politics", "y" => 2801597),
		array("label" => "Space", "y" => 207038),
		array("label" => "Sports", "y" => 521841),
		array("label" => "Technology", "y" => 107505),
		array("label" => "Travel", "y" => 53627),
		array("label" => "Vehicles", "y" => 12726)
	);
?>
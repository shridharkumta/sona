<?php 
	$frequency_label = array(
		array("label" => "Business", "y" => 142),
		array("label" => "Fashion", "y" => 2),
		array("label" => "Food", "y" => 19),
		array("label" => "Gaming", "y" => 12),
		array("label" => "Medicine", "y" => 84),
		array("label" => "Movie", "y" => 26),
		array("label" => "Music", "y" => 12),
		array("label" => "Others", "y" => 19),
		array("label" => "Politics", "y" => 178),
		array("label" => "Space", "y" => 10),
		array("label" => "Sports", "y" => 32),
		array("label" => "Technology", "y" => 4),
		array("label" => "Travel", "y" => 6),
		array("label" => "Vehicles", "y" => 1)
	);
?>
<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 3),
		array("label" => "Business", "y" => 661),
		array("label" => "Fashion", "y" => 8),
		array("label" => "Food", "y" => 69),
		array("label" => "Gaming", "y" => 34),
		array("label" => "Medicine", "y" => 341),
		array("label" => "Movie", "y" => 102),
		array("label" => "Music", "y" => 61),
		array("label" => "Politics", "y" => 1570),
		array("label" => "Space", "y" => 43),
		array("label" => "Sports", "y" => 180),
		array("label" => "Technology", "y" => 12),
		array("label" => "Travel", "y" => 44),
		array("label" => "Vehicles", "y" => 6)
	);
?>
<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 3316),
		array("label" => "Business", "y" => 707498),
		array("label" => "Fashion", "y" => 2618),
		array("label" => "Food", "y" => 218012),
		array("label" => "Gaming", "y" => 34760),
		array("label" => "Medicine", "y" => 258977),
		array("label" => "Movie", "y" => 102110),
		array("label" => "Music", "y" => 53033),
		array("label" => "Politics", "y" => 2821760),
		array("label" => "Space", "y" => 21637),
		array("label" => "Sports", "y" => 439051),
		array("label" => "Technology", "y" => 16042),
		array("label" => "Travel", "y" => 92917),
		array("label" => "Vehicles", "y" => 2293)
	);
?>
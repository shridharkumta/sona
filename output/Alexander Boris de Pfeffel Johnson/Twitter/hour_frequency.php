<?php 
	$hour_frequency = array(
		array("label" => "0:00 - 1:00", "y" => 8),
		array("label" => "1:00 - 2:00", "y" => 0),
		array("label" => "2:00 - 3:00", "y" => 6),
		array("label" => "3:00 - 4:00", "y" => 1),
		array("label" => "4:00 - 5:00", "y" => 2),
		array("label" => "5:00 - 6:00", "y" => 4),
		array("label" => "6:00 - 7:00", "y" => 55),
		array("label" => "7:00 - 8:00", "y" => 140),
		array("label" => "8:00 - 9:00", "y" => 165),
		array("label" => "9:00 - 10:00", "y" => 218),
		array("label" => "10:00 - 11:00", "y" => 224),
		array("label" => "11:00 - 12:00", "y" => 224),
		array("label" => "12:00 - 13:00", "y" => 219),
		array("label" => "13:00 - 14:00", "y" => 210),
		array("label" => "14:00 - 15:00", "y" => 168),
		array("label" => "15:00 - 16:00", "y" => 182),
		array("label" => "16:00 - 17:00", "y" => 250),
		array("label" => "17:00 - 18:00", "y" => 244),
		array("label" => "18:00 - 19:00", "y" => 236),
		array("label" => "19:00 - 20:00", "y" => 214),
		array("label" => "20:00 - 21:00", "y" => 194),
		array("label" => "21:00 - 22:00", "y" => 105),
		array("label" => "22:00 - 23:00", "y" => 53),
		array("label" => "23:00 - 00:00", "y" => 12)
	);
?>
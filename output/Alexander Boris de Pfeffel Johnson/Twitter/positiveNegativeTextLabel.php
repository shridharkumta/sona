<?php 
	$positive = array(
		array("label" => "Politics", "y" => 1011),
		array("label" => "Movie", "y" => 68),
		array("label" => "Business", "y" => 513),
		array("label" => "Food", "y" => 51),
		array("label" => "Sports", "y" => 130),
		array("label" => "Music", "y" => 52),
		array("label" => "Medicine", "y" => 201),
		array("label" => "Gaming", "y" => 25),
		array("label" => "Travel", "y" => 27),
		array("label" => "Fashion", "y" => 7),
		array("label" => "Technology", "y" => 9),
		array("label" => "Space", "y" => 28),
		array("label" => "Vehicles", "y" => 5),
		array("label" => "Books", "y" => 2)
	);
	$negative = array(
		array("label" => "Politics", "y" => 559),
		array("label" => "Movie", "y" => 34),
		array("label" => "Business", "y" => 148),
		array("label" => "Food", "y" => 18),
		array("label" => "Sports", "y" => 50),
		array("label" => "Music", "y" => 9),
		array("label" => "Medicine", "y" => 140),
		array("label" => "Gaming", "y" => 9),
		array("label" => "Travel", "y" => 17),
		array("label" => "Fashion", "y" => 1),
		array("label" => "Technology", "y" => 3),
		array("label" => "Space", "y" => 15),
		array("label" => "Vehicles", "y" => 1),
		array("label" => "Books", "y" => 1)
	);
	$neutral = array(
		array("label" => "Politics", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Books", "y" => 0)
	);

?>
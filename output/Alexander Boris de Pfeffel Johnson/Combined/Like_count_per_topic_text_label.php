<?php 
	$likeCountPerTopic = array(
		array("label" => "Movie", "y" => 736921),
		array("label" => "Space", "y" => 228675),
		array("label" => "Medicine", "y" => 2394527),
		array("label" => "Travel", "y" => 146544),
		array("label" => "Music", "y" => 307555),
		array("label" => "Business", "y" => 3059973),
		array("label" => "Gaming", "y" => 420928),
		array("label" => "Sports", "y" => 960892),
		array("label" => "Vehicles", "y" => 15019),
		array("label" => "Politics", "y" => 5623357),
		array("label" => "Food", "y" => 608711),
		array("label" => "Fashion", "y" => 80269),
		array("label" => "Technology", "y" => 123547),
		array("label" => "Books", "y" => 3316),
		array("label" => "Others", "y" => 433557)
	);
?>
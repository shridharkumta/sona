<?php 
	$frequency_label = array(
		array("label" => "Politics", "y" => 1748),
		array("label" => "Business", "y" => 803),
		array("label" => "Medicine", "y" => 425),
		array("label" => "Sports", "y" => 212),
		array("label" => "Movie", "y" => 128),
		array("label" => "Food", "y" => 88),
		array("label" => "Music", "y" => 73),
		array("label" => "Space", "y" => 53),
		array("label" => "Travel", "y" => 50),
		array("label" => "Gaming", "y" => 46),
		array("label" => "Others", "y" => 19),
		array("label" => "Technology", "y" => 16),
		array("label" => "Fashion", "y" => 10),
		array("label" => "Vehicles", "y" => 7),
		array("label" => "Books", "y" => 3),
	);
?>
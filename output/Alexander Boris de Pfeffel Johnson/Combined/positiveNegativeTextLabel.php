<?php 
	$positive = array(
		array("label" => "Movie", "y" => 88),
		array("label" => "Space", "y" => 35),
		array("label" => "Medicine", "y" => 248),
		array("label" => "Travel", "y" => 31),
		array("label" => "Music", "y" => 62),
		array("label" => "Business", "y" => 627),
		array("label" => "Gaming", "y" => 35),
		array("label" => "Sports", "y" => 154),
		array("label" => "Vehicles", "y" => 5),
		array("label" => "Politics", "y" => 1136),
		array("label" => "Food", "y" => 66),
		array("label" => "Technology", "y" => 12),
		array("label" => "Fashion", "y" => 9),
		array("label" => "Books", "y" => 2),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Movie", "y" => 40),
		array("label" => "Space", "y" => 18),
		array("label" => "Medicine", "y" => 177),
		array("label" => "Travel", "y" => 19),
		array("label" => "Music", "y" => 11),
		array("label" => "Business", "y" => 176),
		array("label" => "Gaming", "y" => 11),
		array("label" => "Sports", "y" => 58),
		array("label" => "Vehicles", "y" => 2),
		array("label" => "Politics", "y" => 612),
		array("label" => "Food", "y" => 22),
		array("label" => "Technology", "y" => 4),
		array("label" => "Fashion", "y" => 1),
		array("label" => "Books", "y" => 1),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 19)
	);

?>
<?php 
	$hashtagCount = array(
		array("label" => "#debate", "y" => 52),
		array("label" => "#debatenight", "y" => 48),
		array("label" => "#gutsywomen", "y" => 42),
		array("label" => "#tbt", "y" => 28),
		array("label" => "#vpdebate", "y" => 27),
		array("label" => "#hillary2016", "y" => 27),
		array("label" => "#imwithher", "y" => 14),
		array("label" => "#shewon", "y" => 8),
		array("label" => "#electionday", "y" => 7),
		array("label" => "#arenaacademy", "y" => 7),
	);
?>
<?php 
	$positive = array(
		array("label" => "Movie", "y" => 322),
		array("label" => "Space", "y" => 51),
		array("label" => "Medicine", "y" => 111),
		array("label" => "Travel", "y" => 19),
		array("label" => "Music", "y" => 101),
		array("label" => "Business", "y" => 551),
		array("label" => "Gaming", "y" => 38),
		array("label" => "Sports", "y" => 140),
		array("label" => "Vehicles", "y" => 13),
		array("label" => "Politics", "y" => 1638),
		array("label" => "Food", "y" => 111),
		array("label" => "Fashion", "y" => 45),
		array("label" => "Technology", "y" => 11),
		array("label" => "Books", "y" => 27),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Movie", "y" => 46),
		array("label" => "Space", "y" => 22),
		array("label" => "Medicine", "y" => 87),
		array("label" => "Travel", "y" => 35),
		array("label" => "Music", "y" => 12),
		array("label" => "Business", "y" => 138),
		array("label" => "Gaming", "y" => 16),
		array("label" => "Sports", "y" => 39),
		array("label" => "Vehicles", "y" => 8),
		array("label" => "Politics", "y" => 785),
		array("label" => "Food", "y" => 17),
		array("label" => "Fashion", "y" => 12),
		array("label" => "Technology", "y" => 3),
		array("label" => "Books", "y" => 6),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 18)
	);

?>
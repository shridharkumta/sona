<?php 
	$frequency_label = array(
		array("label" => "Politics", "y" => 2423),
		array("label" => "Business", "y" => 689),
		array("label" => "Movie", "y" => 368),
		array("label" => "Medicine", "y" => 198),
		array("label" => "Sports", "y" => 179),
		array("label" => "Food", "y" => 128),
		array("label" => "Music", "y" => 113),
		array("label" => "Space", "y" => 73),
		array("label" => "Fashion", "y" => 57),
		array("label" => "Travel", "y" => 54),
		array("label" => "Gaming", "y" => 54),
		array("label" => "Books", "y" => 33),
		array("label" => "Vehicles", "y" => 21),
		array("label" => "Others", "y" => 18),
		array("label" => "Technology", "y" => 14),
	);
?>
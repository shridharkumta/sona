<?php 
	$likeCountPerTopic = array(
		array("label" => "Movie", "y" => 16802105),
		array("label" => "Space", "y" => 2668275),
		array("label" => "Medicine", "y" => 7789496),
		array("label" => "Travel", "y" => 2206557),
		array("label" => "Music", "y" => 4765536),
		array("label" => "Business", "y" => 21738867),
		array("label" => "Gaming", "y" => 2539171),
		array("label" => "Sports", "y" => 7528912),
		array("label" => "Vehicles", "y" => 731247),
		array("label" => "Politics", "y" => 70288406),
		array("label" => "Food", "y" => 6433526),
		array("label" => "Fashion", "y" => 2907544),
		array("label" => "Technology", "y" => 543247),
		array("label" => "Books", "y" => 1435491),
		array("label" => "Others", "y" => 988473)
	);
?>
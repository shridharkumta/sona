<?php 
	$positive = array(
		array("label" => "Sports", "y" => 52),
		array("label" => "Movie", "y" => 140),
		array("label" => "Business", "y" => 142),
		array("label" => "Music", "y" => 28),
		array("label" => "Food", "y" => 45),
		array("label" => "Gaming", "y" => 14),
		array("label" => "Space", "y" => 23),
		array("label" => "Politics", "y" => 401),
		array("label" => "Books", "y" => 6),
		array("label" => "Fashion", "y" => 36),
		array("label" => "Medicine", "y" => 25),
		array("label" => "Vehicles", "y" => 5),
		array("label" => "Travel", "y" => 12),
		array("label" => "Others", "y" => 0),
		array("label" => "Technology", "y" => 5)
	);
	$negative = array(
		array("label" => "Sports", "y" => 9),
		array("label" => "Movie", "y" => 9),
		array("label" => "Business", "y" => 37),
		array("label" => "Music", "y" => 3),
		array("label" => "Food", "y" => 7),
		array("label" => "Gaming", "y" => 3),
		array("label" => "Space", "y" => 7),
		array("label" => "Politics", "y" => 105),
		array("label" => "Books", "y" => 1),
		array("label" => "Fashion", "y" => 11),
		array("label" => "Medicine", "y" => 15),
		array("label" => "Vehicles", "y" => 3),
		array("label" => "Travel", "y" => 13),
		array("label" => "Others", "y" => 0),
		array("label" => "Technology", "y" => 1)
	);
	$neutral = array(
		array("label" => "Sports", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Others", "y" => 17),
		array("label" => "Technology", "y" => 0)
	);

?>
<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 7),
		array("label" => "Business", "y" => 179),
		array("label" => "Fashion", "y" => 47),
		array("label" => "Food", "y" => 52),
		array("label" => "Gaming", "y" => 17),
		array("label" => "Medicine", "y" => 40),
		array("label" => "Movie", "y" => 149),
		array("label" => "Music", "y" => 31),
		array("label" => "Others", "y" => 17),
		array("label" => "Politics", "y" => 506),
		array("label" => "Space", "y" => 30),
		array("label" => "Sports", "y" => 61),
		array("label" => "Technology", "y" => 6),
		array("label" => "Travel", "y" => 25),
		array("label" => "Vehicles", "y" => 8)
	);
?>
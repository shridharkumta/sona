<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 282596),
		array("label" => "Business", "y" => 9873993),
		array("label" => "Fashion", "y" => 2456767),
		array("label" => "Food", "y" => 4125329),
		array("label" => "Gaming", "y" => 1165767),
		array("label" => "Medicine", "y" => 1989463),
		array("label" => "Movie", "y" => 9395592),
		array("label" => "Music", "y" => 1950591),
		array("label" => "Others", "y" => 960715),
		array("label" => "Politics", "y" => 29449935),
		array("label" => "Space", "y" => 1395264),
		array("label" => "Sports", "y" => 4198627),
		array("label" => "Technology", "y" => 369978),
		array("label" => "Travel", "y" => 1582394),
		array("label" => "Vehicles", "y" => 458897)
	);
?>
<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 26),
		array("label" => "Business", "y" => 510),
		array("label" => "Fashion", "y" => 10),
		array("label" => "Food", "y" => 76),
		array("label" => "Gaming", "y" => 37),
		array("label" => "Medicine", "y" => 158),
		array("label" => "Movie", "y" => 219),
		array("label" => "Music", "y" => 82),
		array("label" => "Others", "y" => 1),
		array("label" => "Politics", "y" => 1917),
		array("label" => "Space", "y" => 43),
		array("label" => "Sports", "y" => 118),
		array("label" => "Technology", "y" => 8),
		array("label" => "Travel", "y" => 29),
		array("label" => "Vehicles", "y" => 13)
	);
?>
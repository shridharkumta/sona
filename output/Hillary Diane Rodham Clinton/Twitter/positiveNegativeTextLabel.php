<?php 
	$positive = array(
		array("label" => "Business", "y" => 409),
		array("label" => "Politics", "y" => 1237),
		array("label" => "Gaming", "y" => 24),
		array("label" => "Movie", "y" => 182),
		array("label" => "Medicine", "y" => 86),
		array("label" => "Space", "y" => 28),
		array("label" => "Books", "y" => 21),
		array("label" => "Food", "y" => 66),
		array("label" => "Music", "y" => 73),
		array("label" => "Sports", "y" => 88),
		array("label" => "Travel", "y" => 7),
		array("label" => "Fashion", "y" => 9),
		array("label" => "Vehicles", "y" => 8),
		array("label" => "Technology", "y" => 6),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Business", "y" => 101),
		array("label" => "Politics", "y" => 680),
		array("label" => "Gaming", "y" => 13),
		array("label" => "Movie", "y" => 37),
		array("label" => "Medicine", "y" => 72),
		array("label" => "Space", "y" => 15),
		array("label" => "Books", "y" => 5),
		array("label" => "Food", "y" => 10),
		array("label" => "Music", "y" => 9),
		array("label" => "Sports", "y" => 30),
		array("label" => "Travel", "y" => 22),
		array("label" => "Fashion", "y" => 1),
		array("label" => "Vehicles", "y" => 5),
		array("label" => "Technology", "y" => 2),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Business", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Others", "y" => 1)
	);

?>
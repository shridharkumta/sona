<?php 
	$hashtagCount = array(
		array("label" => "#debate", "y" => 50),
		array("label" => "#debatenight", "y" => 41),
		array("label" => "#vpdebate", "y" => 26),
		array("label" => "#gutsywomen", "y" => 13),
		array("label" => "#electionday", "y" => 7),
		array("label" => "#arenaacademy", "y" => 7),
		array("label" => "#familiesbelongtogether", "y" => 7),
		array("label" => "#strongertogether", "y" => 7),
		array("label" => "#alsmithdinner", "y" => 6),
		array("label" => "#imwithher", "y" => 5),
	);
?>
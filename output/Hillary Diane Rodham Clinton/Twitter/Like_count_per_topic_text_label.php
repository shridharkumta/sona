<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 1152895),
		array("label" => "Business", "y" => 11864874),
		array("label" => "Fashion", "y" => 450777),
		array("label" => "Food", "y" => 2308197),
		array("label" => "Gaming", "y" => 1373404),
		array("label" => "Medicine", "y" => 5800033),
		array("label" => "Movie", "y" => 7406513),
		array("label" => "Music", "y" => 2814945),
		array("label" => "Others", "y" => 27758),
		array("label" => "Politics", "y" => 40838471),
		array("label" => "Space", "y" => 1273011),
		array("label" => "Sports", "y" => 3330285),
		array("label" => "Technology", "y" => 173269),
		array("label" => "Travel", "y" => 624163),
		array("label" => "Vehicles", "y" => 272350)
	);
?>
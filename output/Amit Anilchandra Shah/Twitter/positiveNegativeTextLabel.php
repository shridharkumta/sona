<?php 
	$positive = array(
		array("label" => "Politics", "y" => 880),
		array("label" => "Travel", "y" => 26),
		array("label" => "Sports", "y" => 32),
		array("label" => "Business", "y" => 30),
		array("label" => "Others", "y" => 0),
		array("label" => "Medicine", "y" => 13),
		array("label" => "Movie", "y" => 17),
		array("label" => "Space", "y" => 12),
		array("label" => "Food", "y" => 15),
		array("label" => "Technology", "y" => 21),
		array("label" => "Fashion", "y" => 1),
		array("label" => "Music", "y" => 9),
		array("label" => "Gaming", "y" => 4),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Books", "y" => 4)
	);
	$negative = array(
		array("label" => "Politics", "y" => 1075),
		array("label" => "Travel", "y" => 788),
		array("label" => "Sports", "y" => 20),
		array("label" => "Business", "y" => 16),
		array("label" => "Others", "y" => 0),
		array("label" => "Medicine", "y" => 23),
		array("label" => "Movie", "y" => 25),
		array("label" => "Space", "y" => 15),
		array("label" => "Food", "y" => 7),
		array("label" => "Technology", "y" => 25),
		array("label" => "Fashion", "y" => 7),
		array("label" => "Music", "y" => 6),
		array("label" => "Gaming", "y" => 7),
		array("label" => "Vehicles", "y" => 14),
		array("label" => "Books", "y" => 0)
	);
	$neutral = array(
		array("label" => "Politics", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Others", "y" => 175),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Books", "y" => 0)
	);

?>
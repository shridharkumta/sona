<?php 
	$hour_frequency = array(
		array("label" => "0:00 - 1:00", "y" => 3),
		array("label" => "1:00 - 2:00", "y" => 54),
		array("label" => "2:00 - 3:00", "y" => 108),
		array("label" => "3:00 - 4:00", "y" => 132),
		array("label" => "4:00 - 5:00", "y" => 160),
		array("label" => "5:00 - 6:00", "y" => 155),
		array("label" => "6:00 - 7:00", "y" => 168),
		array("label" => "7:00 - 8:00", "y" => 193),
		array("label" => "8:00 - 9:00", "y" => 166),
		array("label" => "9:00 - 10:00", "y" => 235),
		array("label" => "10:00 - 11:00", "y" => 213),
		array("label" => "11:00 - 12:00", "y" => 225),
		array("label" => "12:00 - 13:00", "y" => 255),
		array("label" => "13:00 - 14:00", "y" => 275),
		array("label" => "14:00 - 15:00", "y" => 271),
		array("label" => "15:00 - 16:00", "y" => 244),
		array("label" => "16:00 - 17:00", "y" => 188),
		array("label" => "17:00 - 18:00", "y" => 157),
		array("label" => "18:00 - 19:00", "y" => 48),
		array("label" => "19:00 - 20:00", "y" => 11),
		array("label" => "20:00 - 21:00", "y" => 1),
		array("label" => "21:00 - 22:00", "y" => 5),
		array("label" => "22:00 - 23:00", "y" => 0),
		array("label" => "23:00 - 00:00", "y" => 0)
	);
?>
<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 4),
		array("label" => "Business", "y" => 46),
		array("label" => "Fashion", "y" => 8),
		array("label" => "Food", "y" => 22),
		array("label" => "Gaming", "y" => 11),
		array("label" => "Medicine", "y" => 36),
		array("label" => "Movie", "y" => 42),
		array("label" => "Music", "y" => 15),
		array("label" => "Others", "y" => 175),
		array("label" => "Politics", "y" => 1955),
		array("label" => "Space", "y" => 27),
		array("label" => "Sports", "y" => 52),
		array("label" => "Technology", "y" => 46),
		array("label" => "Travel", "y" => 814),
		array("label" => "Vehicles", "y" => 14)
	);
?>
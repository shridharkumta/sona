<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 15597),
		array("label" => "Business", "y" => 368256),
		array("label" => "Fashion", "y" => 50201),
		array("label" => "Food", "y" => 99706),
		array("label" => "Gaming", "y" => 83605),
		array("label" => "Medicine", "y" => 307624),
		array("label" => "Movie", "y" => 303411),
		array("label" => "Music", "y" => 93472),
		array("label" => "Others", "y" => 1951288),
		array("label" => "Politics", "y" => 15024621),
		array("label" => "Space", "y" => 292136),
		array("label" => "Sports", "y" => 468814),
		array("label" => "Technology", "y" => 193715),
		array("label" => "Travel", "y" => 5334859),
		array("label" => "Vehicles", "y" => 171321)
	);
?>
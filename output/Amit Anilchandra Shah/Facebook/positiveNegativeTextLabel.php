<?php 
	$positive = array(
		array("label" => "Others", "y" => 0),
		array("label" => "Sports", "y" => 80),
		array("label" => "Travel", "y" => 13),
		array("label" => "Politics", "y" => 1032),
		array("label" => "Medicine", "y" => 6),
		array("label" => "Movie", "y" => 124),
		array("label" => "Space", "y" => 22),
		array("label" => "Business", "y" => 20),
		array("label" => "Music", "y" => 9),
		array("label" => "Fashion", "y" => 6),
		array("label" => "Gaming", "y" => 3),
		array("label" => "Technology", "y" => 3),
		array("label" => "Vehicles", "y" => 2),
		array("label" => "Food", "y" => 13),
		array("label" => "Books", "y" => 6)
	);
	$negative = array(
		array("label" => "Others", "y" => 0),
		array("label" => "Sports", "y" => 41),
		array("label" => "Travel", "y" => 80),
		array("label" => "Politics", "y" => 836),
		array("label" => "Medicine", "y" => 18),
		array("label" => "Movie", "y" => 21),
		array("label" => "Space", "y" => 12),
		array("label" => "Business", "y" => 17),
		array("label" => "Music", "y" => 13),
		array("label" => "Fashion", "y" => 4),
		array("label" => "Gaming", "y" => 15),
		array("label" => "Technology", "y" => 11),
		array("label" => "Vehicles", "y" => 25),
		array("label" => "Food", "y" => 1),
		array("label" => "Books", "y" => 0)
	);
	$neutral = array(
		array("label" => "Others", "y" => 1821),
		array("label" => "Sports", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Books", "y" => 0)
	);

?>
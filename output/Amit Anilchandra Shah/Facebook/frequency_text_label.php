<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 6),
		array("label" => "Business", "y" => 37),
		array("label" => "Fashion", "y" => 10),
		array("label" => "Food", "y" => 14),
		array("label" => "Gaming", "y" => 18),
		array("label" => "Medicine", "y" => 24),
		array("label" => "Movie", "y" => 145),
		array("label" => "Music", "y" => 22),
		array("label" => "Others", "y" => 1821),
		array("label" => "Politics", "y" => 1868),
		array("label" => "Space", "y" => 34),
		array("label" => "Sports", "y" => 121),
		array("label" => "Technology", "y" => 14),
		array("label" => "Travel", "y" => 93),
		array("label" => "Vehicles", "y" => 27)
	);
?>
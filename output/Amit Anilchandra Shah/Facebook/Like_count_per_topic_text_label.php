<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 73995),
		array("label" => "Business", "y" => 945889),
		array("label" => "Fashion", "y" => 288037),
		array("label" => "Food", "y" => 345017),
		array("label" => "Gaming", "y" => 598161),
		array("label" => "Medicine", "y" => 1317669),
		array("label" => "Movie", "y" => 3421028),
		array("label" => "Music", "y" => 1043198),
		array("label" => "Others", "y" => 57641614),
		array("label" => "Politics", "y" => 39645452),
		array("label" => "Space", "y" => 838929),
		array("label" => "Sports", "y" => 3236281),
		array("label" => "Technology", "y" => 187627),
		array("label" => "Travel", "y" => 2602600),
		array("label" => "Vehicles", "y" => 603513)
	);
?>
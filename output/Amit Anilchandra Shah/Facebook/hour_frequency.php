<?php 
	$hour_frequency = array(
		array("label" => "0:00 - 1:00", "y" => 13),
		array("label" => "1:00 - 2:00", "y" => 7),
		array("label" => "2:00 - 3:00", "y" => 9),
		array("label" => "3:00 - 4:00", "y" => 1),
		array("label" => "4:00 - 5:00", "y" => 4),
		array("label" => "5:00 - 6:00", "y" => 3),
		array("label" => "6:00 - 7:00", "y" => 15),
		array("label" => "7:00 - 8:00", "y" => 124),
		array("label" => "8:00 - 9:00", "y" => 274),
		array("label" => "9:00 - 10:00", "y" => 226),
		array("label" => "10:00 - 11:00", "y" => 224),
		array("label" => "11:00 - 12:00", "y" => 243),
		array("label" => "12:00 - 13:00", "y" => 322),
		array("label" => "13:00 - 14:00", "y" => 248),
		array("label" => "14:00 - 15:00", "y" => 241),
		array("label" => "15:00 - 16:00", "y" => 248),
		array("label" => "16:00 - 17:00", "y" => 256),
		array("label" => "17:00 - 18:00", "y" => 252),
		array("label" => "18:00 - 19:00", "y" => 283),
		array("label" => "19:00 - 20:00", "y" => 272),
		array("label" => "20:00 - 21:00", "y" => 457),
		array("label" => "21:00 - 22:00", "y" => 259),
		array("label" => "22:00 - 23:00", "y" => 203),
		array("label" => "23:00 - 00:00", "y" => 69)
	);
?>
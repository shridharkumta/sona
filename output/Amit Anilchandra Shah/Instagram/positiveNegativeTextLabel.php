<?php 
	$positive = array(
		array("label" => "Others", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Politics", "y" => 223),
		array("label" => "Sports", "y" => 14),
		array("label" => "Travel", "y" => 14),
		array("label" => "Space", "y" => 3),
		array("label" => "Movie", "y" => 14),
		array("label" => "Vehicles", "y" => 5),
		array("label" => "Music", "y" => 4),
		array("label" => "Business", "y" => 3),
		array("label" => "Fashion", "y" => 2),
		array("label" => "Technology", "y" => 0),
		array("label" => "Food", "y" => 3),
		array("label" => "Gaming", "y" => 1)
	);
	$negative = array(
		array("label" => "Others", "y" => 0),
		array("label" => "Medicine", "y" => 8),
		array("label" => "Politics", "y" => 113),
		array("label" => "Sports", "y" => 9),
		array("label" => "Travel", "y" => 26),
		array("label" => "Space", "y" => 5),
		array("label" => "Movie", "y" => 7),
		array("label" => "Vehicles", "y" => 4),
		array("label" => "Music", "y" => 1),
		array("label" => "Business", "y" => 2),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Technology", "y" => 1),
		array("label" => "Food", "y" => 0),
		array("label" => "Gaming", "y" => 0)
	);
	$neutral = array(
		array("label" => "Others", "y" => 170),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Gaming", "y" => 0)
	);

?>
<?php 
	$likeCountPerTopic = array(
		array("label" => "Business", "y" => 101028),
		array("label" => "Fashion", "y" => 21685),
		array("label" => "Food", "y" => 47909),
		array("label" => "Gaming", "y" => 10992),
		array("label" => "Medicine", "y" => 499621),
		array("label" => "Movie", "y" => 1206978),
		array("label" => "Music", "y" => 163851),
		array("label" => "Others", "y" => 6410754),
		array("label" => "Politics", "y" => 14556363),
		array("label" => "Space", "y" => 286581),
		array("label" => "Sports", "y" => 1316873),
		array("label" => "Technology", "y" => 11487),
		array("label" => "Travel", "y" => 1633524),
		array("label" => "Vehicles", "y" => 289141)
	);
?>
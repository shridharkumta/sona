<?php 
	$frequency_label = array(
		array("label" => "Business", "y" => 5),
		array("label" => "Fashion", "y" => 2),
		array("label" => "Food", "y" => 3),
		array("label" => "Gaming", "y" => 1),
		array("label" => "Medicine", "y" => 8),
		array("label" => "Movie", "y" => 21),
		array("label" => "Music", "y" => 5),
		array("label" => "Others", "y" => 170),
		array("label" => "Politics", "y" => 336),
		array("label" => "Space", "y" => 8),
		array("label" => "Sports", "y" => 23),
		array("label" => "Technology", "y" => 1),
		array("label" => "Travel", "y" => 40),
		array("label" => "Vehicles", "y" => 9)
	);
?>
<?php 
	$frequency_label = array(
		array("label" => "Politics", "y" => 4159),
		array("label" => "Others", "y" => 2166),
		array("label" => "Travel", "y" => 947),
		array("label" => "Movie", "y" => 208),
		array("label" => "Sports", "y" => 196),
		array("label" => "Business", "y" => 88),
		array("label" => "Space", "y" => 69),
		array("label" => "Medicine", "y" => 68),
		array("label" => "Technology", "y" => 61),
		array("label" => "Vehicles", "y" => 50),
		array("label" => "Music", "y" => 42),
		array("label" => "Food", "y" => 39),
		array("label" => "Gaming", "y" => 30),
		array("label" => "Fashion", "y" => 20),
		array("label" => "Books", "y" => 10),
	);
?>
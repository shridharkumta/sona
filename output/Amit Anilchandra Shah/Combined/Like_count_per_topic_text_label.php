<?php 
	$likeCountPerTopic = array(
		array("label" => "Movie", "y" => 4931417),
		array("label" => "Space", "y" => 1417646),
		array("label" => "Medicine", "y" => 2124914),
		array("label" => "Travel", "y" => 9570983),
		array("label" => "Music", "y" => 1300521),
		array("label" => "Business", "y" => 1415173),
		array("label" => "Gaming", "y" => 692758),
		array("label" => "Sports", "y" => 5021968),
		array("label" => "Vehicles", "y" => 1063975),
		array("label" => "Politics", "y" => 69226436),
		array("label" => "Food", "y" => 492632),
		array("label" => "Fashion", "y" => 359923),
		array("label" => "Technology", "y" => 392829),
		array("label" => "Books", "y" => 89592),
		array("label" => "Others", "y" => 66003656)
	);
?>
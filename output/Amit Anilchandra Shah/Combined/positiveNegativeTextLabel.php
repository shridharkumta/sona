<?php 
	$positive = array(
		array("label" => "Movie", "y" => 155),
		array("label" => "Space", "y" => 37),
		array("label" => "Medicine", "y" => 19),
		array("label" => "Travel", "y" => 53),
		array("label" => "Music", "y" => 22),
		array("label" => "Business", "y" => 53),
		array("label" => "Gaming", "y" => 8),
		array("label" => "Sports", "y" => 126),
		array("label" => "Vehicles", "y" => 7),
		array("label" => "Politics", "y" => 2135),
		array("label" => "Food", "y" => 31),
		array("label" => "Fashion", "y" => 9),
		array("label" => "Technology", "y" => 24),
		array("label" => "Books", "y" => 10),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Movie", "y" => 53),
		array("label" => "Space", "y" => 32),
		array("label" => "Medicine", "y" => 49),
		array("label" => "Travel", "y" => 894),
		array("label" => "Music", "y" => 20),
		array("label" => "Business", "y" => 35),
		array("label" => "Gaming", "y" => 22),
		array("label" => "Sports", "y" => 70),
		array("label" => "Vehicles", "y" => 43),
		array("label" => "Politics", "y" => 2024),
		array("label" => "Food", "y" => 8),
		array("label" => "Fashion", "y" => 11),
		array("label" => "Technology", "y" => 37),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 2166)
	);

?>
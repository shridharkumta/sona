<?php 
	$positive = array(
		array("label" => "Sports", "y" => 62),
		array("label" => "Food", "y" => 731),
		array("label" => "Business", "y" => 24),
		array("label" => "Travel", "y" => 31),
		array("label" => "Movie", "y" => 96),
		array("label" => "Fashion", "y" => 5),
		array("label" => "Gaming", "y" => 26),
		array("label" => "Music", "y" => 9),
		array("label" => "Vehicles", "y" => 8),
		array("label" => "Others", "y" => 0),
		array("label" => "Medicine", "y" => 15),
		array("label" => "Technology", "y" => 5),
		array("label" => "Space", "y" => 7),
		array("label" => "Politics", "y" => 3),
		array("label" => "Books", "y" => 2)
	);
	$negative = array(
		array("label" => "Sports", "y" => 21),
		array("label" => "Food", "y" => 72),
		array("label" => "Business", "y" => 0),
		array("label" => "Travel", "y" => 6),
		array("label" => "Movie", "y" => 17),
		array("label" => "Fashion", "y" => 2),
		array("label" => "Gaming", "y" => 4),
		array("label" => "Music", "y" => 2),
		array("label" => "Vehicles", "y" => 1),
		array("label" => "Others", "y" => 0),
		array("label" => "Medicine", "y" => 1),
		array("label" => "Technology", "y" => 1),
		array("label" => "Space", "y" => 2),
		array("label" => "Politics", "y" => 2),
		array("label" => "Books", "y" => 0)
	);
	$neutral = array(
		array("label" => "Sports", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Others", "y" => 18),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Books", "y" => 0)
	);

?>
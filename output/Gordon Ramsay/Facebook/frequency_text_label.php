<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 2),
		array("label" => "Business", "y" => 24),
		array("label" => "Fashion", "y" => 7),
		array("label" => "Food", "y" => 803),
		array("label" => "Gaming", "y" => 30),
		array("label" => "Medicine", "y" => 16),
		array("label" => "Movie", "y" => 113),
		array("label" => "Music", "y" => 11),
		array("label" => "Others", "y" => 18),
		array("label" => "Politics", "y" => 5),
		array("label" => "Space", "y" => 9),
		array("label" => "Sports", "y" => 83),
		array("label" => "Technology", "y" => 6),
		array("label" => "Travel", "y" => 37),
		array("label" => "Vehicles", "y" => 9)
	);
?>
<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 2275),
		array("label" => "Business", "y" => 257376),
		array("label" => "Fashion", "y" => 88612),
		array("label" => "Food", "y" => 7060381),
		array("label" => "Gaming", "y" => 226656),
		array("label" => "Medicine", "y" => 65465),
		array("label" => "Movie", "y" => 1507233),
		array("label" => "Music", "y" => 132625),
		array("label" => "Others", "y" => 93600),
		array("label" => "Politics", "y" => 21419),
		array("label" => "Space", "y" => 41105),
		array("label" => "Sports", "y" => 470993),
		array("label" => "Technology", "y" => 37269),
		array("label" => "Travel", "y" => 315751),
		array("label" => "Vehicles", "y" => 334886)
	);
?>
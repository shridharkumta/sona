<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 8180),
		array("label" => "Business", "y" => 440542),
		array("label" => "Fashion", "y" => 241555),
		array("label" => "Food", "y" => 3488797),
		array("label" => "Gaming", "y" => 512188),
		array("label" => "Medicine", "y" => 277477),
		array("label" => "Movie", "y" => 844976),
		array("label" => "Music", "y" => 400745),
		array("label" => "Politics", "y" => 191773),
		array("label" => "Space", "y" => 226657),
		array("label" => "Sports", "y" => 612599),
		array("label" => "Technology", "y" => 170302),
		array("label" => "Travel", "y" => 370640),
		array("label" => "Vehicles", "y" => 121560)
	);
?>
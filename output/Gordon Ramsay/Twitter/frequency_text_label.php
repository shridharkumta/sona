<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 8),
		array("label" => "Business", "y" => 174),
		array("label" => "Fashion", "y" => 93),
		array("label" => "Food", "y" => 1429),
		array("label" => "Gaming", "y" => 226),
		array("label" => "Medicine", "y" => 94),
		array("label" => "Movie", "y" => 395),
		array("label" => "Music", "y" => 149),
		array("label" => "Politics", "y" => 94),
		array("label" => "Space", "y" => 78),
		array("label" => "Sports", "y" => 279),
		array("label" => "Technology", "y" => 41),
		array("label" => "Travel", "y" => 166),
		array("label" => "Vehicles", "y" => 46)
	);
?>
<?php 
	$positive = array(
		array("label" => "Vehicles", "y" => 28),
		array("label" => "Food", "y" => 1145),
		array("label" => "Sports", "y" => 196),
		array("label" => "Business", "y" => 133),
		array("label" => "Movie", "y" => 303),
		array("label" => "Space", "y" => 46),
		array("label" => "Gaming", "y" => 166),
		array("label" => "Travel", "y" => 88),
		array("label" => "Medicine", "y" => 46),
		array("label" => "Fashion", "y" => 62),
		array("label" => "Technology", "y" => 28),
		array("label" => "Politics", "y" => 46),
		array("label" => "Music", "y" => 108),
		array("label" => "Books", "y" => 5)
	);
	$negative = array(
		array("label" => "Vehicles", "y" => 18),
		array("label" => "Food", "y" => 284),
		array("label" => "Sports", "y" => 83),
		array("label" => "Business", "y" => 41),
		array("label" => "Movie", "y" => 92),
		array("label" => "Space", "y" => 32),
		array("label" => "Gaming", "y" => 60),
		array("label" => "Travel", "y" => 78),
		array("label" => "Medicine", "y" => 48),
		array("label" => "Fashion", "y" => 31),
		array("label" => "Technology", "y" => 13),
		array("label" => "Politics", "y" => 48),
		array("label" => "Music", "y" => 41),
		array("label" => "Books", "y" => 3)
	);
	$neutral = array(
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Books", "y" => 0)
	);

?>
<?php 
	$hashtagCount = array(
		array("label" => "#hellskitchen", "y" => 134),
		array("label" => "#masterchef", "y" => 111),
		array("label" => "#recipe", "y" => 94),
		array("label" => "#vegas", "y" => 73),
		array("label" => "#recipes", "y" => 70),
		array("label" => "#masterchefjunior", "y" => 58),
		array("label" => "#christmas", "y" => 54),
		array("label" => "#london", "y" => 50),
		array("label" => "#londonrestaurants", "y" => 49),
		array("label" => "#summer", "y" => 47),
	);
?>
<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 124236),
		array("label" => "Business", "y" => 5453389),
		array("label" => "Fashion", "y" => 5273267),
		array("label" => "Food", "y" => 95599569),
		array("label" => "Gaming", "y" => 5179076),
		array("label" => "Medicine", "y" => 1819697),
		array("label" => "Movie", "y" => 19630995),
		array("label" => "Music", "y" => 4718188),
		array("label" => "Others", "y" => 86324),
		array("label" => "Politics", "y" => 1573265),
		array("label" => "Space", "y" => 2047533),
		array("label" => "Sports", "y" => 12215225),
		array("label" => "Technology", "y" => 825362),
		array("label" => "Travel", "y" => 7947170),
		array("label" => "Vehicles", "y" => 3508510)
	);
?>
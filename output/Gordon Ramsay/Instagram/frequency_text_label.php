<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 5),
		array("label" => "Business", "y" => 98),
		array("label" => "Fashion", "y" => 103),
		array("label" => "Food", "y" => 2292),
		array("label" => "Gaming", "y" => 131),
		array("label" => "Medicine", "y" => 56),
		array("label" => "Movie", "y" => 442),
		array("label" => "Music", "y" => 112),
		array("label" => "Others", "y" => 9),
		array("label" => "Politics", "y" => 28),
		array("label" => "Space", "y" => 67),
		array("label" => "Sports", "y" => 264),
		array("label" => "Technology", "y" => 22),
		array("label" => "Travel", "y" => 204),
		array("label" => "Vehicles", "y" => 82)
	);
?>
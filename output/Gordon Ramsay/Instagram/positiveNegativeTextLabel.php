<?php 
	$positive = array(
		array("label" => "Sports", "y" => 213),
		array("label" => "Gaming", "y" => 102),
		array("label" => "Food", "y" => 2108),
		array("label" => "Movie", "y" => 379),
		array("label" => "Business", "y" => 80),
		array("label" => "Travel", "y" => 166),
		array("label" => "Space", "y" => 51),
		array("label" => "Fashion", "y" => 85),
		array("label" => "Music", "y" => 97),
		array("label" => "Vehicles", "y" => 63),
		array("label" => "Technology", "y" => 16),
		array("label" => "Medicine", "y" => 43),
		array("label" => "Politics", "y" => 16),
		array("label" => "Books", "y" => 3),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Sports", "y" => 51),
		array("label" => "Gaming", "y" => 29),
		array("label" => "Food", "y" => 184),
		array("label" => "Movie", "y" => 63),
		array("label" => "Business", "y" => 18),
		array("label" => "Travel", "y" => 38),
		array("label" => "Space", "y" => 16),
		array("label" => "Fashion", "y" => 18),
		array("label" => "Music", "y" => 15),
		array("label" => "Vehicles", "y" => 19),
		array("label" => "Technology", "y" => 6),
		array("label" => "Medicine", "y" => 13),
		array("label" => "Politics", "y" => 12),
		array("label" => "Books", "y" => 2),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Sports", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 9)
	);

?>
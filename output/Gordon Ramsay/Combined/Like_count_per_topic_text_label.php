<?php 
	$likeCountPerTopic = array(
		array("label" => "Movie", "y" => 21983204),
		array("label" => "Space", "y" => 2315295),
		array("label" => "Medicine", "y" => 2162639),
		array("label" => "Travel", "y" => 8633561),
		array("label" => "Music", "y" => 5251558),
		array("label" => "Business", "y" => 6151307),
		array("label" => "Gaming", "y" => 5917920),
		array("label" => "Sports", "y" => 13298817),
		array("label" => "Vehicles", "y" => 3964956),
		array("label" => "Politics", "y" => 1786457),
		array("label" => "Food", "y" => 106148747),
		array("label" => "Fashion", "y" => 5603434),
		array("label" => "Technology", "y" => 1032933),
		array("label" => "Books", "y" => 134691),
		array("label" => "Others", "y" => 179924)
	);
?>
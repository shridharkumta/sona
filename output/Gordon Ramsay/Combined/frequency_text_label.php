<?php 
	$frequency_label = array(
		array("label" => "Food", "y" => 4524),
		array("label" => "Movie", "y" => 950),
		array("label" => "Sports", "y" => 626),
		array("label" => "Travel", "y" => 407),
		array("label" => "Gaming", "y" => 387),
		array("label" => "Business", "y" => 296),
		array("label" => "Music", "y" => 272),
		array("label" => "Fashion", "y" => 203),
		array("label" => "Medicine", "y" => 166),
		array("label" => "Space", "y" => 154),
		array("label" => "Vehicles", "y" => 137),
		array("label" => "Politics", "y" => 127),
		array("label" => "Technology", "y" => 69),
		array("label" => "Others", "y" => 27),
		array("label" => "Books", "y" => 15),
	);
?>
<?php 
	$hour_frequency = array(
		array("label" => "0:00 - 1:00", "y" => 445),
		array("label" => "1:00 - 2:00", "y" => 482),
		array("label" => "2:00 - 3:00", "y" => 283),
		array("label" => "3:00 - 4:00", "y" => 220),
		array("label" => "4:00 - 5:00", "y" => 148),
		array("label" => "5:00 - 6:00", "y" => 122),
		array("label" => "6:00 - 7:00", "y" => 74),
		array("label" => "7:00 - 8:00", "y" => 99),
		array("label" => "8:00 - 9:00", "y" => 136),
		array("label" => "9:00 - 10:00", "y" => 204),
		array("label" => "10:00 - 11:00", "y" => 257),
		array("label" => "11:00 - 12:00", "y" => 299),
		array("label" => "12:00 - 13:00", "y" => 332),
		array("label" => "13:00 - 14:00", "y" => 351),
		array("label" => "14:00 - 15:00", "y" => 378),
		array("label" => "15:00 - 16:00", "y" => 464),
		array("label" => "16:00 - 17:00", "y" => 650),
		array("label" => "17:00 - 18:00", "y" => 686),
		array("label" => "18:00 - 19:00", "y" => 542),
		array("label" => "19:00 - 20:00", "y" => 405),
		array("label" => "20:00 - 21:00", "y" => 582),
		array("label" => "21:00 - 22:00", "y" => 378),
		array("label" => "22:00 - 23:00", "y" => 396),
		array("label" => "23:00 - 00:00", "y" => 424)
	);
?>
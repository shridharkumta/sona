<?php 
	$positive = array(
		array("label" => "Movie", "y" => 778),
		array("label" => "Space", "y" => 104),
		array("label" => "Medicine", "y" => 104),
		array("label" => "Travel", "y" => 285),
		array("label" => "Music", "y" => 214),
		array("label" => "Business", "y" => 237),
		array("label" => "Gaming", "y" => 294),
		array("label" => "Sports", "y" => 471),
		array("label" => "Vehicles", "y" => 99),
		array("label" => "Politics", "y" => 65),
		array("label" => "Food", "y" => 3984),
		array("label" => "Fashion", "y" => 152),
		array("label" => "Technology", "y" => 49),
		array("label" => "Books", "y" => 10),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Movie", "y" => 172),
		array("label" => "Space", "y" => 50),
		array("label" => "Medicine", "y" => 62),
		array("label" => "Travel", "y" => 122),
		array("label" => "Music", "y" => 58),
		array("label" => "Business", "y" => 59),
		array("label" => "Gaming", "y" => 93),
		array("label" => "Sports", "y" => 155),
		array("label" => "Vehicles", "y" => 38),
		array("label" => "Politics", "y" => 62),
		array("label" => "Food", "y" => 540),
		array("label" => "Fashion", "y" => 51),
		array("label" => "Technology", "y" => 20),
		array("label" => "Books", "y" => 5),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 27)
	);

?>
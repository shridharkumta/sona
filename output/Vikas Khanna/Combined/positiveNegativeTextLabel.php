<?php 
	$positive = array(
		array("label" => "Movie", "y" => 935),
		array("label" => "Space", "y" => 101),
		array("label" => "Medicine", "y" => 154),
		array("label" => "Travel", "y" => 148),
		array("label" => "Music", "y" => 211),
		array("label" => "Business", "y" => 409),
		array("label" => "Gaming", "y" => 104),
		array("label" => "Sports", "y" => 212),
		array("label" => "Vehicles", "y" => 45),
		array("label" => "Politics", "y" => 615),
		array("label" => "Food", "y" => 955),
		array("label" => "Fashion", "y" => 59),
		array("label" => "Technology", "y" => 34),
		array("label" => "Books", "y" => 32),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Movie", "y" => 111),
		array("label" => "Space", "y" => 15),
		array("label" => "Medicine", "y" => 69),
		array("label" => "Travel", "y" => 129),
		array("label" => "Music", "y" => 35),
		array("label" => "Business", "y" => 67),
		array("label" => "Gaming", "y" => 29),
		array("label" => "Sports", "y" => 88),
		array("label" => "Vehicles", "y" => 21),
		array("label" => "Politics", "y" => 152),
		array("label" => "Food", "y" => 110),
		array("label" => "Fashion", "y" => 11),
		array("label" => "Technology", "y" => 8),
		array("label" => "Books", "y" => 3),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 7)
	);

?>
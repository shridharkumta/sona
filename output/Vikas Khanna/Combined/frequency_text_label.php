<?php 
	$frequency_label = array(
		array("label" => "Food", "y" => 1065),
		array("label" => "Movie", "y" => 1046),
		array("label" => "Politics", "y" => 767),
		array("label" => "Business", "y" => 476),
		array("label" => "Sports", "y" => 300),
		array("label" => "Travel", "y" => 277),
		array("label" => "Music", "y" => 246),
		array("label" => "Medicine", "y" => 223),
		array("label" => "Gaming", "y" => 133),
		array("label" => "Space", "y" => 116),
		array("label" => "Fashion", "y" => 70),
		array("label" => "Vehicles", "y" => 66),
		array("label" => "Technology", "y" => 42),
		array("label" => "Books", "y" => 35),
		array("label" => "Others", "y" => 7),
	);
?>
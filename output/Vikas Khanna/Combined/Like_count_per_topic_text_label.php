<?php 
	$likeCountPerTopic = array(
		array("label" => "Movie", "y" => 5107099),
		array("label" => "Space", "y" => 661336),
		array("label" => "Medicine", "y" => 1100107),
		array("label" => "Travel", "y" => 1252786),
		array("label" => "Music", "y" => 867811),
		array("label" => "Business", "y" => 2586321),
		array("label" => "Gaming", "y" => 485375),
		array("label" => "Sports", "y" => 1142781),
		array("label" => "Vehicles", "y" => 287910),
		array("label" => "Politics", "y" => 3126127),
		array("label" => "Food", "y" => 6933227),
		array("label" => "Fashion", "y" => 499740),
		array("label" => "Technology", "y" => 116609),
		array("label" => "Books", "y" => 132271),
		array("label" => "Others", "y" => 49195)
	);
?>
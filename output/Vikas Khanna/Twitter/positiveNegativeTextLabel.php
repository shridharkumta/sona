<?php 
	$positive = array(
		array("label" => "Travel", "y" => 64),
		array("label" => "Music", "y" => 150),
		array("label" => "Medicine", "y" => 118),
		array("label" => "Food", "y" => 541),
		array("label" => "Business", "y" => 282),
		array("label" => "Space", "y" => 52),
		array("label" => "Sports", "y" => 160),
		array("label" => "Politics", "y" => 480),
		array("label" => "Vehicles", "y" => 30),
		array("label" => "Gaming", "y" => 73),
		array("label" => "Fashion", "y" => 22),
		array("label" => "Movie", "y" => 605),
		array("label" => "Books", "y" => 17),
		array("label" => "Technology", "y" => 26)
	);
	$negative = array(
		array("label" => "Travel", "y" => 91),
		array("label" => "Music", "y" => 28),
		array("label" => "Medicine", "y" => 55),
		array("label" => "Food", "y" => 79),
		array("label" => "Business", "y" => 52),
		array("label" => "Space", "y" => 8),
		array("label" => "Sports", "y" => 72),
		array("label" => "Politics", "y" => 122),
		array("label" => "Vehicles", "y" => 17),
		array("label" => "Gaming", "y" => 21),
		array("label" => "Fashion", "y" => 5),
		array("label" => "Movie", "y" => 85),
		array("label" => "Books", "y" => 2),
		array("label" => "Technology", "y" => 6)
	);
	$neutral = array(
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Technology", "y" => 0)
	);

?>
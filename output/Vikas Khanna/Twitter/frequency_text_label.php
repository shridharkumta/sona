<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 19),
		array("label" => "Business", "y" => 334),
		array("label" => "Fashion", "y" => 27),
		array("label" => "Food", "y" => 620),
		array("label" => "Gaming", "y" => 94),
		array("label" => "Medicine", "y" => 173),
		array("label" => "Movie", "y" => 690),
		array("label" => "Music", "y" => 178),
		array("label" => "Politics", "y" => 602),
		array("label" => "Space", "y" => 60),
		array("label" => "Sports", "y" => 232),
		array("label" => "Technology", "y" => 32),
		array("label" => "Travel", "y" => 155),
		array("label" => "Vehicles", "y" => 47)
	);
?>
<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 3862),
		array("label" => "Business", "y" => 114106),
		array("label" => "Fashion", "y" => 3097),
		array("label" => "Food", "y" => 168117),
		array("label" => "Gaming", "y" => 19371),
		array("label" => "Medicine", "y" => 58550),
		array("label" => "Movie", "y" => 115239),
		array("label" => "Music", "y" => 35633),
		array("label" => "Politics", "y" => 199275),
		array("label" => "Space", "y" => 19867),
		array("label" => "Sports", "y" => 49975),
		array("label" => "Technology", "y" => 4740),
		array("label" => "Travel", "y" => 32618),
		array("label" => "Vehicles", "y" => 15219)
	);
?>
<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 128409),
		array("label" => "Business", "y" => 2472215),
		array("label" => "Fashion", "y" => 496643),
		array("label" => "Food", "y" => 6765110),
		array("label" => "Gaming", "y" => 466004),
		array("label" => "Medicine", "y" => 1041557),
		array("label" => "Movie", "y" => 4991860),
		array("label" => "Music", "y" => 832178),
		array("label" => "Others", "y" => 49195),
		array("label" => "Politics", "y" => 2926852),
		array("label" => "Space", "y" => 641469),
		array("label" => "Sports", "y" => 1092806),
		array("label" => "Technology", "y" => 111869),
		array("label" => "Travel", "y" => 1220168),
		array("label" => "Vehicles", "y" => 272691)
	);
?>
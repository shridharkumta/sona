<?php 
	$positive = array(
		array("label" => "Food", "y" => 414),
		array("label" => "Business", "y" => 127),
		array("label" => "Politics", "y" => 135),
		array("label" => "Vehicles", "y" => 15),
		array("label" => "Space", "y" => 49),
		array("label" => "Movie", "y" => 330),
		array("label" => "Medicine", "y" => 36),
		array("label" => "Fashion", "y" => 37),
		array("label" => "Travel", "y" => 84),
		array("label" => "Sports", "y" => 52),
		array("label" => "Gaming", "y" => 31),
		array("label" => "Music", "y" => 61),
		array("label" => "Technology", "y" => 8),
		array("label" => "Others", "y" => 0),
		array("label" => "Books", "y" => 15)
	);
	$negative = array(
		array("label" => "Food", "y" => 31),
		array("label" => "Business", "y" => 15),
		array("label" => "Politics", "y" => 30),
		array("label" => "Vehicles", "y" => 4),
		array("label" => "Space", "y" => 7),
		array("label" => "Movie", "y" => 26),
		array("label" => "Medicine", "y" => 14),
		array("label" => "Fashion", "y" => 6),
		array("label" => "Travel", "y" => 38),
		array("label" => "Sports", "y" => 16),
		array("label" => "Gaming", "y" => 8),
		array("label" => "Music", "y" => 7),
		array("label" => "Technology", "y" => 2),
		array("label" => "Others", "y" => 0),
		array("label" => "Books", "y" => 1)
	);
	$neutral = array(
		array("label" => "Food", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Others", "y" => 7),
		array("label" => "Books", "y" => 0)
	);

?>
<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 16),
		array("label" => "Business", "y" => 142),
		array("label" => "Fashion", "y" => 43),
		array("label" => "Food", "y" => 445),
		array("label" => "Gaming", "y" => 39),
		array("label" => "Medicine", "y" => 50),
		array("label" => "Movie", "y" => 356),
		array("label" => "Music", "y" => 68),
		array("label" => "Others", "y" => 7),
		array("label" => "Politics", "y" => 165),
		array("label" => "Space", "y" => 56),
		array("label" => "Sports", "y" => 68),
		array("label" => "Technology", "y" => 10),
		array("label" => "Travel", "y" => 122),
		array("label" => "Vehicles", "y" => 19)
	);
?>
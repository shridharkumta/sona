<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 2034),
		array("label" => "Business", "y" => 38703),
		array("label" => "Fashion", "y" => 9268),
		array("label" => "Food", "y" => 291160),
		array("label" => "Gaming", "y" => 10576),
		array("label" => "Medicine", "y" => 7201),
		array("label" => "Movie", "y" => 42292),
		array("label" => "Music", "y" => 9279),
		array("label" => "Others", "y" => 8765),
		array("label" => "Politics", "y" => 13022),
		array("label" => "Space", "y" => 8166),
		array("label" => "Sports", "y" => 32202),
		array("label" => "Technology", "y" => 3110),
		array("label" => "Travel", "y" => 46030),
		array("label" => "Vehicles", "y" => 4564)
	);
?>
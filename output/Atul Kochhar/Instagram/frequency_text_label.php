<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 4),
		array("label" => "Business", "y" => 90),
		array("label" => "Fashion", "y" => 27),
		array("label" => "Food", "y" => 711),
		array("label" => "Gaming", "y" => 30),
		array("label" => "Medicine", "y" => 18),
		array("label" => "Movie", "y" => 117),
		array("label" => "Music", "y" => 29),
		array("label" => "Others", "y" => 30),
		array("label" => "Politics", "y" => 50),
		array("label" => "Space", "y" => 31),
		array("label" => "Sports", "y" => 89),
		array("label" => "Technology", "y" => 8),
		array("label" => "Travel", "y" => 185),
		array("label" => "Vehicles", "y" => 21)
	);
?>
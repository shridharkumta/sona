<?php 
	$positive = array(
		array("label" => "Food", "y" => 672),
		array("label" => "Books", "y" => 4),
		array("label" => "Business", "y" => 84),
		array("label" => "Music", "y" => 27),
		array("label" => "Fashion", "y" => 25),
		array("label" => "Movie", "y" => 111),
		array("label" => "Medicine", "y" => 13),
		array("label" => "Travel", "y" => 150),
		array("label" => "Sports", "y" => 67),
		array("label" => "Vehicles", "y" => 15),
		array("label" => "Others", "y" => 0),
		array("label" => "Technology", "y" => 7),
		array("label" => "Gaming", "y" => 26),
		array("label" => "Politics", "y" => 40),
		array("label" => "Space", "y" => 23)
	);
	$negative = array(
		array("label" => "Food", "y" => 39),
		array("label" => "Books", "y" => 0),
		array("label" => "Business", "y" => 6),
		array("label" => "Music", "y" => 2),
		array("label" => "Fashion", "y" => 2),
		array("label" => "Movie", "y" => 6),
		array("label" => "Medicine", "y" => 5),
		array("label" => "Travel", "y" => 35),
		array("label" => "Sports", "y" => 22),
		array("label" => "Vehicles", "y" => 6),
		array("label" => "Others", "y" => 0),
		array("label" => "Technology", "y" => 1),
		array("label" => "Gaming", "y" => 4),
		array("label" => "Politics", "y" => 10),
		array("label" => "Space", "y" => 8)
	);
	$neutral = array(
		array("label" => "Food", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Others", "y" => 30),
		array("label" => "Technology", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Space", "y" => 0)
	);

?>
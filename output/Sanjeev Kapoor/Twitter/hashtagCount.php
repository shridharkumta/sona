<?php 
	$hashtagCount = array(
		array("label" => "#whatsinseason", "y" => 288),
		array("label" => "#skrecipes", "y" => 123),
		array("label" => "#alltimefavourite", "y" => 87),
		array("label" => "#veganrecipes", "y" => 80),
		array("label" => "#wellnesswednesday", "y" => 76),
		array("label" => "#yumutsav", "y" => 72),
		array("label" => "#streetsaturday", "y" => 71),
		array("label" => "#mondaymotivation", "y" => 70),
		array("label" => "#tuesdaytip", "y" => 68),
		array("label" => "#sundayfunday", "y" => 63),
	);
?>
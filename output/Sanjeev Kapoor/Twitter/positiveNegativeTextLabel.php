<?php 
	$positive = array(
		array("label" => "Business", "y" => 107),
		array("label" => "Food", "y" => 2484),
		array("label" => "Music", "y" => 37),
		array("label" => "Politics", "y" => 196),
		array("label" => "Medicine", "y" => 63),
		array("label" => "Vehicles", "y" => 2),
		array("label" => "Travel", "y" => 28),
		array("label" => "Movie", "y" => 40),
		array("label" => "Fashion", "y" => 6),
		array("label" => "Gaming", "y" => 36),
		array("label" => "Technology", "y" => 8),
		array("label" => "Sports", "y" => 23),
		array("label" => "Space", "y" => 4),
		array("label" => "Books", "y" => 1)
	);
	$negative = array(
		array("label" => "Business", "y" => 42),
		array("label" => "Food", "y" => 46),
		array("label" => "Music", "y" => 1),
		array("label" => "Politics", "y" => 14),
		array("label" => "Medicine", "y" => 11),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Travel", "y" => 10),
		array("label" => "Movie", "y" => 11),
		array("label" => "Fashion", "y" => 3),
		array("label" => "Gaming", "y" => 1),
		array("label" => "Technology", "y" => 0),
		array("label" => "Sports", "y" => 3),
		array("label" => "Space", "y" => 0),
		array("label" => "Books", "y" => 0)
	);
	$neutral = array(
		array("label" => "Business", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Books", "y" => 0)
	);

?>
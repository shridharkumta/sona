<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 1),
		array("label" => "Business", "y" => 149),
		array("label" => "Fashion", "y" => 9),
		array("label" => "Food", "y" => 2530),
		array("label" => "Gaming", "y" => 37),
		array("label" => "Medicine", "y" => 74),
		array("label" => "Movie", "y" => 51),
		array("label" => "Music", "y" => 38),
		array("label" => "Politics", "y" => 210),
		array("label" => "Space", "y" => 4),
		array("label" => "Sports", "y" => 26),
		array("label" => "Technology", "y" => 8),
		array("label" => "Travel", "y" => 38),
		array("label" => "Vehicles", "y" => 2)
	);
?>
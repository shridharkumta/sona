<?php 
	$positive = array(
		array("label" => "Food", "y" => 3481),
		array("label" => "Vehicles", "y" => 23),
		array("label" => "Business", "y" => 231),
		array("label" => "Politics", "y" => 183),
		array("label" => "Movie", "y" => 258),
		array("label" => "Travel", "y" => 296),
		array("label" => "Fashion", "y" => 34),
		array("label" => "Medicine", "y" => 60),
		array("label" => "Space", "y" => 13),
		array("label" => "Music", "y" => 64),
		array("label" => "Gaming", "y" => 48),
		array("label" => "Sports", "y" => 105),
		array("label" => "Books", "y" => 13),
		array("label" => "Technology", "y" => 19),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Food", "y" => 99),
		array("label" => "Vehicles", "y" => 6),
		array("label" => "Business", "y" => 38),
		array("label" => "Politics", "y" => 29),
		array("label" => "Movie", "y" => 27),
		array("label" => "Travel", "y" => 108),
		array("label" => "Fashion", "y" => 18),
		array("label" => "Medicine", "y" => 15),
		array("label" => "Space", "y" => 6),
		array("label" => "Music", "y" => 11),
		array("label" => "Gaming", "y" => 9),
		array("label" => "Sports", "y" => 22),
		array("label" => "Books", "y" => 2),
		array("label" => "Technology", "y" => 12),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Food", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Others", "y" => 20)
	);

?>
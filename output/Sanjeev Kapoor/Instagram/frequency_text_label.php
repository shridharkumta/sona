<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 15),
		array("label" => "Business", "y" => 269),
		array("label" => "Fashion", "y" => 52),
		array("label" => "Food", "y" => 3580),
		array("label" => "Gaming", "y" => 57),
		array("label" => "Medicine", "y" => 75),
		array("label" => "Movie", "y" => 285),
		array("label" => "Music", "y" => 75),
		array("label" => "Others", "y" => 20),
		array("label" => "Politics", "y" => 212),
		array("label" => "Space", "y" => 19),
		array("label" => "Sports", "y" => 127),
		array("label" => "Technology", "y" => 31),
		array("label" => "Travel", "y" => 404),
		array("label" => "Vehicles", "y" => 29)
	);
?>
<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 23911),
		array("label" => "Business", "y" => 600063),
		array("label" => "Fashion", "y" => 68355),
		array("label" => "Food", "y" => 6767342),
		array("label" => "Gaming", "y" => 117542),
		array("label" => "Medicine", "y" => 158174),
		array("label" => "Movie", "y" => 726546),
		array("label" => "Music", "y" => 149307),
		array("label" => "Others", "y" => 8583),
		array("label" => "Politics", "y" => 568150),
		array("label" => "Space", "y" => 29664),
		array("label" => "Sports", "y" => 149028),
		array("label" => "Technology", "y" => 8182),
		array("label" => "Travel", "y" => 622600),
		array("label" => "Vehicles", "y" => 41207)
	);
?>
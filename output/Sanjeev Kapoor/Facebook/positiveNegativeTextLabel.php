<?php 
	$positive = array(
		array("label" => "Business", "y" => 190),
		array("label" => "Food", "y" => 4408),
		array("label" => "Politics", "y" => 251),
		array("label" => "Medicine", "y" => 56),
		array("label" => "Vehicles", "y" => 21),
		array("label" => "Movie", "y" => 618),
		array("label" => "Travel", "y" => 183),
		array("label" => "Fashion", "y" => 38),
		array("label" => "Gaming", "y" => 74),
		array("label" => "Music", "y" => 54),
		array("label" => "Others", "y" => 0),
		array("label" => "Sports", "y" => 117),
		array("label" => "Space", "y" => 22),
		array("label" => "Technology", "y" => 23),
		array("label" => "Books", "y" => 8)
	);
	$negative = array(
		array("label" => "Business", "y" => 55),
		array("label" => "Food", "y" => 160),
		array("label" => "Politics", "y" => 47),
		array("label" => "Medicine", "y" => 15),
		array("label" => "Vehicles", "y" => 12),
		array("label" => "Movie", "y" => 49),
		array("label" => "Travel", "y" => 157),
		array("label" => "Fashion", "y" => 10),
		array("label" => "Gaming", "y" => 9),
		array("label" => "Music", "y" => 3),
		array("label" => "Others", "y" => 0),
		array("label" => "Sports", "y" => 32),
		array("label" => "Space", "y" => 11),
		array("label" => "Technology", "y" => 4),
		array("label" => "Books", "y" => 1)
	);
	$neutral = array(
		array("label" => "Business", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Others", "y" => 207),
		array("label" => "Sports", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Books", "y" => 0)
	);

?>
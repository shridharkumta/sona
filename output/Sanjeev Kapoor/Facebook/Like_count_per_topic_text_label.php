<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 37424),
		array("label" => "Business", "y" => 694022),
		array("label" => "Fashion", "y" => 104065),
		array("label" => "Food", "y" => 10894317),
		array("label" => "Gaming", "y" => 315402),
		array("label" => "Medicine", "y" => 89741),
		array("label" => "Movie", "y" => 2024175),
		array("label" => "Music", "y" => 500779),
		array("label" => "Others", "y" => 2130576),
		array("label" => "Politics", "y" => 990854),
		array("label" => "Space", "y" => 65866),
		array("label" => "Sports", "y" => 711372),
		array("label" => "Technology", "y" => 86852),
		array("label" => "Travel", "y" => 1765035),
		array("label" => "Vehicles", "y" => 120558)
	);
?>
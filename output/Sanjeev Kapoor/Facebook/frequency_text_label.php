<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 9),
		array("label" => "Business", "y" => 245),
		array("label" => "Fashion", "y" => 48),
		array("label" => "Food", "y" => 4568),
		array("label" => "Gaming", "y" => 83),
		array("label" => "Medicine", "y" => 71),
		array("label" => "Movie", "y" => 667),
		array("label" => "Music", "y" => 57),
		array("label" => "Others", "y" => 207),
		array("label" => "Politics", "y" => 298),
		array("label" => "Space", "y" => 33),
		array("label" => "Sports", "y" => 149),
		array("label" => "Technology", "y" => 27),
		array("label" => "Travel", "y" => 340),
		array("label" => "Vehicles", "y" => 33)
	);
?>
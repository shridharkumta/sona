<?php 
	$hashtagCount = array(
		array("label" => "#foodlove", "y" => 184),
		array("label" => "#cooking", "y" => 163),
		array("label" => "#whatsinseason", "y" => 146),
		array("label" => "#foodie", "y" => 136),
		array("label" => "#tipoftheday", "y" => 128),
		array("label" => "#recipes", "y" => 118),
		array("label" => "#namakswadanusar", "y" => 113),
		array("label" => "#indiancuisine", "y" => 98),
		array("label" => "#food", "y" => 91),
		array("label" => "#recipe", "y" => 73),
	);
?>
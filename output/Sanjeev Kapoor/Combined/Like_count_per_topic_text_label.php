<?php 
	$likeCountPerTopic = array(
		array("label" => "Movie", "y" => 2756348),
		array("label" => "Space", "y" => 95762),
		array("label" => "Medicine", "y" => 256073),
		array("label" => "Travel", "y" => 2391741),
		array("label" => "Music", "y" => 653793),
		array("label" => "Business", "y" => 1309237),
		array("label" => "Gaming", "y" => 436520),
		array("label" => "Sports", "y" => 863015),
		array("label" => "Vehicles", "y" => 161943),
		array("label" => "Politics", "y" => 1581275),
		array("label" => "Food", "y" => 17909385),
		array("label" => "Fashion", "y" => 173622),
		array("label" => "Technology", "y" => 96510),
		array("label" => "Books", "y" => 61455),
		array("label" => "Others", "y" => 2139159)
	);
?>
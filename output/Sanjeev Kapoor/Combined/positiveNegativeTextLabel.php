<?php 
	$positive = array(
		array("label" => "Movie", "y" => 916),
		array("label" => "Space", "y" => 39),
		array("label" => "Medicine", "y" => 179),
		array("label" => "Travel", "y" => 507),
		array("label" => "Music", "y" => 155),
		array("label" => "Business", "y" => 528),
		array("label" => "Gaming", "y" => 158),
		array("label" => "Sports", "y" => 245),
		array("label" => "Vehicles", "y" => 46),
		array("label" => "Politics", "y" => 630),
		array("label" => "Food", "y" => 10373),
		array("label" => "Fashion", "y" => 78),
		array("label" => "Technology", "y" => 50),
		array("label" => "Books", "y" => 22),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Movie", "y" => 87),
		array("label" => "Space", "y" => 17),
		array("label" => "Medicine", "y" => 41),
		array("label" => "Travel", "y" => 275),
		array("label" => "Music", "y" => 15),
		array("label" => "Business", "y" => 135),
		array("label" => "Gaming", "y" => 19),
		array("label" => "Sports", "y" => 57),
		array("label" => "Vehicles", "y" => 18),
		array("label" => "Politics", "y" => 90),
		array("label" => "Food", "y" => 305),
		array("label" => "Fashion", "y" => 31),
		array("label" => "Technology", "y" => 16),
		array("label" => "Books", "y" => 3),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 227)
	);

?>
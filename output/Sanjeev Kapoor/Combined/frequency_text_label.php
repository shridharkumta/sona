<?php 
	$frequency_label = array(
		array("label" => "Food", "y" => 10678),
		array("label" => "Movie", "y" => 1003),
		array("label" => "Travel", "y" => 782),
		array("label" => "Politics", "y" => 720),
		array("label" => "Business", "y" => 663),
		array("label" => "Sports", "y" => 302),
		array("label" => "Others", "y" => 227),
		array("label" => "Medicine", "y" => 220),
		array("label" => "Gaming", "y" => 177),
		array("label" => "Music", "y" => 170),
		array("label" => "Fashion", "y" => 109),
		array("label" => "Technology", "y" => 66),
		array("label" => "Vehicles", "y" => 64),
		array("label" => "Space", "y" => 56),
		array("label" => "Books", "y" => 25),
	);
?>
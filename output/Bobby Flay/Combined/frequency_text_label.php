<?php 
	$frequency_label = array(
		array("label" => "Food", "y" => 3131),
		array("label" => "Movie", "y" => 348),
		array("label" => "Music", "y" => 274),
		array("label" => "Sports", "y" => 240),
		array("label" => "Business", "y" => 175),
		array("label" => "Gaming", "y" => 129),
		array("label" => "Travel", "y" => 101),
		array("label" => "Fashion", "y" => 74),
		array("label" => "Medicine", "y" => 70),
		array("label" => "Space", "y" => 56),
		array("label" => "Politics", "y" => 45),
		array("label" => "Vehicles", "y" => 38),
		array("label" => "Books", "y" => 20),
		array("label" => "Technology", "y" => 14),
		array("label" => "Others", "y" => 7),
	);
?>
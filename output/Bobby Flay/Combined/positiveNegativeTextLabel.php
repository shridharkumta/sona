<?php 
	$positive = array(
		array("label" => "Movie", "y" => 302),
		array("label" => "Space", "y" => 38),
		array("label" => "Medicine", "y" => 46),
		array("label" => "Travel", "y" => 60),
		array("label" => "Music", "y" => 231),
		array("label" => "Business", "y" => 148),
		array("label" => "Gaming", "y" => 100),
		array("label" => "Sports", "y" => 153),
		array("label" => "Vehicles", "y" => 18),
		array("label" => "Politics", "y" => 30),
		array("label" => "Food", "y" => 2797),
		array("label" => "Fashion", "y" => 55),
		array("label" => "Technology", "y" => 10),
		array("label" => "Books", "y" => 16),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Movie", "y" => 46),
		array("label" => "Space", "y" => 18),
		array("label" => "Medicine", "y" => 24),
		array("label" => "Travel", "y" => 41),
		array("label" => "Music", "y" => 43),
		array("label" => "Business", "y" => 27),
		array("label" => "Gaming", "y" => 29),
		array("label" => "Sports", "y" => 87),
		array("label" => "Vehicles", "y" => 20),
		array("label" => "Politics", "y" => 15),
		array("label" => "Food", "y" => 334),
		array("label" => "Fashion", "y" => 19),
		array("label" => "Technology", "y" => 4),
		array("label" => "Books", "y" => 4),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 7)
	);

?>
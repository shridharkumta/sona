<?php 
	$likeCountPerTopic = array(
		array("label" => "Movie", "y" => 1449436),
		array("label" => "Space", "y" => 96587),
		array("label" => "Medicine", "y" => 141946),
		array("label" => "Travel", "y" => 402853),
		array("label" => "Music", "y" => 410219),
		array("label" => "Business", "y" => 358186),
		array("label" => "Gaming", "y" => 249832),
		array("label" => "Sports", "y" => 288549),
		array("label" => "Vehicles", "y" => 66496),
		array("label" => "Politics", "y" => 139184),
		array("label" => "Food", "y" => 11471166),
		array("label" => "Fashion", "y" => 439532),
		array("label" => "Technology", "y" => 40796),
		array("label" => "Books", "y" => 44687),
		array("label" => "Others", "y" => 163004)
	);
?>
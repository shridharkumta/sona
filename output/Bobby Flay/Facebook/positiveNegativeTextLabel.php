<?php 
	$positive = array(
		array("label" => "Movie", "y" => 8),
		array("label" => "Food", "y" => 103),
		array("label" => "Business", "y" => 2),
		array("label" => "Medicine", "y" => 2),
		array("label" => "Music", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Sports", "y" => 1),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Movie", "y" => 0),
		array("label" => "Food", "y" => 3),
		array("label" => "Business", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Music", "y" => 2),
		array("label" => "Vehicles", "y" => 1),
		array("label" => "Sports", "y" => 1),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Movie", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Others", "y" => 1)
	);

?>
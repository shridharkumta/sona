<?php 
	$positive = array(
		array("label" => "Food", "y" => 1756),
		array("label" => "Music", "y" => 189),
		array("label" => "Sports", "y" => 129),
		array("label" => "Business", "y" => 120),
		array("label" => "Movie", "y" => 205),
		array("label" => "Gaming", "y" => 82),
		array("label" => "Fashion", "y" => 37),
		array("label" => "Travel", "y" => 38),
		array("label" => "Politics", "y" => 26),
		array("label" => "Medicine", "y" => 34),
		array("label" => "Space", "y" => 29),
		array("label" => "Vehicles", "y" => 13),
		array("label" => "Technology", "y" => 9),
		array("label" => "Books", "y" => 15),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Food", "y" => 241),
		array("label" => "Music", "y" => 35),
		array("label" => "Sports", "y" => 75),
		array("label" => "Business", "y" => 21),
		array("label" => "Movie", "y" => 34),
		array("label" => "Gaming", "y" => 21),
		array("label" => "Fashion", "y" => 13),
		array("label" => "Travel", "y" => 34),
		array("label" => "Politics", "y" => 14),
		array("label" => "Medicine", "y" => 21),
		array("label" => "Space", "y" => 16),
		array("label" => "Vehicles", "y" => 15),
		array("label" => "Technology", "y" => 2),
		array("label" => "Books", "y" => 3),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Food", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 3)
	);

?>
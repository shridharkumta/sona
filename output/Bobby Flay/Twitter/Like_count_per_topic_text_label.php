<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 1095),
		array("label" => "Business", "y" => 9819),
		array("label" => "Fashion", "y" => 5507),
		array("label" => "Food", "y" => 199029),
		array("label" => "Gaming", "y" => 7987),
		array("label" => "Medicine", "y" => 4616),
		array("label" => "Movie", "y" => 23455),
		array("label" => "Music", "y" => 37446),
		array("label" => "Others", "y" => 5),
		array("label" => "Politics", "y" => 5496),
		array("label" => "Space", "y" => 2093),
		array("label" => "Sports", "y" => 15253),
		array("label" => "Technology", "y" => 549),
		array("label" => "Travel", "y" => 7042),
		array("label" => "Vehicles", "y" => 1820)
	);
?>
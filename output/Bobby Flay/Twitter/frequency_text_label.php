<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 18),
		array("label" => "Business", "y" => 141),
		array("label" => "Fashion", "y" => 50),
		array("label" => "Food", "y" => 1997),
		array("label" => "Gaming", "y" => 103),
		array("label" => "Medicine", "y" => 55),
		array("label" => "Movie", "y" => 239),
		array("label" => "Music", "y" => 224),
		array("label" => "Others", "y" => 3),
		array("label" => "Politics", "y" => 40),
		array("label" => "Space", "y" => 45),
		array("label" => "Sports", "y" => 204),
		array("label" => "Technology", "y" => 11),
		array("label" => "Travel", "y" => 72),
		array("label" => "Vehicles", "y" => 28)
	);
?>
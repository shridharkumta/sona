<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 2),
		array("label" => "Business", "y" => 32),
		array("label" => "Fashion", "y" => 24),
		array("label" => "Food", "y" => 1028),
		array("label" => "Gaming", "y" => 26),
		array("label" => "Medicine", "y" => 13),
		array("label" => "Movie", "y" => 101),
		array("label" => "Music", "y" => 48),
		array("label" => "Others", "y" => 3),
		array("label" => "Politics", "y" => 5),
		array("label" => "Space", "y" => 11),
		array("label" => "Sports", "y" => 34),
		array("label" => "Technology", "y" => 3),
		array("label" => "Travel", "y" => 29),
		array("label" => "Vehicles", "y" => 9)
	);
?>
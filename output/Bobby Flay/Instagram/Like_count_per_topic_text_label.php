<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 43592),
		array("label" => "Business", "y" => 344230),
		array("label" => "Fashion", "y" => 434025),
		array("label" => "Food", "y" => 11070972),
		array("label" => "Gaming", "y" => 241845),
		array("label" => "Medicine", "y" => 135686),
		array("label" => "Movie", "y" => 1408254),
		array("label" => "Music", "y" => 372245),
		array("label" => "Others", "y" => 161951),
		array("label" => "Politics", "y" => 133688),
		array("label" => "Space", "y" => 94494),
		array("label" => "Sports", "y" => 272331),
		array("label" => "Technology", "y" => 40247),
		array("label" => "Travel", "y" => 395811),
		array("label" => "Vehicles", "y" => 60194)
	);
?>
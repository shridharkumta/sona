<?php 
	$positive = array(
		array("label" => "Food", "y" => 938),
		array("label" => "Fashion", "y" => 18),
		array("label" => "Movie", "y" => 89),
		array("label" => "Medicine", "y" => 10),
		array("label" => "Politics", "y" => 4),
		array("label" => "Music", "y" => 42),
		array("label" => "Others", "y" => 0),
		array("label" => "Business", "y" => 26),
		array("label" => "Gaming", "y" => 18),
		array("label" => "Travel", "y" => 22),
		array("label" => "Sports", "y" => 23),
		array("label" => "Books", "y" => 1),
		array("label" => "Space", "y" => 9),
		array("label" => "Vehicles", "y" => 5),
		array("label" => "Technology", "y" => 1)
	);
	$negative = array(
		array("label" => "Food", "y" => 90),
		array("label" => "Fashion", "y" => 6),
		array("label" => "Movie", "y" => 12),
		array("label" => "Medicine", "y" => 3),
		array("label" => "Politics", "y" => 1),
		array("label" => "Music", "y" => 6),
		array("label" => "Others", "y" => 0),
		array("label" => "Business", "y" => 6),
		array("label" => "Gaming", "y" => 8),
		array("label" => "Travel", "y" => 7),
		array("label" => "Sports", "y" => 11),
		array("label" => "Books", "y" => 1),
		array("label" => "Space", "y" => 2),
		array("label" => "Vehicles", "y" => 4),
		array("label" => "Technology", "y" => 2)
	);
	$neutral = array(
		array("label" => "Food", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Others", "y" => 3),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Technology", "y" => 0)
	);

?>
<?php 
	$likeCountPerTopic = array(
		array("label" => "Movie", "y" => 329930),
		array("label" => "Space", "y" => 349403),
		array("label" => "Medicine", "y" => 383735),
		array("label" => "Travel", "y" => 12467674),
		array("label" => "Music", "y" => 142186),
		array("label" => "Business", "y" => 428451),
		array("label" => "Gaming", "y" => 228677),
		array("label" => "Sports", "y" => 1079378),
		array("label" => "Vehicles", "y" => 1416853),
		array("label" => "Politics", "y" => 23800168),
		array("label" => "Food", "y" => 110895),
		array("label" => "Fashion", "y" => 187644),
		array("label" => "Technology", "y" => 336952),
		array("label" => "Books", "y" => 25068),
		array("label" => "Others", "y" => 27204694)
	);
?>
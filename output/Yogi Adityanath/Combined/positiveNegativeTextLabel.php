<?php 
	$positive = array(
		array("label" => "Movie", "y" => 6),
		array("label" => "Space", "y" => 9),
		array("label" => "Medicine", "y" => 8),
		array("label" => "Travel", "y" => 15),
		array("label" => "Music", "y" => 4),
		array("label" => "Business", "y" => 13),
		array("label" => "Gaming", "y" => 5),
		array("label" => "Sports", "y" => 29),
		array("label" => "Vehicles", "y" => 1),
		array("label" => "Politics", "y" => 368),
		array("label" => "Food", "y" => 3),
		array("label" => "Technology", "y" => 9),
		array("label" => "Fashion", "y" => 1),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Movie", "y" => 26),
		array("label" => "Space", "y" => 20),
		array("label" => "Medicine", "y" => 20),
		array("label" => "Travel", "y" => 1497),
		array("label" => "Music", "y" => 14),
		array("label" => "Business", "y" => 34),
		array("label" => "Gaming", "y" => 19),
		array("label" => "Sports", "y" => 65),
		array("label" => "Vehicles", "y" => 154),
		array("label" => "Politics", "y" => 1438),
		array("label" => "Food", "y" => 10),
		array("label" => "Technology", "y" => 31),
		array("label" => "Fashion", "y" => 12),
		array("label" => "Books", "y" => 3),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 2508)
	);

?>
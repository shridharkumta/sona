<?php 
	$frequency_label = array(
		array("label" => "Others", "y" => 2508),
		array("label" => "Politics", "y" => 1806),
		array("label" => "Travel", "y" => 1512),
		array("label" => "Vehicles", "y" => 155),
		array("label" => "Sports", "y" => 94),
		array("label" => "Business", "y" => 47),
		array("label" => "Technology", "y" => 40),
		array("label" => "Movie", "y" => 32),
		array("label" => "Space", "y" => 29),
		array("label" => "Medicine", "y" => 28),
		array("label" => "Gaming", "y" => 24),
		array("label" => "Music", "y" => 18),
		array("label" => "Food", "y" => 13),
		array("label" => "Fashion", "y" => 13),
		array("label" => "Books", "y" => 3),
	);
?>
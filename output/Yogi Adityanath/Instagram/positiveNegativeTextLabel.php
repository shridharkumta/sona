<?php 
	$positive = array(
		array("label" => "Politics", "y" => 46),
		array("label" => "Others", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Travel", "y" => 4),
		array("label" => "Technology", "y" => 7),
		array("label" => "Medicine", "y" => 2),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 13),
		array("label" => "Movie", "y" => 1),
		array("label" => "Space", "y" => 0),
		array("label" => "Business", "y" => 2),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Food", "y" => 1),
		array("label" => "Music", "y" => 0)
	);
	$negative = array(
		array("label" => "Politics", "y" => 808),
		array("label" => "Others", "y" => 0),
		array("label" => "Vehicles", "y" => 100),
		array("label" => "Travel", "y" => 192),
		array("label" => "Technology", "y" => 14),
		array("label" => "Medicine", "y" => 3),
		array("label" => "Gaming", "y" => 9),
		array("label" => "Sports", "y" => 25),
		array("label" => "Movie", "y" => 10),
		array("label" => "Space", "y" => 10),
		array("label" => "Business", "y" => 14),
		array("label" => "Fashion", "y" => 6),
		array("label" => "Books", "y" => 2),
		array("label" => "Food", "y" => 5),
		array("label" => "Music", "y" => 9)
	);
	$neutral = array(
		array("label" => "Politics", "y" => 0),
		array("label" => "Others", "y" => 1801),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Music", "y" => 0)
	);

?>
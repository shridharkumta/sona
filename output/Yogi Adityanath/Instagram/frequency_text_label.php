<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 2),
		array("label" => "Business", "y" => 16),
		array("label" => "Fashion", "y" => 6),
		array("label" => "Food", "y" => 6),
		array("label" => "Gaming", "y" => 9),
		array("label" => "Medicine", "y" => 5),
		array("label" => "Movie", "y" => 11),
		array("label" => "Music", "y" => 9),
		array("label" => "Others", "y" => 1801),
		array("label" => "Politics", "y" => 854),
		array("label" => "Space", "y" => 10),
		array("label" => "Sports", "y" => 38),
		array("label" => "Technology", "y" => 21),
		array("label" => "Travel", "y" => 196),
		array("label" => "Vehicles", "y" => 100)
	);
?>
<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 18586),
		array("label" => "Business", "y" => 138612),
		array("label" => "Fashion", "y" => 69112),
		array("label" => "Food", "y" => 33716),
		array("label" => "Gaming", "y" => 112170),
		array("label" => "Medicine", "y" => 53611),
		array("label" => "Movie", "y" => 128815),
		array("label" => "Music", "y" => 58548),
		array("label" => "Others", "y" => 19126324),
		array("label" => "Politics", "y" => 13473433),
		array("label" => "Space", "y" => 121601),
		array("label" => "Sports", "y" => 425646),
		array("label" => "Technology", "y" => 170349),
		array("label" => "Travel", "y" => 2299406),
		array("label" => "Vehicles", "y" => 995199)
	);
?>
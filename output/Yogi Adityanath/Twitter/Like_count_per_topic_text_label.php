<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 6482),
		array("label" => "Business", "y" => 289839),
		array("label" => "Fashion", "y" => 118532),
		array("label" => "Food", "y" => 77179),
		array("label" => "Gaming", "y" => 116507),
		array("label" => "Medicine", "y" => 330124),
		array("label" => "Movie", "y" => 201115),
		array("label" => "Music", "y" => 83638),
		array("label" => "Others", "y" => 8078370),
		array("label" => "Politics", "y" => 10326735),
		array("label" => "Space", "y" => 227802),
		array("label" => "Sports", "y" => 653732),
		array("label" => "Technology", "y" => 166603),
		array("label" => "Travel", "y" => 10168268),
		array("label" => "Vehicles", "y" => 421654)
	);
?>
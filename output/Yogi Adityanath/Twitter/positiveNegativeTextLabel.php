<?php 
	$positive = array(
		array("label" => "Politics", "y" => 322),
		array("label" => "Others", "y" => 0),
		array("label" => "Travel", "y" => 11),
		array("label" => "Sports", "y" => 16),
		array("label" => "Fashion", "y" => 1),
		array("label" => "Movie", "y" => 5),
		array("label" => "Business", "y" => 11),
		array("label" => "Vehicles", "y" => 1),
		array("label" => "Space", "y" => 9),
		array("label" => "Medicine", "y" => 6),
		array("label" => "Gaming", "y" => 5),
		array("label" => "Technology", "y" => 2),
		array("label" => "Music", "y" => 4),
		array("label" => "Food", "y" => 2),
		array("label" => "Books", "y" => 0)
	);
	$negative = array(
		array("label" => "Politics", "y" => 630),
		array("label" => "Others", "y" => 0),
		array("label" => "Travel", "y" => 1305),
		array("label" => "Sports", "y" => 40),
		array("label" => "Fashion", "y" => 6),
		array("label" => "Movie", "y" => 16),
		array("label" => "Business", "y" => 20),
		array("label" => "Vehicles", "y" => 54),
		array("label" => "Space", "y" => 10),
		array("label" => "Medicine", "y" => 17),
		array("label" => "Gaming", "y" => 10),
		array("label" => "Technology", "y" => 17),
		array("label" => "Music", "y" => 5),
		array("label" => "Food", "y" => 5),
		array("label" => "Books", "y" => 1)
	);
	$neutral = array(
		array("label" => "Politics", "y" => 0),
		array("label" => "Others", "y" => 707),
		array("label" => "Travel", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Books", "y" => 0)
	);

?>
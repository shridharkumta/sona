<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 1),
		array("label" => "Business", "y" => 31),
		array("label" => "Fashion", "y" => 7),
		array("label" => "Food", "y" => 7),
		array("label" => "Gaming", "y" => 15),
		array("label" => "Medicine", "y" => 23),
		array("label" => "Movie", "y" => 21),
		array("label" => "Music", "y" => 9),
		array("label" => "Others", "y" => 707),
		array("label" => "Politics", "y" => 952),
		array("label" => "Space", "y" => 19),
		array("label" => "Sports", "y" => 56),
		array("label" => "Technology", "y" => 19),
		array("label" => "Travel", "y" => 1316),
		array("label" => "Vehicles", "y" => 55)
	);
?>
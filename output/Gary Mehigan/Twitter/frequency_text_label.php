<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 13),
		array("label" => "Business", "y" => 139),
		array("label" => "Fashion", "y" => 54),
		array("label" => "Food", "y" => 1653),
		array("label" => "Gaming", "y" => 154),
		array("label" => "Medicine", "y" => 63),
		array("label" => "Movie", "y" => 326),
		array("label" => "Music", "y" => 243),
		array("label" => "Politics", "y" => 74),
		array("label" => "Space", "y" => 70),
		array("label" => "Sports", "y" => 186),
		array("label" => "Technology", "y" => 23),
		array("label" => "Travel", "y" => 180),
		array("label" => "Vehicles", "y" => 52)
	);
?>
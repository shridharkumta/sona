<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 133),
		array("label" => "Business", "y" => 1322),
		array("label" => "Fashion", "y" => 444),
		array("label" => "Food", "y" => 19529),
		array("label" => "Gaming", "y" => 1234),
		array("label" => "Medicine", "y" => 496),
		array("label" => "Movie", "y" => 2969),
		array("label" => "Music", "y" => 2147),
		array("label" => "Politics", "y" => 618),
		array("label" => "Space", "y" => 681),
		array("label" => "Sports", "y" => 1323),
		array("label" => "Technology", "y" => 115),
		array("label" => "Travel", "y" => 2485),
		array("label" => "Vehicles", "y" => 336)
	);
?>
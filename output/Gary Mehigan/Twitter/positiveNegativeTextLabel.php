<?php 
	$positive = array(
		array("label" => "Food", "y" => 1438),
		array("label" => "Movie", "y" => 271),
		array("label" => "Business", "y" => 112),
		array("label" => "Sports", "y" => 131),
		array("label" => "Music", "y" => 191),
		array("label" => "Travel", "y" => 138),
		array("label" => "Medicine", "y" => 37),
		array("label" => "Gaming", "y" => 109),
		array("label" => "Vehicles", "y" => 35),
		array("label" => "Politics", "y" => 44),
		array("label" => "Space", "y" => 50),
		array("label" => "Fashion", "y" => 42),
		array("label" => "Books", "y" => 8),
		array("label" => "Technology", "y" => 18)
	);
	$negative = array(
		array("label" => "Food", "y" => 215),
		array("label" => "Movie", "y" => 55),
		array("label" => "Business", "y" => 27),
		array("label" => "Sports", "y" => 55),
		array("label" => "Music", "y" => 52),
		array("label" => "Travel", "y" => 42),
		array("label" => "Medicine", "y" => 26),
		array("label" => "Gaming", "y" => 45),
		array("label" => "Vehicles", "y" => 17),
		array("label" => "Politics", "y" => 30),
		array("label" => "Space", "y" => 20),
		array("label" => "Fashion", "y" => 12),
		array("label" => "Books", "y" => 5),
		array("label" => "Technology", "y" => 5)
	);
	$neutral = array(
		array("label" => "Food", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Technology", "y" => 0)
	);

?>
<?php 
	$hashtagCount = array(
		array("label" => "#masterchefau", "y" => 639),
		array("label" => "#delicious", "y" => 479),
		array("label" => "#yum", "y" => 389),
		array("label" => "#foodies", "y" => 272),
		array("label" => "#foodiesofinstagram", "y" => 257),
		array("label" => "#chefslife", "y" => 241),
		array("label" => "#lovefood", "y" => 215),
		array("label" => "#deliciousfood", "y" => 178),
		array("label" => "#yummy", "y" => 157),
		array("label" => "#homecooked", "y" => 127),
	);
?>
<?php 
	$frequency_label = array(
		array("label" => "Food", "y" => 2880),
		array("label" => "Movie", "y" => 486),
		array("label" => "Sports", "y" => 303),
		array("label" => "Travel", "y" => 293),
		array("label" => "Music", "y" => 286),
		array("label" => "Business", "y" => 207),
		array("label" => "Gaming", "y" => 167),
		array("label" => "Vehicles", "y" => 89),
		array("label" => "Politics", "y" => 86),
		array("label" => "Space", "y" => 76),
		array("label" => "Medicine", "y" => 72),
		array("label" => "Fashion", "y" => 66),
		array("label" => "Others", "y" => 50),
		array("label" => "Technology", "y" => 24),
		array("label" => "Books", "y" => 19),
	);
?>
<?php 
	$likeCountPerTopic = array(
		array("label" => "Movie", "y" => 192857),
		array("label" => "Space", "y" => 18532),
		array("label" => "Medicine", "y" => 7941),
		array("label" => "Travel", "y" => 302800),
		array("label" => "Music", "y" => 33197),
		array("label" => "Business", "y" => 137312),
		array("label" => "Gaming", "y" => 28161),
		array("label" => "Sports", "y" => 136560),
		array("label" => "Vehicles", "y" => 107579),
		array("label" => "Politics", "y" => 23419),
		array("label" => "Food", "y" => 3947929),
		array("label" => "Fashion", "y" => 12154),
		array("label" => "Technology", "y" => 166),
		array("label" => "Books", "y" => 50874),
		array("label" => "Others", "y" => 71206)
	);
?>
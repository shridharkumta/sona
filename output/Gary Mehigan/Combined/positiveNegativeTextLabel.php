<?php 
	$positive = array(
		array("label" => "Movie", "y" => 414),
		array("label" => "Space", "y" => 54),
		array("label" => "Medicine", "y" => 41),
		array("label" => "Travel", "y" => 233),
		array("label" => "Music", "y" => 226),
		array("label" => "Business", "y" => 175),
		array("label" => "Gaming", "y" => 120),
		array("label" => "Sports", "y" => 227),
		array("label" => "Vehicles", "y" => 58),
		array("label" => "Politics", "y" => 52),
		array("label" => "Food", "y" => 2620),
		array("label" => "Fashion", "y" => 51),
		array("label" => "Technology", "y" => 19),
		array("label" => "Books", "y" => 14),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Movie", "y" => 72),
		array("label" => "Space", "y" => 22),
		array("label" => "Medicine", "y" => 31),
		array("label" => "Travel", "y" => 60),
		array("label" => "Music", "y" => 60),
		array("label" => "Business", "y" => 32),
		array("label" => "Gaming", "y" => 47),
		array("label" => "Sports", "y" => 76),
		array("label" => "Vehicles", "y" => 31),
		array("label" => "Politics", "y" => 34),
		array("label" => "Food", "y" => 260),
		array("label" => "Fashion", "y" => 15),
		array("label" => "Technology", "y" => 5),
		array("label" => "Books", "y" => 5),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 50)
	);

?>
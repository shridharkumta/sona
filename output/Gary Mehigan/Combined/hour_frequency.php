<?php 
	$hour_frequency = array(
		array("label" => "0:00 - 1:00", "y" => 116),
		array("label" => "1:00 - 2:00", "y" => 132),
		array("label" => "2:00 - 3:00", "y" => 148),
		array("label" => "3:00 - 4:00", "y" => 171),
		array("label" => "4:00 - 5:00", "y" => 230),
		array("label" => "5:00 - 6:00", "y" => 296),
		array("label" => "6:00 - 7:00", "y" => 277),
		array("label" => "7:00 - 8:00", "y" => 322),
		array("label" => "8:00 - 9:00", "y" => 528),
		array("label" => "9:00 - 10:00", "y" => 644),
		array("label" => "10:00 - 11:00", "y" => 915),
		array("label" => "11:00 - 12:00", "y" => 355),
		array("label" => "12:00 - 13:00", "y" => 172),
		array("label" => "13:00 - 14:00", "y" => 112),
		array("label" => "14:00 - 15:00", "y" => 108),
		array("label" => "15:00 - 16:00", "y" => 82),
		array("label" => "16:00 - 17:00", "y" => 110),
		array("label" => "17:00 - 18:00", "y" => 42),
		array("label" => "18:00 - 19:00", "y" => 23),
		array("label" => "19:00 - 20:00", "y" => 28),
		array("label" => "20:00 - 21:00", "y" => 68),
		array("label" => "21:00 - 22:00", "y" => 47),
		array("label" => "22:00 - 23:00", "y" => 79),
		array("label" => "23:00 - 00:00", "y" => 97)
	);
?>
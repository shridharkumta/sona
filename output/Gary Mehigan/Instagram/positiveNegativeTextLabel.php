<?php 
	$positive = array(
		array("label" => "Food", "y" => 859),
		array("label" => "Business", "y" => 16),
		array("label" => "Music", "y" => 5),
		array("label" => "Travel", "y" => 64),
		array("label" => "Gaming", "y" => 2),
		array("label" => "Vehicles", "y" => 21),
		array("label" => "Movie", "y" => 22),
		array("label" => "Politics", "y" => 1),
		array("label" => "Space", "y" => 1),
		array("label" => "Books", "y" => 2),
		array("label" => "Sports", "y" => 9),
		array("label" => "Medicine", "y" => 2),
		array("label" => "Fashion", "y" => 3)
	);
	$negative = array(
		array("label" => "Food", "y" => 27),
		array("label" => "Business", "y" => 2),
		array("label" => "Music", "y" => 3),
		array("label" => "Travel", "y" => 15),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Vehicles", "y" => 11),
		array("label" => "Movie", "y" => 3),
		array("label" => "Politics", "y" => 2),
		array("label" => "Space", "y" => 1),
		array("label" => "Books", "y" => 0),
		array("label" => "Sports", "y" => 2),
		array("label" => "Medicine", "y" => 2),
		array("label" => "Fashion", "y" => 2)
	);
	$neutral = array(
		array("label" => "Food", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Fashion", "y" => 0)
	);

?>
<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 2),
		array("label" => "Business", "y" => 18),
		array("label" => "Fashion", "y" => 5),
		array("label" => "Food", "y" => 886),
		array("label" => "Gaming", "y" => 2),
		array("label" => "Medicine", "y" => 4),
		array("label" => "Movie", "y" => 25),
		array("label" => "Music", "y" => 8),
		array("label" => "Politics", "y" => 3),
		array("label" => "Space", "y" => 2),
		array("label" => "Sports", "y" => 11),
		array("label" => "Travel", "y" => 79),
		array("label" => "Vehicles", "y" => 32)
	);
?>
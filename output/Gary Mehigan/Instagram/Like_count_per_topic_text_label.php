<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 15232),
		array("label" => "Business", "y" => 83378),
		array("label" => "Fashion", "y" => 10466),
		array("label" => "Food", "y" => 3790298),
		array("label" => "Gaming", "y" => 25141),
		array("label" => "Medicine", "y" => 6483),
		array("label" => "Movie", "y" => 118604),
		array("label" => "Music", "y" => 20526),
		array("label" => "Politics", "y" => 11116),
		array("label" => "Space", "y" => 15907),
		array("label" => "Sports", "y" => 48732),
		array("label" => "Travel", "y" => 280251),
		array("label" => "Vehicles", "y" => 99425)
	);
?>
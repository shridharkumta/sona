<?php 
	$hashtagCount = array(
		array("label" => "#delicious", "y" => 453),
		array("label" => "#yum", "y" => 374),
		array("label" => "#foodies", "y" => 238),
		array("label" => "#foodiesofinstagram", "y" => 234),
		array("label" => "#chefslife", "y" => 231),
		array("label" => "#lovefood", "y" => 177),
		array("label" => "#deliciousfood", "y" => 173),
		array("label" => "#masterchefau", "y" => 161),
		array("label" => "#yummy", "y" => 151),
		array("label" => "#homecooked", "y" => 116),
	);
?>
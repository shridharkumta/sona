<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 35509),
		array("label" => "Business", "y" => 52612),
		array("label" => "Fashion", "y" => 1244),
		array("label" => "Food", "y" => 138102),
		array("label" => "Gaming", "y" => 1786),
		array("label" => "Medicine", "y" => 962),
		array("label" => "Movie", "y" => 71284),
		array("label" => "Music", "y" => 10524),
		array("label" => "Others", "y" => 71206),
		array("label" => "Politics", "y" => 11685),
		array("label" => "Space", "y" => 1944),
		array("label" => "Sports", "y" => 86505),
		array("label" => "Technology", "y" => 51),
		array("label" => "Travel", "y" => 20064),
		array("label" => "Vehicles", "y" => 7818)
	);
?>
<?php 
	$hashtagCount = array(
		array("label" => "#crispycrackling", "y" => 25),
		array("label" => "#masterchefau", "y" => 24),
		array("label" => "#mastersoftaste", "y" => 21),
		array("label" => "#foodies", "y" => 18),
		array("label" => "#aplatetocallhome", "y" => 18),
		array("label" => "#podcastoneau", "y" => 16),
		array("label" => "#farflung", "y" => 16),
		array("label" => "#india", "y" => 13),
		array("label" => "#garymehigan", "y" => 12),
		array("label" => "#perfectionornothing", "y" => 12),
	);
?>
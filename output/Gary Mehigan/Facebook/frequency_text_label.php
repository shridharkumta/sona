<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 4),
		array("label" => "Business", "y" => 50),
		array("label" => "Fashion", "y" => 7),
		array("label" => "Food", "y" => 341),
		array("label" => "Gaming", "y" => 11),
		array("label" => "Medicine", "y" => 5),
		array("label" => "Movie", "y" => 135),
		array("label" => "Music", "y" => 35),
		array("label" => "Others", "y" => 50),
		array("label" => "Politics", "y" => 9),
		array("label" => "Space", "y" => 4),
		array("label" => "Sports", "y" => 106),
		array("label" => "Technology", "y" => 1),
		array("label" => "Travel", "y" => 34),
		array("label" => "Vehicles", "y" => 5)
	);
?>
<?php 
	$positive = array(
		array("label" => "Food", "y" => 323),
		array("label" => "Business", "y" => 47),
		array("label" => "Music", "y" => 30),
		array("label" => "Movie", "y" => 121),
		array("label" => "Sports", "y" => 87),
		array("label" => "Vehicles", "y" => 2),
		array("label" => "Space", "y" => 3),
		array("label" => "Travel", "y" => 31),
		array("label" => "Fashion", "y" => 6),
		array("label" => "Others", "y" => 0),
		array("label" => "Politics", "y" => 7),
		array("label" => "Books", "y" => 4),
		array("label" => "Medicine", "y" => 2),
		array("label" => "Gaming", "y" => 9),
		array("label" => "Technology", "y" => 1)
	);
	$negative = array(
		array("label" => "Food", "y" => 18),
		array("label" => "Business", "y" => 3),
		array("label" => "Music", "y" => 5),
		array("label" => "Movie", "y" => 14),
		array("label" => "Sports", "y" => 19),
		array("label" => "Vehicles", "y" => 3),
		array("label" => "Space", "y" => 1),
		array("label" => "Travel", "y" => 3),
		array("label" => "Fashion", "y" => 1),
		array("label" => "Others", "y" => 0),
		array("label" => "Politics", "y" => 2),
		array("label" => "Books", "y" => 0),
		array("label" => "Medicine", "y" => 3),
		array("label" => "Gaming", "y" => 2),
		array("label" => "Technology", "y" => 0)
	);
	$neutral = array(
		array("label" => "Food", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Others", "y" => 50),
		array("label" => "Politics", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Technology", "y" => 0)
	);

?>
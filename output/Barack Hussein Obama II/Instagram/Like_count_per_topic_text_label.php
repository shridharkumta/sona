<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 4636005),
		array("label" => "Business", "y" => 39319378),
		array("label" => "Fashion", "y" => 1828741),
		array("label" => "Food", "y" => 18840738),
		array("label" => "Gaming", "y" => 3502649),
		array("label" => "Medicine", "y" => 2176345),
		array("label" => "Movie", "y" => 42872173),
		array("label" => "Music", "y" => 16672067),
		array("label" => "Others", "y" => 324122),
		array("label" => "Politics", "y" => 35968487),
		array("label" => "Space", "y" => 1628980),
		array("label" => "Sports", "y" => 2836656),
		array("label" => "Technology", "y" => 556522),
		array("label" => "Travel", "y" => 3638400),
		array("label" => "Vehicles", "y" => 984922)
	);
?>
<?php 
	$positive = array(
		array("label" => "Politics", "y" => 120),
		array("label" => "Sports", "y" => 10),
		array("label" => "Business", "y" => 59),
		array("label" => "Medicine", "y" => 8),
		array("label" => "Food", "y" => 19),
		array("label" => "Movie", "y" => 37),
		array("label" => "Music", "y" => 9),
		array("label" => "Books", "y" => 6),
		array("label" => "Travel", "y" => 16),
		array("label" => "Space", "y" => 5),
		array("label" => "Gaming", "y" => 3),
		array("label" => "Fashion", "y" => 6),
		array("label" => "Vehicles", "y" => 1),
		array("label" => "Technology", "y" => 1),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Politics", "y" => 26),
		array("label" => "Sports", "y" => 4),
		array("label" => "Business", "y" => 5),
		array("label" => "Medicine", "y" => 3),
		array("label" => "Food", "y" => 4),
		array("label" => "Movie", "y" => 1),
		array("label" => "Music", "y" => 1),
		array("label" => "Books", "y" => 0),
		array("label" => "Travel", "y" => 4),
		array("label" => "Space", "y" => 0),
		array("label" => "Gaming", "y" => 1),
		array("label" => "Fashion", "y" => 4),
		array("label" => "Vehicles", "y" => 3),
		array("label" => "Technology", "y" => 2),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Politics", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Others", "y" => 2)
	);

?>
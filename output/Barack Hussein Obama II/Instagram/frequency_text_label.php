<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 6),
		array("label" => "Business", "y" => 64),
		array("label" => "Fashion", "y" => 10),
		array("label" => "Food", "y" => 23),
		array("label" => "Gaming", "y" => 4),
		array("label" => "Medicine", "y" => 11),
		array("label" => "Movie", "y" => 38),
		array("label" => "Music", "y" => 10),
		array("label" => "Others", "y" => 2),
		array("label" => "Politics", "y" => 146),
		array("label" => "Space", "y" => 5),
		array("label" => "Sports", "y" => 14),
		array("label" => "Technology", "y" => 3),
		array("label" => "Travel", "y" => 20),
		array("label" => "Vehicles", "y" => 4)
	);
?>
<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 4),
		array("label" => "Business", "y" => 512),
		array("label" => "Fashion", "y" => 15),
		array("label" => "Food", "y" => 86),
		array("label" => "Gaming", "y" => 39),
		array("label" => "Medicine", "y" => 143),
		array("label" => "Movie", "y" => 128),
		array("label" => "Music", "y" => 63),
		array("label" => "Politics", "y" => 2030),
		array("label" => "Space", "y" => 72),
		array("label" => "Sports", "y" => 112),
		array("label" => "Technology", "y" => 12),
		array("label" => "Travel", "y" => 38),
		array("label" => "Vehicles", "y" => 11)
	);
?>
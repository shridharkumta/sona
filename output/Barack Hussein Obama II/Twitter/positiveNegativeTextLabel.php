<?php 
	$positive = array(
		array("label" => "Business", "y" => 406),
		array("label" => "Politics", "y" => 1554),
		array("label" => "Medicine", "y" => 68),
		array("label" => "Music", "y" => 53),
		array("label" => "Sports", "y" => 77),
		array("label" => "Travel", "y" => 30),
		array("label" => "Gaming", "y" => 29),
		array("label" => "Space", "y" => 46),
		array("label" => "Movie", "y" => 111),
		array("label" => "Food", "y" => 70),
		array("label" => "Books", "y" => 4),
		array("label" => "Technology", "y" => 5),
		array("label" => "Vehicles", "y" => 6),
		array("label" => "Fashion", "y" => 13)
	);
	$negative = array(
		array("label" => "Business", "y" => 106),
		array("label" => "Politics", "y" => 476),
		array("label" => "Medicine", "y" => 75),
		array("label" => "Music", "y" => 10),
		array("label" => "Sports", "y" => 35),
		array("label" => "Travel", "y" => 8),
		array("label" => "Gaming", "y" => 10),
		array("label" => "Space", "y" => 26),
		array("label" => "Movie", "y" => 17),
		array("label" => "Food", "y" => 16),
		array("label" => "Books", "y" => 0),
		array("label" => "Technology", "y" => 7),
		array("label" => "Vehicles", "y" => 5),
		array("label" => "Fashion", "y" => 2)
	);
	$neutral = array(
		array("label" => "Business", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Fashion", "y" => 0)
	);

?>
<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 92838),
		array("label" => "Business", "y" => 13894431),
		array("label" => "Fashion", "y" => 520515),
		array("label" => "Food", "y" => 2248235),
		array("label" => "Gaming", "y" => 968811),
		array("label" => "Medicine", "y" => 4501602),
		array("label" => "Movie", "y" => 5330034),
		array("label" => "Music", "y" => 2801174),
		array("label" => "Politics", "y" => 22055744),
		array("label" => "Space", "y" => 847499),
		array("label" => "Sports", "y" => 3720686),
		array("label" => "Technology", "y" => 188144),
		array("label" => "Travel", "y" => 1139730),
		array("label" => "Vehicles", "y" => 157111)
	);
?>
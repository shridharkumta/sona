<?php 
	$frequency_label = array(
		array("label" => "Politics", "y" => 3983),
		array("label" => "Business", "y" => 1003),
		array("label" => "Movie", "y" => 377),
		array("label" => "Medicine", "y" => 307),
		array("label" => "Sports", "y" => 203),
		array("label" => "Food", "y" => 182),
		array("label" => "Space", "y" => 128),
		array("label" => "Music", "y" => 109),
		array("label" => "Travel", "y" => 89),
		array("label" => "Gaming", "y" => 69),
		array("label" => "Fashion", "y" => 48),
		array("label" => "Vehicles", "y" => 40),
		array("label" => "Others", "y" => 35),
		array("label" => "Technology", "y" => 24),
		array("label" => "Books", "y" => 17),
	);
?>
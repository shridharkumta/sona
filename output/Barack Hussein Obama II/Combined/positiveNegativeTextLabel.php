<?php 
	$positive = array(
		array("label" => "Movie", "y" => 351),
		array("label" => "Space", "y" => 87),
		array("label" => "Medicine", "y" => 142),
		array("label" => "Travel", "y" => 67),
		array("label" => "Music", "y" => 96),
		array("label" => "Business", "y" => 787),
		array("label" => "Gaming", "y" => 49),
		array("label" => "Sports", "y" => 141),
		array("label" => "Vehicles", "y" => 26),
		array("label" => "Politics", "y" => 3170),
		array("label" => "Food", "y" => 153),
		array("label" => "Fashion", "y" => 37),
		array("label" => "Technology", "y" => 13),
		array("label" => "Books", "y" => 17),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Movie", "y" => 26),
		array("label" => "Space", "y" => 41),
		array("label" => "Medicine", "y" => 165),
		array("label" => "Travel", "y" => 22),
		array("label" => "Music", "y" => 13),
		array("label" => "Business", "y" => 216),
		array("label" => "Gaming", "y" => 20),
		array("label" => "Sports", "y" => 62),
		array("label" => "Vehicles", "y" => 14),
		array("label" => "Politics", "y" => 813),
		array("label" => "Food", "y" => 29),
		array("label" => "Fashion", "y" => 11),
		array("label" => "Technology", "y" => 11),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 35)
	);

?>
<?php 
	$likeCountPerTopic = array(
		array("label" => "Movie", "y" => 72988197),
		array("label" => "Space", "y" => 6841433),
		array("label" => "Medicine", "y" => 15013334),
		array("label" => "Travel", "y" => 9118205),
		array("label" => "Music", "y" => 24904785),
		array("label" => "Business", "y" => 77708818),
		array("label" => "Gaming", "y" => 9373157),
		array("label" => "Sports", "y" => 18676111),
		array("label" => "Vehicles", "y" => 3225545),
		array("label" => "Politics", "y" => 166332752),
		array("label" => "Food", "y" => 43751260),
		array("label" => "Fashion", "y" => 7439543),
		array("label" => "Technology", "y" => 5180638),
		array("label" => "Books", "y" => 5716839),
		array("label" => "Others", "y" => 2618411)
	);
?>
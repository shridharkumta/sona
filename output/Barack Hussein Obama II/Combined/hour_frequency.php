<?php 
	$hour_frequency = array(
		array("label" => "0:00 - 1:00", "y" => 303),
		array("label" => "1:00 - 2:00", "y" => 266),
		array("label" => "2:00 - 3:00", "y" => 404),
		array("label" => "3:00 - 4:00", "y" => 298),
		array("label" => "4:00 - 5:00", "y" => 168),
		array("label" => "5:00 - 6:00", "y" => 123),
		array("label" => "6:00 - 7:00", "y" => 114),
		array("label" => "7:00 - 8:00", "y" => 41),
		array("label" => "8:00 - 9:00", "y" => 33),
		array("label" => "9:00 - 10:00", "y" => 8),
		array("label" => "10:00 - 11:00", "y" => 2),
		array("label" => "11:00 - 12:00", "y" => 4),
		array("label" => "12:00 - 13:00", "y" => 80),
		array("label" => "13:00 - 14:00", "y" => 90),
		array("label" => "14:00 - 15:00", "y" => 131),
		array("label" => "15:00 - 16:00", "y" => 399),
		array("label" => "16:00 - 17:00", "y" => 436),
		array("label" => "17:00 - 18:00", "y" => 370),
		array("label" => "18:00 - 19:00", "y" => 424),
		array("label" => "19:00 - 20:00", "y" => 452),
		array("label" => "20:00 - 21:00", "y" => 605),
		array("label" => "21:00 - 22:00", "y" => 876),
		array("label" => "22:00 - 23:00", "y" => 625),
		array("label" => "23:00 - 00:00", "y" => 359)
	);
?>
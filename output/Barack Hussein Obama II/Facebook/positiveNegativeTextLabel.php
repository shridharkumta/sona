<?php 
	$positive = array(
		array("label" => "Politics", "y" => 1496),
		array("label" => "Business", "y" => 322),
		array("label" => "Sports", "y" => 54),
		array("label" => "Movie", "y" => 203),
		array("label" => "Medicine", "y" => 66),
		array("label" => "Food", "y" => 64),
		array("label" => "Music", "y" => 34),
		array("label" => "Books", "y" => 7),
		array("label" => "Travel", "y" => 21),
		array("label" => "Space", "y" => 36),
		array("label" => "Fashion", "y" => 18),
		array("label" => "Others", "y" => 0),
		array("label" => "Gaming", "y" => 17),
		array("label" => "Vehicles", "y" => 19),
		array("label" => "Technology", "y" => 7)
	);
	$negative = array(
		array("label" => "Politics", "y" => 311),
		array("label" => "Business", "y" => 105),
		array("label" => "Sports", "y" => 23),
		array("label" => "Movie", "y" => 8),
		array("label" => "Medicine", "y" => 87),
		array("label" => "Food", "y" => 9),
		array("label" => "Music", "y" => 2),
		array("label" => "Books", "y" => 0),
		array("label" => "Travel", "y" => 10),
		array("label" => "Space", "y" => 15),
		array("label" => "Fashion", "y" => 5),
		array("label" => "Others", "y" => 0),
		array("label" => "Gaming", "y" => 9),
		array("label" => "Vehicles", "y" => 6),
		array("label" => "Technology", "y" => 2)
	);
	$neutral = array(
		array("label" => "Politics", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Others", "y" => 33),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Technology", "y" => 0)
	);

?>
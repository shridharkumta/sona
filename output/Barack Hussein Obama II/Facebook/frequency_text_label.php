<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 7),
		array("label" => "Business", "y" => 427),
		array("label" => "Fashion", "y" => 23),
		array("label" => "Food", "y" => 73),
		array("label" => "Gaming", "y" => 26),
		array("label" => "Medicine", "y" => 153),
		array("label" => "Movie", "y" => 211),
		array("label" => "Music", "y" => 36),
		array("label" => "Others", "y" => 33),
		array("label" => "Politics", "y" => 1807),
		array("label" => "Space", "y" => 51),
		array("label" => "Sports", "y" => 77),
		array("label" => "Technology", "y" => 9),
		array("label" => "Travel", "y" => 31),
		array("label" => "Vehicles", "y" => 25)
	);
?>
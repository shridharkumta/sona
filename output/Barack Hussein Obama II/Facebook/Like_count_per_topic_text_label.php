<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 987996),
		array("label" => "Business", "y" => 24495009),
		array("label" => "Fashion", "y" => 5090287),
		array("label" => "Food", "y" => 22662287),
		array("label" => "Gaming", "y" => 4901697),
		array("label" => "Medicine", "y" => 8335387),
		array("label" => "Movie", "y" => 24785990),
		array("label" => "Music", "y" => 5431544),
		array("label" => "Others", "y" => 2294289),
		array("label" => "Politics", "y" => 108308521),
		array("label" => "Space", "y" => 4364954),
		array("label" => "Sports", "y" => 12118769),
		array("label" => "Technology", "y" => 4435972),
		array("label" => "Travel", "y" => 4340075),
		array("label" => "Vehicles", "y" => 2083512)
	);
?>
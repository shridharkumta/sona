<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 8907),
		array("label" => "Business", "y" => 147959),
		array("label" => "Fashion", "y" => 179207),
		array("label" => "Food", "y" => 4241902),
		array("label" => "Gaming", "y" => 221815),
		array("label" => "Medicine", "y" => 64360),
		array("label" => "Movie", "y" => 1606268),
		array("label" => "Music", "y" => 289314),
		array("label" => "Others", "y" => 15293),
		array("label" => "Politics", "y" => 35281),
		array("label" => "Space", "y" => 43320),
		array("label" => "Sports", "y" => 327498),
		array("label" => "Technology", "y" => 16837),
		array("label" => "Travel", "y" => 258701),
		array("label" => "Vehicles", "y" => 68360)
	);
?>
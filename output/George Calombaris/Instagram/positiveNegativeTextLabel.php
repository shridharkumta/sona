<?php 
	$positive = array(
		array("label" => "Food", "y" => 1926),
		array("label" => "Sports", "y" => 174),
		array("label" => "Vehicles", "y" => 29),
		array("label" => "Movie", "y" => 532),
		array("label" => "Music", "y" => 143),
		array("label" => "Space", "y" => 20),
		array("label" => "Travel", "y" => 113),
		array("label" => "Gaming", "y" => 101),
		array("label" => "Others", "y" => 0),
		array("label" => "Business", "y" => 88),
		array("label" => "Medicine", "y" => 25),
		array("label" => "Fashion", "y" => 87),
		array("label" => "Books", "y" => 8),
		array("label" => "Politics", "y" => 16),
		array("label" => "Technology", "y" => 7)
	);
	$negative = array(
		array("label" => "Food", "y" => 94),
		array("label" => "Sports", "y" => 24),
		array("label" => "Vehicles", "y" => 7),
		array("label" => "Movie", "y" => 26),
		array("label" => "Music", "y" => 7),
		array("label" => "Space", "y" => 6),
		array("label" => "Travel", "y" => 21),
		array("label" => "Gaming", "y" => 17),
		array("label" => "Others", "y" => 0),
		array("label" => "Business", "y" => 6),
		array("label" => "Medicine", "y" => 7),
		array("label" => "Fashion", "y" => 9),
		array("label" => "Books", "y" => 0),
		array("label" => "Politics", "y" => 6),
		array("label" => "Technology", "y" => 6)
	);
	$neutral = array(
		array("label" => "Food", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Others", "y" => 4),
		array("label" => "Business", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Technology", "y" => 0)
	);

?>
<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 8),
		array("label" => "Business", "y" => 94),
		array("label" => "Fashion", "y" => 96),
		array("label" => "Food", "y" => 2020),
		array("label" => "Gaming", "y" => 118),
		array("label" => "Medicine", "y" => 32),
		array("label" => "Movie", "y" => 558),
		array("label" => "Music", "y" => 150),
		array("label" => "Others", "y" => 4),
		array("label" => "Politics", "y" => 22),
		array("label" => "Space", "y" => 26),
		array("label" => "Sports", "y" => 198),
		array("label" => "Technology", "y" => 13),
		array("label" => "Travel", "y" => 134),
		array("label" => "Vehicles", "y" => 36)
	);
?>
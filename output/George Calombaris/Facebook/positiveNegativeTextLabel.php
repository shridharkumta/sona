<?php 
	$positive = array(
		array("label" => "Medicine", "y" => 10),
		array("label" => "Movie", "y" => 117),
		array("label" => "Food", "y" => 296),
		array("label" => "Sports", "y" => 56),
		array("label" => "Others", "y" => 0),
		array("label" => "Business", "y" => 29),
		array("label" => "Travel", "y" => 38),
		array("label" => "Fashion", "y" => 3),
		array("label" => "Music", "y" => 30),
		array("label" => "Vehicles", "y" => 4),
		array("label" => "Gaming", "y" => 8),
		array("label" => "Space", "y" => 6),
		array("label" => "Politics", "y" => 2)
	);
	$negative = array(
		array("label" => "Medicine", "y" => 4),
		array("label" => "Movie", "y" => 7),
		array("label" => "Food", "y" => 11),
		array("label" => "Sports", "y" => 7),
		array("label" => "Others", "y" => 0),
		array("label" => "Business", "y" => 2),
		array("label" => "Travel", "y" => 5),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Music", "y" => 1),
		array("label" => "Vehicles", "y" => 1),
		array("label" => "Gaming", "y" => 1),
		array("label" => "Space", "y" => 3),
		array("label" => "Politics", "y" => 1)
	);
	$neutral = array(
		array("label" => "Medicine", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Others", "y" => 21),
		array("label" => "Business", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Politics", "y" => 0)
	);

?>
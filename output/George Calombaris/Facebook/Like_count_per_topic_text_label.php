<?php 
	$likeCountPerTopic = array(
		array("label" => "Business", "y" => 19375),
		array("label" => "Fashion", "y" => 2254),
		array("label" => "Food", "y" => 277205),
		array("label" => "Gaming", "y" => 4722),
		array("label" => "Medicine", "y" => 5946),
		array("label" => "Movie", "y" => 129337),
		array("label" => "Music", "y" => 28804),
		array("label" => "Others", "y" => 20512),
		array("label" => "Politics", "y" => 6665),
		array("label" => "Space", "y" => 1214),
		array("label" => "Sports", "y" => 76104),
		array("label" => "Travel", "y" => 75761),
		array("label" => "Vehicles", "y" => 3682)
	);
?>
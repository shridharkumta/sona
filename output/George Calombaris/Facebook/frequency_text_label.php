<?php 
	$frequency_label = array(
		array("label" => "Business", "y" => 31),
		array("label" => "Fashion", "y" => 3),
		array("label" => "Food", "y" => 307),
		array("label" => "Gaming", "y" => 9),
		array("label" => "Medicine", "y" => 14),
		array("label" => "Movie", "y" => 124),
		array("label" => "Music", "y" => 31),
		array("label" => "Others", "y" => 21),
		array("label" => "Politics", "y" => 3),
		array("label" => "Space", "y" => 9),
		array("label" => "Sports", "y" => 63),
		array("label" => "Travel", "y" => 43),
		array("label" => "Vehicles", "y" => 5)
	);
?>
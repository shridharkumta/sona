<?php 
	$frequency_label = array(
		array("label" => "Food", "y" => 2327),
		array("label" => "Movie", "y" => 682),
		array("label" => "Sports", "y" => 261),
		array("label" => "Music", "y" => 181),
		array("label" => "Travel", "y" => 177),
		array("label" => "Gaming", "y" => 127),
		array("label" => "Business", "y" => 125),
		array("label" => "Fashion", "y" => 99),
		array("label" => "Medicine", "y" => 46),
		array("label" => "Vehicles", "y" => 41),
		array("label" => "Space", "y" => 35),
		array("label" => "Politics", "y" => 25),
		array("label" => "Others", "y" => 25),
		array("label" => "Technology", "y" => 13),
		array("label" => "Books", "y" => 8),
	);
?>
<?php 
	$likeCountPerTopic = array(
		array("label" => "Movie", "y" => 1735605),
		array("label" => "Space", "y" => 44534),
		array("label" => "Medicine", "y" => 70306),
		array("label" => "Travel", "y" => 334462),
		array("label" => "Music", "y" => 318118),
		array("label" => "Business", "y" => 167334),
		array("label" => "Gaming", "y" => 226537),
		array("label" => "Sports", "y" => 403602),
		array("label" => "Vehicles", "y" => 72042),
		array("label" => "Politics", "y" => 41946),
		array("label" => "Food", "y" => 4519107),
		array("label" => "Fashion", "y" => 181461),
		array("label" => "Technology", "y" => 16837),
		array("label" => "Books", "y" => 8907),
		array("label" => "Others", "y" => 35805)
	);
?>
<?php 
	$positive = array(
		array("label" => "Movie", "y" => 649),
		array("label" => "Space", "y" => 26),
		array("label" => "Medicine", "y" => 35),
		array("label" => "Travel", "y" => 151),
		array("label" => "Music", "y" => 173),
		array("label" => "Business", "y" => 117),
		array("label" => "Gaming", "y" => 109),
		array("label" => "Sports", "y" => 230),
		array("label" => "Vehicles", "y" => 33),
		array("label" => "Politics", "y" => 18),
		array("label" => "Food", "y" => 2222),
		array("label" => "Fashion", "y" => 90),
		array("label" => "Technology", "y" => 7),
		array("label" => "Books", "y" => 8),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Movie", "y" => 33),
		array("label" => "Space", "y" => 9),
		array("label" => "Medicine", "y" => 11),
		array("label" => "Travel", "y" => 26),
		array("label" => "Music", "y" => 8),
		array("label" => "Business", "y" => 8),
		array("label" => "Gaming", "y" => 18),
		array("label" => "Sports", "y" => 31),
		array("label" => "Vehicles", "y" => 8),
		array("label" => "Politics", "y" => 7),
		array("label" => "Food", "y" => 105),
		array("label" => "Fashion", "y" => 9),
		array("label" => "Technology", "y" => 6),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 25)
	);

?>
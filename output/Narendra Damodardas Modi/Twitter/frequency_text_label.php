<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 6),
		array("label" => "Business", "y" => 379),
		array("label" => "Fashion", "y" => 4),
		array("label" => "Food", "y" => 46),
		array("label" => "Gaming", "y" => 24),
		array("label" => "Medicine", "y" => 207),
		array("label" => "Movie", "y" => 111),
		array("label" => "Music", "y" => 36),
		array("label" => "Others", "y" => 196),
		array("label" => "Politics", "y" => 1509),
		array("label" => "Space", "y" => 64),
		array("label" => "Sports", "y" => 111),
		array("label" => "Technology", "y" => 67),
		array("label" => "Travel", "y" => 482),
		array("label" => "Vehicles", "y" => 25)
	);
?>
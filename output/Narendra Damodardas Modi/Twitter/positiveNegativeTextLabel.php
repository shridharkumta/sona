<?php 
	$positive = array(
		array("label" => "Politics", "y" => 1145),
		array("label" => "Business", "y" => 333),
		array("label" => "Medicine", "y" => 156),
		array("label" => "Technology", "y" => 50),
		array("label" => "Others", "y" => 0),
		array("label" => "Travel", "y" => 62),
		array("label" => "Space", "y" => 49),
		array("label" => "Vehicles", "y" => 9),
		array("label" => "Music", "y" => 32),
		array("label" => "Movie", "y" => 99),
		array("label" => "Food", "y" => 43),
		array("label" => "Sports", "y" => 90),
		array("label" => "Books", "y" => 5),
		array("label" => "Gaming", "y" => 20),
		array("label" => "Fashion", "y" => 3)
	);
	$negative = array(
		array("label" => "Politics", "y" => 364),
		array("label" => "Business", "y" => 46),
		array("label" => "Medicine", "y" => 51),
		array("label" => "Technology", "y" => 17),
		array("label" => "Others", "y" => 0),
		array("label" => "Travel", "y" => 420),
		array("label" => "Space", "y" => 15),
		array("label" => "Vehicles", "y" => 16),
		array("label" => "Music", "y" => 4),
		array("label" => "Movie", "y" => 12),
		array("label" => "Food", "y" => 3),
		array("label" => "Sports", "y" => 21),
		array("label" => "Books", "y" => 1),
		array("label" => "Gaming", "y" => 4),
		array("label" => "Fashion", "y" => 1)
	);
	$neutral = array(
		array("label" => "Politics", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Others", "y" => 196),
		array("label" => "Travel", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Fashion", "y" => 0)
	);

?>
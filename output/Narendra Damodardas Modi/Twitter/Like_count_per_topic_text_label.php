<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 143706),
		array("label" => "Business", "y" => 12026730),
		array("label" => "Fashion", "y" => 110526),
		array("label" => "Food", "y" => 1570815),
		array("label" => "Gaming", "y" => 988586),
		array("label" => "Medicine", "y" => 6952515),
		array("label" => "Movie", "y" => 2995965),
		array("label" => "Music", "y" => 1178965),
		array("label" => "Others", "y" => 4671027),
		array("label" => "Politics", "y" => 47185622),
		array("label" => "Space", "y" => 1508378),
		array("label" => "Sports", "y" => 3069310),
		array("label" => "Technology", "y" => 2115367),
		array("label" => "Travel", "y" => 11930477),
		array("label" => "Vehicles", "y" => 486478)
	);
?>
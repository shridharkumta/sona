<?php 
	$hour_frequency = array(
		array("label" => "0:00 - 1:00", "y" => 11),
		array("label" => "1:00 - 2:00", "y" => 69),
		array("label" => "2:00 - 3:00", "y" => 147),
		array("label" => "3:00 - 4:00", "y" => 168),
		array("label" => "4:00 - 5:00", "y" => 140),
		array("label" => "5:00 - 6:00", "y" => 199),
		array("label" => "6:00 - 7:00", "y" => 158),
		array("label" => "7:00 - 8:00", "y" => 172),
		array("label" => "8:00 - 9:00", "y" => 236),
		array("label" => "9:00 - 10:00", "y" => 120),
		array("label" => "10:00 - 11:00", "y" => 207),
		array("label" => "11:00 - 12:00", "y" => 243),
		array("label" => "12:00 - 13:00", "y" => 241),
		array("label" => "13:00 - 14:00", "y" => 316),
		array("label" => "14:00 - 15:00", "y" => 420),
		array("label" => "15:00 - 16:00", "y" => 282),
		array("label" => "16:00 - 17:00", "y" => 253),
		array("label" => "17:00 - 18:00", "y" => 179),
		array("label" => "18:00 - 19:00", "y" => 44),
		array("label" => "19:00 - 20:00", "y" => 27),
		array("label" => "20:00 - 21:00", "y" => 22),
		array("label" => "21:00 - 22:00", "y" => 3),
		array("label" => "22:00 - 23:00", "y" => 3),
		array("label" => "23:00 - 00:00", "y" => 8)
	);
?>
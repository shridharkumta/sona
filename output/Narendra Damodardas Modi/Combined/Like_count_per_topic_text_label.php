<?php 
	$likeCountPerTopic = array(
		array("label" => "Movie", "y" => 34474037),
		array("label" => "Space", "y" => 6416897),
		array("label" => "Medicine", "y" => 18594147),
		array("label" => "Travel", "y" => 79930776),
		array("label" => "Music", "y" => 4242060),
		array("label" => "Business", "y" => 29954541),
		array("label" => "Gaming", "y" => 2173138),
		array("label" => "Sports", "y" => 37250146),
		array("label" => "Vehicles", "y" => 2841894),
		array("label" => "Politics", "y" => 258707982),
		array("label" => "Food", "y" => 19770817),
		array("label" => "Fashion", "y" => 2806171),
		array("label" => "Technology", "y" => 3062030),
		array("label" => "Books", "y" => 143706),
		array("label" => "Others", "y" => 22770631)
	);
?>
<?php 
	$positive = array(
		array("label" => "Movie", "y" => 124),
		array("label" => "Space", "y" => 53),
		array("label" => "Medicine", "y" => 167),
		array("label" => "Travel", "y" => 122),
		array("label" => "Music", "y" => 36),
		array("label" => "Business", "y" => 350),
		array("label" => "Gaming", "y" => 22),
		array("label" => "Sports", "y" => 109),
		array("label" => "Vehicles", "y" => 11),
		array("label" => "Politics", "y" => 1306),
		array("label" => "Food", "y" => 52),
		array("label" => "Fashion", "y" => 4),
		array("label" => "Technology", "y" => 51),
		array("label" => "Books", "y" => 5),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Movie", "y" => 13),
		array("label" => "Space", "y" => 16),
		array("label" => "Medicine", "y" => 54),
		array("label" => "Travel", "y" => 441),
		array("label" => "Music", "y" => 4),
		array("label" => "Business", "y" => 48),
		array("label" => "Gaming", "y" => 4),
		array("label" => "Sports", "y" => 25),
		array("label" => "Vehicles", "y" => 17),
		array("label" => "Politics", "y" => 398),
		array("label" => "Food", "y" => 4),
		array("label" => "Fashion", "y" => 2),
		array("label" => "Technology", "y" => 17),
		array("label" => "Books", "y" => 1),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 212)
	);

?>
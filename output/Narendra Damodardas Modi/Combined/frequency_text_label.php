<?php 
	$frequency_label = array(
		array("label" => "Politics", "y" => 1704),
		array("label" => "Travel", "y" => 563),
		array("label" => "Business", "y" => 398),
		array("label" => "Medicine", "y" => 221),
		array("label" => "Others", "y" => 212),
		array("label" => "Movie", "y" => 137),
		array("label" => "Sports", "y" => 134),
		array("label" => "Space", "y" => 69),
		array("label" => "Technology", "y" => 68),
		array("label" => "Food", "y" => 56),
		array("label" => "Music", "y" => 40),
		array("label" => "Vehicles", "y" => 28),
		array("label" => "Gaming", "y" => 26),
		array("label" => "Fashion", "y" => 6),
		array("label" => "Books", "y" => 6),
	);
?>
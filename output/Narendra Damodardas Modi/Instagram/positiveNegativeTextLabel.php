<?php 
	$positive = array(
		array("label" => "Politics", "y" => 161),
		array("label" => "Sports", "y" => 19),
		array("label" => "Others", "y" => 0),
		array("label" => "Medicine", "y" => 11),
		array("label" => "Business", "y" => 17),
		array("label" => "Gaming", "y" => 2),
		array("label" => "Music", "y" => 4),
		array("label" => "Food", "y" => 9),
		array("label" => "Travel", "y" => 60),
		array("label" => "Space", "y" => 4),
		array("label" => "Movie", "y" => 25),
		array("label" => "Vehicles", "y" => 2),
		array("label" => "Fashion", "y" => 1),
		array("label" => "Technology", "y" => 1)
	);
	$negative = array(
		array("label" => "Politics", "y" => 34),
		array("label" => "Sports", "y" => 4),
		array("label" => "Others", "y" => 0),
		array("label" => "Medicine", "y" => 3),
		array("label" => "Business", "y" => 2),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Food", "y" => 1),
		array("label" => "Travel", "y" => 21),
		array("label" => "Space", "y" => 1),
		array("label" => "Movie", "y" => 1),
		array("label" => "Vehicles", "y" => 1),
		array("label" => "Fashion", "y" => 1),
		array("label" => "Technology", "y" => 0)
	);
	$neutral = array(
		array("label" => "Politics", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Others", "y" => 16),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Technology", "y" => 0)
	);

?>
<?php 
	$frequency_label = array(
		array("label" => "Business", "y" => 19),
		array("label" => "Fashion", "y" => 2),
		array("label" => "Food", "y" => 10),
		array("label" => "Gaming", "y" => 2),
		array("label" => "Medicine", "y" => 14),
		array("label" => "Movie", "y" => 26),
		array("label" => "Music", "y" => 4),
		array("label" => "Others", "y" => 16),
		array("label" => "Politics", "y" => 195),
		array("label" => "Space", "y" => 5),
		array("label" => "Sports", "y" => 23),
		array("label" => "Technology", "y" => 1),
		array("label" => "Travel", "y" => 81),
		array("label" => "Vehicles", "y" => 3)
	);
?>
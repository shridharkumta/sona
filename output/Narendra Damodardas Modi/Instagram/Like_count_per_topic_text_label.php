<?php 
	$likeCountPerTopic = array(
		array("label" => "Business", "y" => 17927811),
		array("label" => "Fashion", "y" => 2695645),
		array("label" => "Food", "y" => 18200002),
		array("label" => "Gaming", "y" => 1184552),
		array("label" => "Medicine", "y" => 11641632),
		array("label" => "Movie", "y" => 31478072),
		array("label" => "Music", "y" => 3063095),
		array("label" => "Others", "y" => 18099604),
		array("label" => "Politics", "y" => 211522360),
		array("label" => "Space", "y" => 4908519),
		array("label" => "Sports", "y" => 34180836),
		array("label" => "Technology", "y" => 946663),
		array("label" => "Travel", "y" => 68000299),
		array("label" => "Vehicles", "y" => 2355416)
	);
?>
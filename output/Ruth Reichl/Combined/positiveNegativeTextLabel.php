<?php 
	$positive = array(
		array("label" => "Movie", "y" => 195),
		array("label" => "Space", "y" => 61),
		array("label" => "Medicine", "y" => 41),
		array("label" => "Travel", "y" => 259),
		array("label" => "Music", "y" => 72),
		array("label" => "Business", "y" => 163),
		array("label" => "Gaming", "y" => 89),
		array("label" => "Sports", "y" => 55),
		array("label" => "Vehicles", "y" => 9),
		array("label" => "Politics", "y" => 101),
		array("label" => "Food", "y" => 2192),
		array("label" => "Technology", "y" => 16),
		array("label" => "Fashion", "y" => 61),
		array("label" => "Books", "y" => 91),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Movie", "y" => 38),
		array("label" => "Space", "y" => 33),
		array("label" => "Medicine", "y" => 15),
		array("label" => "Travel", "y" => 124),
		array("label" => "Music", "y" => 28),
		array("label" => "Business", "y" => 44),
		array("label" => "Gaming", "y" => 34),
		array("label" => "Sports", "y" => 45),
		array("label" => "Vehicles", "y" => 13),
		array("label" => "Politics", "y" => 63),
		array("label" => "Food", "y" => 345),
		array("label" => "Technology", "y" => 7),
		array("label" => "Fashion", "y" => 16),
		array("label" => "Books", "y" => 12),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 31)
	);

?>
<?php 
	$likeCountPerTopic = array(
		array("label" => "Movie", "y" => 28457),
		array("label" => "Space", "y" => 17034),
		array("label" => "Medicine", "y" => 4452),
		array("label" => "Travel", "y" => 44337),
		array("label" => "Music", "y" => 12209),
		array("label" => "Business", "y" => 25770),
		array("label" => "Gaming", "y" => 13078),
		array("label" => "Sports", "y" => 11840),
		array("label" => "Vehicles", "y" => 3214),
		array("label" => "Politics", "y" => 17223),
		array("label" => "Food", "y" => 317560),
		array("label" => "Technology", "y" => 3149),
		array("label" => "Fashion", "y" => 13731),
		array("label" => "Books", "y" => 12000),
		array("label" => "Others", "y" => 4701)
	);
?>
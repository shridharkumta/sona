<?php 
	$frequency_label = array(
		array("label" => "Food", "y" => 2537),
		array("label" => "Travel", "y" => 383),
		array("label" => "Movie", "y" => 233),
		array("label" => "Business", "y" => 207),
		array("label" => "Politics", "y" => 164),
		array("label" => "Gaming", "y" => 123),
		array("label" => "Books", "y" => 103),
		array("label" => "Music", "y" => 100),
		array("label" => "Sports", "y" => 100),
		array("label" => "Space", "y" => 94),
		array("label" => "Fashion", "y" => 77),
		array("label" => "Medicine", "y" => 56),
		array("label" => "Others", "y" => 31),
		array("label" => "Technology", "y" => 23),
		array("label" => "Vehicles", "y" => 22),
	);
?>
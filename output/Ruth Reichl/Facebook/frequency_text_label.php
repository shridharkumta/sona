<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 1),
		array("label" => "Business", "y" => 6),
		array("label" => "Food", "y" => 62),
		array("label" => "Gaming", "y" => 8),
		array("label" => "Medicine", "y" => 1),
		array("label" => "Movie", "y" => 16),
		array("label" => "Others", "y" => 8),
		array("label" => "Politics", "y" => 3),
		array("label" => "Sports", "y" => 3),
		array("label" => "Technology", "y" => 1),
		array("label" => "Travel", "y" => 10)
	);
?>
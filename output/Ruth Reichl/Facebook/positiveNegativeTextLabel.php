<?php 
	$positive = array(
		array("label" => "Travel", "y" => 9),
		array("label" => "Movie", "y" => 15),
		array("label" => "Politics", "y" => 2),
		array("label" => "Food", "y" => 54),
		array("label" => "Others", "y" => 0),
		array("label" => "Business", "y" => 5),
		array("label" => "Gaming", "y" => 8),
		array("label" => "Sports", "y" => 3),
		array("label" => "Books", "y" => 1),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Technology", "y" => 1)
	);
	$negative = array(
		array("label" => "Travel", "y" => 1),
		array("label" => "Movie", "y" => 1),
		array("label" => "Politics", "y" => 1),
		array("label" => "Food", "y" => 8),
		array("label" => "Others", "y" => 0),
		array("label" => "Business", "y" => 1),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Medicine", "y" => 1),
		array("label" => "Technology", "y" => 0)
	);
	$neutral = array(
		array("label" => "Travel", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Others", "y" => 8),
		array("label" => "Business", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Technology", "y" => 0)
	);

?>
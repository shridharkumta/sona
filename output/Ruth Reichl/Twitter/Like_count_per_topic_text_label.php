<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 7253),
		array("label" => "Business", "y" => 17405),
		array("label" => "Fashion", "y" => 3140),
		array("label" => "Food", "y" => 107789),
		array("label" => "Gaming", "y" => 5584),
		array("label" => "Medicine", "y" => 4013),
		array("label" => "Movie", "y" => 16210),
		array("label" => "Music", "y" => 7478),
		array("label" => "Politics", "y" => 12650),
		array("label" => "Space", "y" => 4940),
		array("label" => "Sports", "y" => 4867),
		array("label" => "Technology", "y" => 1722),
		array("label" => "Travel", "y" => 17904),
		array("label" => "Vehicles", "y" => 729)
	);
?>
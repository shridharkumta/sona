<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 90),
		array("label" => "Business", "y" => 181),
		array("label" => "Fashion", "y" => 49),
		array("label" => "Food", "y" => 1882),
		array("label" => "Gaming", "y" => 90),
		array("label" => "Medicine", "y" => 53),
		array("label" => "Movie", "y" => 193),
		array("label" => "Music", "y" => 83),
		array("label" => "Politics", "y" => 149),
		array("label" => "Space", "y" => 62),
		array("label" => "Sports", "y" => 80),
		array("label" => "Technology", "y" => 17),
		array("label" => "Travel", "y" => 291),
		array("label" => "Vehicles", "y" => 14)
	);
?>
<?php 
	$positive = array(
		array("label" => "Food", "y" => 1621),
		array("label" => "Sports", "y" => 45),
		array("label" => "Movie", "y" => 161),
		array("label" => "Business", "y" => 147),
		array("label" => "Politics", "y" => 91),
		array("label" => "Music", "y" => 61),
		array("label" => "Fashion", "y" => 44),
		array("label" => "Medicine", "y" => 40),
		array("label" => "Books", "y" => 80),
		array("label" => "Travel", "y" => 204),
		array("label" => "Technology", "y" => 10),
		array("label" => "Gaming", "y" => 67),
		array("label" => "Space", "y" => 42),
		array("label" => "Vehicles", "y" => 6)
	);
	$negative = array(
		array("label" => "Food", "y" => 261),
		array("label" => "Sports", "y" => 35),
		array("label" => "Movie", "y" => 32),
		array("label" => "Business", "y" => 34),
		array("label" => "Politics", "y" => 58),
		array("label" => "Music", "y" => 22),
		array("label" => "Fashion", "y" => 5),
		array("label" => "Medicine", "y" => 13),
		array("label" => "Books", "y" => 10),
		array("label" => "Travel", "y" => 87),
		array("label" => "Technology", "y" => 7),
		array("label" => "Gaming", "y" => 23),
		array("label" => "Space", "y" => 20),
		array("label" => "Vehicles", "y" => 8)
	);
	$neutral = array(
		array("label" => "Food", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Vehicles", "y" => 0)
	);

?>
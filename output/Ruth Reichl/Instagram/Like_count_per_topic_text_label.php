<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 4726),
		array("label" => "Business", "y" => 7922),
		array("label" => "Fashion", "y" => 10591),
		array("label" => "Food", "y" => 203441),
		array("label" => "Gaming", "y" => 6968),
		array("label" => "Medicine", "y" => 418),
		array("label" => "Movie", "y" => 11583),
		array("label" => "Music", "y" => 4731),
		array("label" => "Others", "y" => 2697),
		array("label" => "Politics", "y" => 4508),
		array("label" => "Space", "y" => 12094),
		array("label" => "Sports", "y" => 6604),
		array("label" => "Technology", "y" => 1407),
		array("label" => "Travel", "y" => 25822),
		array("label" => "Vehicles", "y" => 2485)
	);
?>
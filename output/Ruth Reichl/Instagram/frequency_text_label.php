<?php 
	$frequency_label = array(
		array("label" => "Books", "y" => 12),
		array("label" => "Business", "y" => 20),
		array("label" => "Fashion", "y" => 28),
		array("label" => "Food", "y" => 593),
		array("label" => "Gaming", "y" => 25),
		array("label" => "Medicine", "y" => 2),
		array("label" => "Movie", "y" => 24),
		array("label" => "Music", "y" => 17),
		array("label" => "Others", "y" => 23),
		array("label" => "Politics", "y" => 12),
		array("label" => "Space", "y" => 32),
		array("label" => "Sports", "y" => 17),
		array("label" => "Technology", "y" => 5),
		array("label" => "Travel", "y" => 82),
		array("label" => "Vehicles", "y" => 8)
	);
?>
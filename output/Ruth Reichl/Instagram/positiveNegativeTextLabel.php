<?php 
	$positive = array(
		array("label" => "Music", "y" => 11),
		array("label" => "Fashion", "y" => 17),
		array("label" => "Space", "y" => 19),
		array("label" => "Food", "y" => 517),
		array("label" => "Business", "y" => 11),
		array("label" => "Movie", "y" => 19),
		array("label" => "Sports", "y" => 7),
		array("label" => "Politics", "y" => 8),
		array("label" => "Travel", "y" => 46),
		array("label" => "Gaming", "y" => 14),
		array("label" => "Vehicles", "y" => 3),
		array("label" => "Others", "y" => 0),
		array("label" => "Books", "y" => 10),
		array("label" => "Technology", "y" => 5),
		array("label" => "Medicine", "y" => 1)
	);
	$negative = array(
		array("label" => "Music", "y" => 6),
		array("label" => "Fashion", "y" => 11),
		array("label" => "Space", "y" => 13),
		array("label" => "Food", "y" => 76),
		array("label" => "Business", "y" => 9),
		array("label" => "Movie", "y" => 5),
		array("label" => "Sports", "y" => 10),
		array("label" => "Politics", "y" => 4),
		array("label" => "Travel", "y" => 36),
		array("label" => "Gaming", "y" => 11),
		array("label" => "Vehicles", "y" => 5),
		array("label" => "Others", "y" => 0),
		array("label" => "Books", "y" => 2),
		array("label" => "Technology", "y" => 0),
		array("label" => "Medicine", "y" => 1)
	);
	$neutral = array(
		array("label" => "Music", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Others", "y" => 23),
		array("label" => "Books", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Medicine", "y" => 0)
	);

?>
"""
    Module to accept a twitter user-id and scrape their posts
"""

#Importing requests to request for twitter user data
import requests

#Importing pandas to perform dataframe operations
import pandas as pd

#Library to accept command-line arguments
import argparse

#Importing library to convert datetime from string to datetime object
from datetime import datetime

#Initializing request parameters
BEARER_TOKEN = "AAAAAAAAAAAAAAAAAAAAANmaCgEAAAAAQ9y1vClEDoPdvtb9DeXYkrElJNo%3Dv9PSypqIsrfnH44J1YONZqPb3azjDY8rRj5m809WWIZhM8PGU8"
Header = dict(Authorization = "Bearer " + BEARER_TOKEN)
counter = 0

def valid_xml_char_ordinal(c):
    """
    Function to validate characters in an xml response
     ----------
     c: Character
         Character from an XML response received
     """
     
    #Get the unicode for the character in an XML response
    codepoint = ord(c)
    
    #Conditions ordered by presumed frequency
    return (
        0x20 <= codepoint <= 0xD7FF or
        codepoint in (0x9, 0xA, 0xD) or
        0xE000 <= codepoint <= 0xFFFD or
        0x10000 <= codepoint <= 0x10FFFF
        )


def serve_requests(url, data):
    """
    Function to request data from Twitter for the user-id
     ----------
     url: str
         XML response received
     data: object
         Data that contains related informatoin for a request, includes - 
         user id, access token, et cetera
     """
     
    #Request to collect user data from twitter and return as a response
    try:
        Response = requests.get(url = url, params = data, headers = Header)
        return Response.json()
    except:
        print('Check internet conenction')
        return None


def Download_data(Response):
    """
    Function to download data for the given user-id
     ----------
     Response: Object 
         Response received from serve_requests function 
     """
     
    #Creating a dataframe to store the user's posts
    Twitter = pd.DataFrame()
    id = ''
    for idx in range(len(Response)):
      id = id + ',' + Response[idx]['id_str']

    id = id[1:]
    
    #Twitter URL to get the posts from
    tweet_url = "https://api.twitter.com/labs/2/tweets?tweet.fields=created_at,author_id,lang,source,public_metrics&expansions=attachments.media_keys"
    tweet_data = dict(ids= id)
    
    #Get JSON response from serve requests function
    tweet = serve_requests(tweet_url,tweet_data)
    
    
    global ERROR
    ERROR = tweet
    
    #If there is no posts, return the empty dataframe
    if not 'data' in ERROR.keys():
      return Twitter
  
    #loop through the posts and store the details
    for idx in range(len(tweet['data'])):
        
      #Check if media is present
      if 'media' in Response[idx]['entities']:
        media_url = ''.join(c for c in Response[idx]['entities']['media'][0]['media_url'] if valid_xml_char_ordinal(c))
      
      #There are no medias
      else:
        media_url = None
        
      #Adding the obtained detials to a dataFrame
      df = pd.DataFrame([pd.Series({
          'created' : ''.join(c for c in Response[idx]['created_at'] if valid_xml_char_ordinal(c)),
          'image_url' : media_url,
          'text' : ''.join(c for c in tweet['data'][idx]['text'] if valid_xml_char_ordinal(c)),
          'likeCount': tweet['data'][0]['public_metrics']['like_count'],
          'replyCount': tweet['data'][0]['public_metrics']['reply_count']
      })])
    
      #Adding to final Dataframe
      Twitter = Twitter.append(df, ignore_index=True, sort = True)
      
      #If no media url, replace the null values with an empty string
      Twitter["image_url"].fillna(" ", inplace = True)
      
    return Twitter

def Get_Twitter_Data(screen_name):
    """
    Function(High-level) to scrape data for the given twitter-id
     ----------
     Screen_name: str
         Twitter id of the user to be scraped
     """
    
    #Check if user with the screen_name given exists
    url = "https://api.twitter.com/1.1/statuses/user_timeline.json"
    data =  dict(screen_name = screen_name, count = 100)
    Response = serve_requests(url, data)
    length = len(Response)
    counter = length
    Twitter = pd.DataFrame()
    
    #If such user id does exist, return a dataframe containing information 
    #about the posts and return true
    try:
        flag = len(Response)
        print("Twitter scraping started")
        print("In progress...")
        
        #Loop through the pages and extract the posts calling the download_data
        #method
        while (length > 1):
          Twitter = Twitter.append(Download_data(Response), ignore_index = True, sort = True)
          max_id = Response[length - 1]['id']
          data =  dict(screen_name = screen_name, count = 100, max_id = max_id)
          Response = serve_requests(url, data)
          length = len(Response)
          counter += len(Response)
          flag += len(Response)
        print("Twitter scraping done")
        Twitter['created'] = Twitter['created'].apply(lambda elem: elem.replace('+', '00'))
        Twitter['created'] = Twitter['created'].apply(lambda elem: datetime.strptime(elem, "%a %b %d %H:%M:%S %f %Y"))
        
        return Twitter, True
    
    #If such user id does not exist, return an empty dataframe and return false
    except: 
        print('No such user exists')
        return pd.DataFrame(columns = ['created', 'image_url', 'text', 'likeCount', 'replyCount']), False
        


if __name__ == "__main__":
    
    #Construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-n", "--name", required=True,
    	help="user id of the twitter user")
    args = vars(ap.parse_args())
    
    #Get the user id from the terminal
    username = args["name"]
    
    #Create a dataframe to store the user's posts
    Data = pd.DataFrame()
    
    #Download and write the content to csv file
    Data = Data.append(Get_Twitter_Data(username))
    Data.to_csv(username + '_twitter.csv', sep=',')

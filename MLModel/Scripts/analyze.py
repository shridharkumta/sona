"""
    Module to analyze the processed-labelled data
"""

#Importing library to create structured folders suitable for visualization
import os

#Importing pandas to perform dataframe operations
import pandas as pd

#Importing library to perform numerical computations
import numpy as np


def Analyse(path, dataframes, args):
    #Arrays to hold data for combined analysis
    timeAnalysis = []
    likesAnalysis = []
    tagAnalysis = []
    hourAnalysis = []
    imageAnalysis = []
    sentimentAnalysis = []
    textAnalysis = []

    """
    Function to analyze the data given
     ----------
     path: str
         Contains path of the parent file
     dataframes: array
         Contains 4 dataframes, one each for a platform containing user data
     args: dictionary
         Contains the user id's of a single across different platforms
    """

    #Arrays to hold platform id's and platform names
    platformNames = ['Facebook', 'Instagram', 'Reddit', 'Twitter']
    platformIds = ['facebookid', 'instagramid', 'redditid', 'twitterid']


    #Extract the user name from the argument parser
    name = args['name']

    #loop through the dataframes for each platform
    i = -1
    for dataframe in dataframes:
        i = i + 1

        #If the patform id is not set, move to next platform
        if args[platformIds[i]] == '':
            continue

        #Extract the platform name and create a directory for the same if it
        #does not exist already
        platform = platformNames[i]
        localpath = path + '\\Output\\' + name + '\\' + platform
        if not os.path.exists(localpath):
            os.makedirs(localpath)

        #Perform different analysis on the data
        timeAnalysis.append(analyzeTime(dataframe, localpath))
        textAnalysis.append(textPerTopic(dataframe, localpath))
        likesAnalysis.append(likeCountPerTopic(dataframe, localpath))
        tagAnalysis.append(hashtagCount(dataframe, localpath))
        hourAnalysis.append(topicPostHour(dataframe, localpath, 3))
        sentimentAnalysis.append(positiveNegative(dataframe, localpath))

        #If opted for image analysis, perform it
        if(args['imageAnalysis'] == 'True'):
            imageAnalysis.append(imagesPerTopic(dataframe, localpath))

    #Performed combined analysis once individual analysis is done for
    #each platform
    combinedAnalysis(path, args, name, 3, timeAnalysis, likesAnalysis, tagAnalysis, hourAnalysis, imageAnalysis, sentimentAnalysis, textAnalysis)




def combinedAnalysis(path, args, name, n, timeAnalysis, likesAnalysis, tagAnalysis, hourAnalysis, imageAnalysis, sentimentAnalysis, textAnalysis):
    """
    Function to analyze the data given across all platform
     ----------
     path: str
         Contains path of the parent file
     name: str
         Name of the user in this website
     args: dictionary
         Contains the user id's of a single across different platforms
     n: Integer
         Specifies the window for the time analysis
    """
    #**********************************************TIME ANALYSIS**********************************************
    #Create a folder for combined analysis
    localpath = localpath = path + '\\Output\\' + name + '\\Combined'
    if not os.path.exists(localpath):
           os.makedirs(localpath)

    #Perform time analysis for the data
    final_time = np.zeros(24, dtype = 'int64')
    name = 'hour_frequency'
    code = '<?php \n'
    code = code + '\t$' + name + ' = array(\n'

    #loop through the data available across platforms and merge them
    for i in timeAnalysis:
        #Chech if length of the data is greater than zero
        if len(i) > 0:
            final_time = np.add(final_time, i)

    #Generate the php code
    for j in range(24):
        if j == 23:
            code = code + '\t\tarray("label" => "'+ str(j) + ':00 - 00:00' + '", "y" => ' + str(final_time[j]) + ')\n'
        else:
            code = code + '\t\tarray("label" => "'+ str(j) + ':00 - ' + str(j + 1 ) + ':00' + '", "y" => ' + str(final_time[j]) + '),\n'
    code = code + '\t);\n?>'

    #Write the php code to a file
    fileName = os.path.join(localpath, name+".php")
    text_file = open(fileName, "w")
    text_file.write(code)
    text_file.close()

    #**********************************************LIKES ANALYSIS**********************************************
    #Perform analysis for the three different models considered
    labels = ['text_label']
    count = -1

    #Perform analysis for the three different ouptuts generated
    for label in labels:

        count += 1
        dictionary = {}

        #loop through the data available across platforms and merge them
        for i in range(len(likesAnalysis)):

            #Check if the length of the data is greater than zero
            if len(likesAnalysis[i]) != 0:
                sub_dictionary = likesAnalysis[i][count]
                dictionary = {k: dictionary.get(k, 0) + sub_dictionary.get(k, 0) for k in set(dictionary) | set(sub_dictionary)}


        name = 'Like_count_per_topic_' + label
        code = '<?php \n'
        code = code + '\t$likeCountPerTopic = array(\n'
        index = 0

        #Generate the php code
        for key, value in dictionary.items():
            if index == len(dictionary) - 1:
                code = code + '\t\tarray("label" => "'+ key + '", "y" => ' + str(value) + ')\n'
            else:
                code = code + '\t\tarray("label" => "'+ key + '", "y" => ' + str(value) + '),\n'
            index += 1
        code = code + '\t);\n?>'

        #Write the php code to a file
        fileName = os.path.join(localpath, name+".php")
        text_file = open(fileName, "w")
        text_file.write(code)
        text_file.close()


    #**********************************************HASHTAG ANALYSIS**********************************************
    final_tags = {}

    #Loop through the data available across platforms and merge them
    for i in tagAnalysis:
        final_tags = {k: final_tags.get(k, 0) + i.get(k, 0) for k in set(final_tags) | set(i)}

    #Sort the dictionary based in counts
    final_tags = dict(sorted(final_tags.items(), key = lambda x: x[1], reverse = True))
    name = 'hashtagCount'
    code = '<?php \n'
    code = code + '\t$hashtagCount = array(\n'
    i = 0

    #Generate the php file
    for key, value in final_tags.items():
        i = i + 1

        #Consider onl the top ten hashtags
        if i <=10:
            if i == len(final_tags) - 1:
                code = code + '\t\tarray("label" => "'+ key + '", "y" => ' + str(value) + ')\n'
            else:
                code = code + '\t\tarray("label" => "'+ key + '", "y" => ' + str(value) + '),\n'
    code = code + '\t);\n?>'

    #Write the php code to a file
    fileName = os.path.join(localpath, name+".php")
    with open(fileName, 'w', encoding='utf-8') as f:
        f.write(code)


    #**********************************************HOUR ANALYSIS**********************************************

    count = -1

    #Perform analysis for the three different ouptuts generated
    for label in labels:
        count += 1
        dictionary = {}

        #Loop through the data available through different platforms
        for i in range(len(hourAnalysis)):

            #Check if the length of the data is greater than zero
            if len(hourAnalysis[i]) != 0:
                sub_dictionary = hourAnalysis[i][count]
                dictionary = {k: np.add(dictionary.get(k, np.zeros(int(24/n))), sub_dictionary.get(k, np.zeros(int(24/n)))) for k in set(dictionary) | set(sub_dictionary)}

        name = 'Window_max_' + label
        code = '<?php \n'
        k = 0
        ranges = []

        #Generate the hour ranges
        for i in range(0, int(24/n)):
            ranges.append(str(k) + '-' + str(k + n))
            k += n

        #Generate the php code
        for key, value in dictionary.items():
            code = code + '\t$' + key + ' = array('
            for i in range(len(value)):
                if i == len(value) - 1:
                    code = code + str(int(value[i])) + ');\n'
                else:
                    code = code + str(int(value[i])) + ', '
        code = code + '?>'

        #Write the php code to a file
        fileName = os.path.join(localpath, name+".php")
        text_file = open(fileName, "w")
        text_file.write(code)
        text_file.close()

    #**********************************************IMAGE ANALYSIS**********************************************

    final_image_labels = {}
    name = 'frequency_image_label'

    #Loop through the data available through different platforms
    for i in imageAnalysis:

        #Check if the length of the data is greater than zero
        if len(i) != 0:
            final_image_labels= {k: final_image_labels.get(k, 0) + i.get(k, 0) for k in set(final_image_labels) | set(i)}

    #Sort the dictionary based on labels
    final_image_labels = dict(sorted(final_image_labels.items(), key = lambda x: x[1], reverse = True))
    code = '<?php \n'
    code = code + '\t$frequency_label_image = array(\n'

    #Generate the php code
    for key, value in final_image_labels.items():
        if i == len(final_image_labels) - 1:
            code = code + '\t\tarray("label" => "'+ key+ '", "y" => ' + str(int(value)) + ')\n'
        else:
            code = code + '\t\tarray("label" => "'+ key+ '", "y" => ' + str(int(value)) + '),\n'
    code = code + '\t);\n?>'

    #Write the php code to a file
    fileName = os.path.join(localpath, name+".php")
    text_file = open(fileName, "w")
    text_file.write(code)
    text_file.close()

    #**********************************************TEXT ANALYSIS**********************************************

    count = -1
    #Perform analysis for the three different ouptuts generated
    for label in labels:
       count += 1
       final_text_labels = {}

       #Loop through the data available through different platforms
       for i in textAnalysis:

           #Check if the length of the data is greater than zero
           if len(i) != 0:
               final_text_labels= {k: final_text_labels.get(k, 0) + i[count].get(k, 0) for k in set(final_text_labels) | set(i[count])}

       #Sort the dictionary based on frequencies
       final_text_labels = dict(sorted(final_text_labels.items(), key = lambda x: x[1], reverse = True))
       name = 'frequency_' + label
       code = '<?php \n'
       code = code + '\t$frequency_label_text = array(\n'

       #Generate the php code
       for key, value in final_text_labels.items():
            if i == len(final_text_labels) - 1:
                code = code + '\t\tarray("label" => "'+ key+ '", "y" => ' + str(int(value)) + ')\n'
            else:
                code = code + '\t\tarray("label" => "'+ key+ '", "y" => ' + str(int(value)) + '),\n'
       code = code + '\t);\n?>'

       #Write the php code to a file
       fileName = os.path.join(localpath, name+".php")
       text_file = open(fileName, "w")
       text_file.write(code)
       text_file.close()

    #**********************************************SENTIMENT ANALYSIS**********************************************

    count = -1
    #Perform analysis for the three different ouptuts generated
    for label in labels:
        count += 1
        dictionary = {}

        #Loop through the data available through different platforms
        for i in sentimentAnalysis:

            #Check if the length of the data is greater than zero
            if len(i) != 0:
                sub_dictionary = i[count]
                dictionary = {k: np.add(dictionary.get(k, np.zeros(3)), sub_dictionary.get(k, np.zeros(3))) for k in set(dictionary) | set(sub_dictionary)}

        name = 'positiveNegativeTextLabel'
        code = '<?php \n'

        #Generate the php code
        for key, value in dictionary.items():
            code = code + '\t$' + key + ' = array(\n'
            code = code + '\t\tarray("label" => "positive", "y" => ' + str(int(value[0])) + '),\n'
            code = code + '\t\tarray("label" => "neutral", "y" => ' + str(int(value[2])) + '),\n'
            code = code + '\t\tarray("label" => "negative", "y" => ' + str(int(value[1])) + ')\n\t);\n'
        code = code + '?>'

        splitOnes = code.split('$')
        dictionary = {'positive':{}, 'negative': {}, 'neutral': {}}
        for j in range(1, len(splitOnes)):
            seperateLines = splitOnes[j].split('\n')
            topic = seperateLines[0].split(' ')[0]
            dictionary['positive'][topic] = int(seperateLines[1].split('\"')[6].split('>')[1].split(')')[0])
            dictionary['neutral'][topic] = int(seperateLines[2].split('\"')[6].split('>')[1].split(')')[0])
            dictionary['negative'][topic] = int(seperateLines[3].split('\"')[6].split('>')[1].split(')')[0])
        code = '<?php \n'
        for j in dictionary.keys():
            code = code + '\t$' + j + ' = array(\n'
            if(j != {}):
                for k in dictionary[j].keys():
                    code = code + '\t\tarray("label" => "' + k + '", "y" => ' + str(dictionary[j][k]) + '),\n'
                code = code[:-2] + '\n\t);\n'
        code = code + '\n?>'


        #Write the php code to a file
        fileName = os.path.join(localpath, name+".php")
        text_file = open(fileName, "w")
        text_file.write(code)
        text_file.close()


def analyzeTime(dataframe, path):
    """
    Function to analyze the time series data given
     ----------
     path: str
         Contains path where the output needs to be written
     dataframe: pandas dataframe
         Contains user data specific to a platform
    """

    #Create a dummy array containing zeros initially
    array = np.zeros(24, dtype = 'int64')

    #If the dataframe is empty, return an empty array
    if(dataframe.shape[0] == 0):
        return []

    #loop through the rows of the dataframe and extract the hour from each
    for i in range(dataframe.shape[0]):
        temp = dataframe.iloc[i]['created'].hour
        #print(temp)
        import math
        if(not math.isnan(temp)):
            array[temp] = array[temp] + 1

    name = 'hour_frequency'
    code = '<?php \n'
    code = code + '\t$' + name + ' = array(\n'

    #Generate the php code
    for i in range(24):
        if i == 23:
            code = code + '\t\tarray("label" => "'+ str(i) + ':00 - 00:00' + '", "y" => ' + str(array[i]) + ')\n'
        else:
            code = code + '\t\tarray("label" => "'+ str(i) + ':00 - ' + str(i + 1 ) + ':00' + '", "y" => ' + str(array[i]) + '),\n'
    code = code + '\t);\n?>'

    #Write the php code to a file
    fileName = os.path.join(path, name+".php")
    text_file = open(fileName, "w")
    text_file.write(code)
    text_file.close()

    return array


def likeCountPerTopic(dataframe, path):
    """
    Function to analyze the time series data given
     ----------
     path: str
         Contains path where the output needs to be written
     dataframe: pandas dataframe
         Contains user data specific to a platform
    """

    output = []

    #If the dataframe is empty, return an empty array
    if(dataframe.shape[0] == 0):
        return output


    labels = ['text_label']

    #Perform analysis for the three different ouptuts generated
    for label in labels:
        dataframe = dataframe.sort_values(by = [label])
        dictionary = {}

        #Group by label and find the cumulative sum
        dataframe['like_count_per_topic'+label] = dataframe.groupby(label)['likeCount'].cumsum()
        df = dataframe.groupby([label], sort = False)['like_count_per_topic'+label].max()
        df = pd.DataFrame({label:df.index, 'likeCount':df.values})

        name = 'Like_count_per_topic_' + label
        code = '<?php \n'
        code = code + '\t$likeCountPerTopic = array(\n'

        #Generate the php code
        for i in range(df.shape[0]):
            dictionary[df.iloc[i][label]] = df.iloc[i]['likeCount']
            if i == df.shape[0] - 1:
                code = code + '\t\tarray("label" => "'+ df.iloc[i][label] + '", "y" => ' + str(df.iloc[i]['likeCount']) + ')\n'
            else:
                code = code + '\t\tarray("label" => "'+ df.iloc[i][label] + '", "y" => ' + str(df.iloc[i]['likeCount']) + '),\n'
        code = code + '\t);\n?>'

        #Write the php code to a file
        fileName = os.path.join(path, name+".php")
        text_file = open(fileName, "w")
        text_file.write(code)
        text_file.close()
        output.append(dictionary)

    return output


def hashtagCount(dataframe, path):
    """
    Function to analyze the time series data given
     ----------
     path: str
         Contains path where the output needs to be written
     dataframe: pandas dataframe
         Contains user data specific to a platform
    """
    #If the dataframe is empty, return an empty dictionary
    if(dataframe.shape[0] == 0):
        return {}

    #Get the list of hashtags used
    tagsList = dataframe.text_copy.str.findall(r'(?:(?<=\s)|(?<=^))#.*?(?=\s|$)')

    #Flatten the two - dimensional list containing hash tags and to lowercase
    tagsList = list(pd.core.common.flatten([x for x in tagsList if len(x) != 0]))
    tagsList = [x.lower() for x in tagsList]

    #Create a dictionary to associate a hastag to its count and sort it based on frequency
    dictionary = {x : tagsList.count(x) for x in tagsList}
    dictionary = dict(sorted(dictionary.items(), key = lambda x: dictionary[x[0]], reverse=True))
    name = 'hashtagCount'
    code = '<?php \n'
    code = code + '\t$hashtagCount = array(\n'
    i = 0

    #Generate the php code
    for key, value in dictionary.items():
        i = i + 1
        if i <=10:
            if i == len(dictionary) - 1:
                code = code + '\t\tarray("label" => "'+ key + '", "y" => ' + str(value) + ')\n'
            else:
                code = code + '\t\tarray("label" => "'+ key + '", "y" => ' + str(value) + '),\n'
    code = code + '\t);\n?>'

    #Write the php code to a file
    fileName = os.path.join(path, name+".php")
    with open(fileName, 'w', encoding='utf-8') as f:
        f.write(code)

    return dictionary

def topicPostHour(dataframe, path, n):
    """
    Function to analyze the time series data given
     ----------
     path: str
         Contains path where the output needs to be written
     dataframe: pandas dataframe
         Contains user data specific to a platform
     n: Integer
         Specifies the window for the time analysis
    """

    output = []

    #If the dataframe is empty, return an empty array
    if(dataframe.shape[0] == 0):
        return output

    labels = ['text_label']

    #Perform analysis for the three different ouptuts generated
    for label in labels:

        #Extract the hour attribute the data
        dataframe['created_hour'] = dataframe['created'].dt.hour

        #Get the unique labels in the dataframe
        columns = list(dataframe[label].unique())
        dictionary = {}


        #Create the time ranges for analysis
        df = pd.DataFrame(columns = columns, index = range(int(24/n)))
        ranges = []
        k = 0
        for i in range(0, int(24/n)):
            for j in columns:
                df.loc[i][str(j)] = len(dataframe[(dataframe['created_hour'] >= k) & (dataframe['created_hour'] < k + n) & (dataframe[label] == j)])
            ranges.append(str(k) + '-' + str(k + n))
            k += n
        df['Time'] = ranges

        name = 'Window_max_' + label
        code = '<?php \n'

        #For every label found, do the analysis
        for z in columns:
            sub = []
            code = code + '\t$' + z + ' = array('

            #Generate the php code
            for i in range(df.shape[0]):

                sub.append(df.loc[i][z])
                if i == df.shape[0] - 1:
                    code = code + str(df.loc[i][z]) + ');\n'
                else:
                    code = code + str(df.loc[i][z]) + ', '
            dictionary[z] = sub
        code = code + '?>'

        #Write the php code to a file
        fileName = os.path.join(path, name+".php")
        text_file = open(fileName, "w")
        text_file.write(code)
        text_file.close()
        output.append(dictionary)

    return output

def imagesPerTopic(dataframe, path):
    """
    Function to analyze the time series data given
     ----------
     path: str
         Contains path where the output needs to be written
     dataframe: pandas dataframe
         Contains user data specific to a platform
    """

    dictionary = {}

    #If the dataframe is empty, return an empty dictionary
    if(dataframe.shape[0] == 0):
        return dictionary

    label = 'image_label'

    #Create a dataframe based on label-frequency pair
    df = dataframe[label].value_counts().rename_axis('values').reset_index(name='counts')
    df.sort_values('values', ascending = True, inplace = True)

    name = 'frequency_' + label
    code = '<?php \n'
    code = code + '\t$frequency_label_image = array(\n'

    #Generate the php code
    for i in range(df.shape[0]):
        value = df.iloc[i]['values']
        dictionary[value] = df.iloc[i]['counts']
        if i == df.shape[0] - 1:
            code = code + '\t\tarray("label" => "'+ value + '", "y" => ' + str(int(df.iloc[i]['counts'])) + ')\n'
        else:
            code = code + '\t\tarray("label" => "'+ value + '", "y" => ' + str(int(df.iloc[i]['counts'])) + '),\n'
    code = code + '\t);\n?>'

    #Write the php code to a file
    fileName = os.path.join(path, name+".php")
    text_file = open(fileName, "w")
    text_file.write(code)
    text_file.close()

    return dictionary

def textPerTopic(dataframe, path):
    """
    Function to analyze the time series data given
     ----------
     path: str
         Contains path where the output needs to be written
     dataframe: pandas dataframe
         Contains user data specific to a platform
    """

    output = []

    #If the dataframe is empty, return an empty array
    if(dataframe.shape[0] == 0):
        return output

    labels = ['text_label']

    #Perform analysis for the three different ouptuts generated
    for label in labels:

        #Create a dataframe based on label-frequency pair
        df = dataframe[label].value_counts().rename_axis('values').reset_index(name='counts')
        df.sort_values('values', ascending = True, inplace = True)

        dictionary = {}
        name = 'frequency_' + label
        code = '<?php \n'
        code = code + '\t$frequency_label_text = array(\n'

        #Generate the php code
        for i in range(df.shape[0]):
            value = df.iloc[i]['values']
            dictionary[value] = df.iloc[i]['counts']
            if i == df.shape[0] - 1:
                code = code + '\t\tarray("label" => "'+ value + '", "y" => ' + str(int(df.iloc[i]['counts'])) + ')\n'
            else:
                code = code + '\t\tarray("label" => "'+ value + '", "y" => ' + str(int(df.iloc[i]['counts'])) + '),\n'
        code = code + '\t);\n?>'

        #Write the php code to a file
        fileName = os.path.join(path, name+".php")
        text_file = open(fileName, "w")
        text_file.write(code)
        text_file.close()
        output.append(dictionary)

    return output


def positiveNegative(dataframe, path):
    """
    Function to analyze the time series data given
     ----------
     path: str
         Contains path where the output needs to be written
     dataframe: pandas dataframe
         Contains user data specific to a platform
    """

    output = []

    #If the dataframe is empty, return an empty array
    if(dataframe.shape[0] == 0):
        return output

    labels = ['text_label']

    #Perform analysis for the three different ouptuts generated
    for label in labels:
        columns = list(dataframe[label].unique())
        name = 'positiveNegativeTextLabel'
        code = '<?php \n'
        dictionary = {}

        #Generate the php code for every label
        for z in columns:
            code = code + '\t$' + z + ' = array(\n'

            #Extract postive, negative and other posts and generate php code
            pos = len(dataframe[(dataframe['sentiment'] == 'Positive') & (dataframe[label] == z)])
            neg = len(dataframe[(dataframe['sentiment'] == 'Negative') & (dataframe[label] == z)])
            neutral =  len(dataframe[(dataframe['sentiment'] == 'Others') & (dataframe[label] == z)])
            dictionary[z] = [pos, neg, neutral]
            code = code + '\t\tarray("label" => "positive", "y" => ' + str(pos) + '),\n'
            code = code + '\t\tarray("label" => "neutral", "y" => ' + str(neutral) + '),\n'
            code = code + '\t\tarray("label" => "negative", "y" => ' + str(neg) + ')\n\t);\n'
        code = code + '?>'
        output.append(dictionary)

        splitOnes = code.split('$')
        dictionary = {'positive':{}, 'negative': {}, 'neutral': {}}
        for j in range(1, len(splitOnes)):
            seperateLines = splitOnes[j].split('\n')
            topic = seperateLines[0].split(' ')[0]
            dictionary['positive'][topic] = int(seperateLines[1].split('\"')[6].split('>')[1].split(')')[0])
            dictionary['neutral'][topic] = int(seperateLines[2].split('\"')[6].split('>')[1].split(')')[0])
            dictionary['negative'][topic] = int(seperateLines[3].split('\"')[6].split('>')[1].split(')')[0])
        code = '<?php \n'
        for j in dictionary.keys():
            code = code + '\t$' + j + ' = array(\n'
            for k in dictionary[j].keys():
                code = code + '\t\tarray("label" => "' + k + '", "y" => ' + str(dictionary[j][k]) + '),\n'
            code = code[:-2] + '\n\t);\n'
        code = code + '\n?>'

        #Write the php code to a file
        fileName = os.path.join(path, name+".php")
        text_file = open(fileName, "w")
        text_file.write(code)
        text_file.close()

    return output

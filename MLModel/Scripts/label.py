"""
    Module to load the trained models and predict the labes for user data
"""

#Importing library to load trained models
import joblib 

#Importing library to perform array operations
import numpy as np 

#Importing pandas to perform dataframe operations
#import pandas as pd

#Importing library to pre-process text
#from keras.preprocessing.text import Tokenizer
#from keras.preprocessing.sequence import pad_sequences
from gensim.models import Word2Vec
from nltk.tokenize import TweetTokenizer

#Importing library dependencies
#import tensorflow as tf

#Import library to load models
import pickle


def Get_Label_For_Text(dataframes, path):
    """
    Function to label the text data given
     ----------
     dataframes: DataFrame object
         dataframes containing the user's texts 
    """
     
    #Loading the models (and weights)
    print('Loading models for text classification')
    #linear_svc_model = joblib.load(path + '/Models/Text/Topic Classification/LinearSVCUpdated1.joblib')
    NB_model = joblib.load(path + '/Models/Text/Topic Classification/MultinomialNaiveBayers.joblib')
    #lstm_rnn_model = tf.keras.models.load_model(path + '/Models/Text/Topic Classification/Final_Model.hdf5', compile = False)
    #lstm_rnn_model.load_weights((path + '/Models/Text/Topic Classification/Final_Weights.hdf5'))
    
    tweet_w2v = Word2Vec.load(path + '/Models/Text/Sentiment Analysis/w2vmodel')
    with open(path + "/Models/Text/Sentiment Analysis/tfidfdict.txt", "rb") as myFile:
      tfidf = pickle.load(myFile)
    #loading model
    with open(path + "/Models/Text/Sentiment Analysis/classifier.sav", "rb") as myFile:
      classifier = pickle.load(myFile)
      
    
    print('Started labelling')
    
    #Define the labels
    labels = ['Food', 'Business', 'Fashion', 'Gaming', 'Movie', 'Music', 'Sports', 'Travel', 'Politics', 'Technology', 'Vehicles', 'Space', 'Medicine', 'Books']
    
    def buildWordVector(tokens, size):
        """
        Function to build a word vector
         ----------
         tokens: List
             List representing all the tokens
         size: Number
             Size of the word vector to be built
        """
        
        #Create a 1D array of 'size' shape and fill with zeros
        vec = np.zeros(size).reshape((1, size))
        count = 0
        
        #loop through the tokens
        for word in tokens:
            
            #Add the token to vector if word exists
            try:
                vec += tweet_w2v[word].reshape((1, size)) * tfidf[word]
                count += 1
                
            #If error continues, move to next word
            except KeyError:
                continue
        
        #Normalize the values when count is not zero
        if count != 0:
            vec /= count
            
        return vec
  
    #loop through the dataframes provided and label them
    for dataframe in dataframes:
        
        #Label only when the dataframe has at least one entry
        if(dataframe.shape[0] > 0):
            
            
            #Predict labels using the linear svc model and convert it to corresponding label
            #dataframe['text_label_linear_svc_model'] = linear_svc_model.predict(dataframe['text'])
            #dataframe['text_label_linear_svc_model'] = dataframe['text_label_linear_svc_model'].apply(lambda elem: labels[int(elem)-1])
            
            #Predict labels using the Naive Bayer's model and convert it to corresponding label
            dataframe['text_label'] = NB_model.predict(dataframe['text'])
            dataframe['text_label'] = dataframe['text_label'].apply(lambda elem: labels[int(elem)])
            
            
            #Create a tokenizer for the neural network model
            #tokenizer = createTokenizer(path)
            
            #Convert the raw data to a tensor
            #X = tokenizer.texts_to_sequences(dataframe['text'].values)
            
            # Max number of words in each complaint.
            #X = pad_sequences(X, maxlen=250)
            
            #Predict labels using the lstm rnn model and convert it to corresponding label
            #dataframe['text_label_lstm_rnn_model'] = [labels[np.argmax(p)] for p in lstm_rnn_model.predict(X)]
            
            #Define the labels for sentiment analysis
            classes = ['Negative', 'Positive']
            
            #Analyse the text for sentiment
            dataframe['sentiment'] = dataframe['text'].apply(lambda elem: tokenize(elem))
            dataframe['sentiment'] = dataframe['sentiment'].apply(lambda elem: buildWordVector(elem, 200))
            dataframe['sentiment'] = dataframe['sentiment'].apply(lambda elem: classifier.predict(elem))
            dataframe['sentiment'] = dataframe['sentiment'].apply(lambda elem: classes[elem[0]])
            
            
            #Set those labels having null strings as inappropriate
            dataframe.loc[dataframe.text.isin(['', ' ', 'nan']), 'text_label'] = 'Others'
            #dataframe.loc[dataframe.text.isin(['', ' ', 'nan']), 'text_label_linear_svc_model'] = 'Others'
            #dataframe.loc[dataframe.text.isin(['', ' ', 'nan']), 'text_label_lstm_rnn_model'] = 'Others'
            dataframe.loc[dataframe.text.isin(['', ' ', 'nan']), 'sentiment'] = 'Others'
        
        #If dataframe has no rows, move to next dataframe
        else:
            pass
    
    return dataframes
    
#def createTokenizer(path):
#    """
#    Function to create a tokenizer
#     ----------
#         path: str
#             Contains the current directory
#    """
#    
#    #Read the csv file to fit a tokenizer on
#    df = pd.read_csv(path + '/Scripts/formatted.csv')
#    
#    # The maximum number of words to be used. (most frequent)
#    tokenizer = Tokenizer(num_words = 50000, filters = '!"#$%&()*+,-./:;<=>?@[\]^_`{|}~')
#    
#    #Fit the tokenizer on the text data and return it                          
#    df['text'] = df['text'].astype(str)
#    tokenizer.fit_on_texts(df['text'].values)
#    
#    return tokenizer


def tokenize(text):
     """
     Function to create a tokenizer
     ----------
         text: str
              Text that needs to be tokenized
     """
     try:
         tokenizer = TweetTokenizer()
         text = text.lower()
         tokens = tokenizer.tokenize(text)
         tokens = list(filter(lambda t: not t.startswith('@'), tokens))
         tokens = list(filter(lambda t: not t.startswith('#'), tokens))
         tokens = list(filter(lambda t: not t.startswith('http'), tokens))
         return tokens
     
     except:
         return 'NC'



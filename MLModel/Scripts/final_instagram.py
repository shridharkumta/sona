"""
    Module to accept a instagram user-id and scrape their posts
"""

#Import dependencies for the instagram scraping to function
from insta.igramscraper.context import Instagram 

#Import library to command-line arguments
import argparse

#Importing pandas to perform dataframe operations
import pandas as pd

#Importing library to convert string to date-time object
from datetime import datetime

def Get_Instagram_Data(username):
    """
    Function to scrape data for the given instagram-id
     ----------
     Screen_name: str
         Instagram id of the user to be scraped
     """
     
    instagram = Instagram()
    
    #To obtain media from private profile uncomment the next two lines and
    #provide username and password
    
    #instagram.with_credentials('santosh_gunashekar', '84392Sis@1')
    #instagram.login()
    
    #If such user id does exist, return a dataframe containing information 
    #about the posts and return true
    try:
        print("Instagram scraping started")
        print("In progress...")
        #Obtain basic information about the user profile:
        account = instagram.get_account(username)
        
        #Get the medias for the user from instagram
        medias = instagram.get_medias(username, account.media_count)
        
        #Columns considered for scraping
        cols = ['image_url', 'text', 'thumbnail_src', 'likeCount', 'commentCount', 'created']
        complete_data = []
        
        #loop through the media and append necessary attributes
        for media in medias:
            arr = []
            arr.append(media.image_high_resolution_url)
            arr.append(media.caption)
            arr.append(media.thumbnail_src)
            arr.append(media.likes_count)
            arr.append(media.comments_count)
            arr.append(media.created)
            
            #Append details about each media to an array
            complete_data.append(arr)
            
        #Create a dataframe from the values obtained, fill null values with empty 
        #string and convert the date-time from string to date-time object
        dataframe = pd.DataFrame(data =  complete_data, columns = cols)
        dataframe.fillna(" ", inplace = True)
        dataframe['created'] = dataframe['created'].apply(lambda elem: datetime.utcfromtimestamp(elem).strftime('%Y-%m-%d %H:%M:%S'))
        dataframe['created'] = dataframe['created'].apply(lambda elem: datetime.strptime(elem, "%Y-%m-%d %H:%M:%S"))
        print("Instagram scraping done")
        
        return dataframe, True
    
    #If such user id does not exist, return an empty dataframe and return false
    except:
        print('No such user exists')
        return pd.DataFrame(columns = ['image_url', 'text', 'thumbnail_src', 'likeCount', 'commentCount', 'created']), False


if __name__ == "__main__":
    
    #Construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-n", "--name", required=True,
    	help="user id of the twitter user")
    args = vars(ap.parse_args())
    
    #Get the user id from the terminal
    username = args["name"]
    
    #Get the instagram data and write it to a csv file
    dataframe = Get_Instagram_Data(username)
    dataframe.to_csv(username+'_instagram.csv')
    
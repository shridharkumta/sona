"""
    Module to accept a reddit user-id and scrape their posts
"""

#Importing library that acts as a python-reddit API wrapper
import praw

#Importing pandas to perform database operations
import pandas as pd

#Importing library to convert string to date-time object
import datetime as dt

#Import library to command-line arguments
import argparse


def Get_Reddit_Data(reddit_id, limit = 100):
    """
    Function to scrape data for the given reddit-id
     ----------
     reddit_id: str
         reddit id of the user to be scraped
     limit: Integer
         number of posts to be scraped, 100, by default, if not specified
     """
     
    class Listables():
        """
        A class used to represent the list of posts retrieved from reddit
        ...
        Attributes
        ----------
        user : Object
            a Redditor object holding the information related to reddit posts
        overview: dict
            dictionary for different categories of posts as a key-value pair
        limit: int
            specifies the limit of the reddit posts to be scraped
    
        Methods
        -------
        extract:
            extracts the required attributes of a from the JSON response
        sort_submissions:
            sort the posts data in order 
        """
        def __init__(self, user, overview, limit):
            self.user = user
            self.overview = overview
            self.limit = limit
            self.submissions = user.submissions.new(limit = limit)
            
        def extract(self, cat, obj):
            """
            Function to extract attributes of posts from the JSON response
             ----------
             obj: Redditor Object 
                 contains the list of posts from the reddit user
             """
             
            #Iterate through the posts and extract infomration
            for item in obj:
                if isinstance(item,praw.models.Submission):
                    row = ["%s" % dt.datetime.fromtimestamp(item.created).strftime("%Y-%m-%d %H:%M:%S"),
                            "%s" % item.score,"%s" % item.upvote_ratio, "%s" % item.selftext, "%s" % item.subreddit.display_name]
                    
                    #Append it to the class attribute
                    self.overview["Submissions"].append(row)
    
        #Sort Redditor submissions
        def sort_submissions(self):
            """
            Function to sort the submissions/posts of reddit user
            """
            self.extract(None,self.submissions)
        
    #Initialize the reddit API credentials
    reddit = praw.Reddit(client_id = 'c0AKTFSQ_K87iQ', client_secret = '3fyuL1vio1Zu0aflde46X3ajbw4', 
                         user_agent = 'Sample_tutorial', username = 'santoshguna001', password = '84392sis')
    
    #If such user id does exist, return a dataframe containing information 
    #about the posts and return true
    try:
        print("Reddit scraping started")
        print("In progress...")
        
        #Retrieve posts, sort them
        titles = ["Submissions"]
        overview = dict((title,[]) for title in titles)
        listable = Listables(reddit.redditor(reddit_id),overview,limit = int(limit))
        listable.sort_submissions()
        sub_lst = overview['Submissions']
        
        #Create a dataframe the data obtained
        array = []
        for i in range(len(sub_lst)):
            array.append(list([sub_lst[i][0], int(sub_lst[i][1]), float(sub_lst[i][2]), sub_lst[i][3], 0, sub_lst[i][4]]))
        df = pd.DataFrame(array, columns = ['created','likeCount','likeRatio','text','Score','Sub reddit'])
        df['created'] = df['created'].apply(lambda elem: dt.datetime.strptime(elem, "%Y-%m-%d %H:%M:%S"))
        
        return df, True
    
    #If such user id does not exist, return an empty dataframe and return false
    except:
        print('No such user exists')
        return pd.DataFrame(columns = ['created','likeCount','likeRatio','text','Score','Sub reddit']), False


if __name__ == "__main__":
    
    #Construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-n", "--name", required=True,
    	help="user id of the reddit user")
    args = vars(ap.parse_args())
    
    #Get the user id from the terminal
    username = args["name"]
    
    #Scrape and save the dataframe of reddit's users posts to a csv file
    Data = pd.DataFrame()
    print("Starting to scrap {}".format(username))
    Data = Data.append(Get_Reddit_Data(username))
    Data.to_csv(username + '_reddit.csv', sep=',')



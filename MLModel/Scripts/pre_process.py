"""
    Module to pre-process text data for analysis. Methods include cleaning the
    text - removing stopwords, unwanted white spaces, links, mentions and other
    special symblols, lemmatizing a word to the base form
"""
#Importing pandas to perform dataframe operations
import pandas as pd

#Importing libraries for performing string operations
import re

#Importing library for obtaining a list of stopwords
from nltk.corpus import stopwords

#Importing library to Lemmatize a word
from nltk.stem import WordNetLemmatizer

#Importing library for tokenization of words and labelling them parts of speech 
from nltk import pos_tag, word_tokenize

#Library to accept command-line arguments
import argparse


def preProcessData(df, textField):
    """
    Function to pre-process a dataframe column including cleaning of text, removal of 
    stopwords and lemmatizing words
     ----------
     df: Dataframe Object
         Dataframe containing the information of the posts scraped
     textField: str
         textField is the name of the column in dataframe for which this 
         pre-processing needs to be applied
     """
    return clean_text(lemmatize_text(removal_of_stopwords(df, textField), textField), textField)

def clean_text(df, textField):
    """
    Function to pre=process a dataframe column - removal of white spaces,
    links, mentions, special symbols, converting to lower case and strip the
    text
    ----------
     df: Dataframe Object
         Dataframe containing the information of the posts scraped
     textField: str
         textField is the name of the column in dataframe for which this 
         pre-processing needs to be applied
    """
    df[textField] = df[textField].fillna('')
    df[textField] = df[textField].str.lower()
    df[textField] = df[textField].str.replace("'s", "")
    df[textField] = df[textField].apply(lambda elem: re.sub(r"(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)|^rt|http.+?", "", elem))  
    df[textField] = df[textField].apply(lambda elem: " ".join([i for i in word_tokenize(elem) if len(i) > 2]))
    df[textField] = df[textField].apply(lambda elem: re.sub(' +', ' ', elem))
    df[textField] = df[textField].str.strip()
    
    return df

def lemmatize_text(df, textField):
    """
    Function to pre=process a dataframe column - lemmatizing words for all the 
    words in the given text column
    ----------
     df: Dataframe Object
         Dataframe containing the information of the posts scraped
     textField: str
         textField is the name of the column in dataframe for which this 
         pre-processing needs to be applied
    """
    wordnet_lemmatizer = WordNetLemmatizer()
    lemmatized_text_list=[]
    
    #Iterate through the rows of the dataframe provided
    for row in range(len(df)):
        lemmatized_list = []
        
        #Extracting the text from the specified column
        text = df.loc[row][textField]
        
        #Loop through the words and lemmatize it, finally adding it to a list
        for word, tag in pos_tag(word_tokenize(text)):

            if tag.startswith("NN"):
               lemmatized_list.append(wordnet_lemmatizer.lemmatize(word, pos='n'))
            elif tag.startswith('VB'):
                lemmatized_list.append(wordnet_lemmatizer.lemmatize(word, pos='v'))
            elif tag.startswith('JJ'):
                lemmatized_list.append(wordnet_lemmatizer.lemmatize(word, pos='a'))
            else:
                lemmatized_list.append(word)
        
        #Join the words to one single sentence seperated by a white space
        lemmatized_text = " ".join(lemmatized_list)
        
        #Append all the lemmatized sentences into one array
        lemmatized_text_list.append(lemmatized_text)
        
    #Replace the column with the array of lemmatized sentences
    df[textField] = lemmatized_text_list
    
    return df


def removal_of_stopwords(df, textField):
    """
    Function to pre=process a dataframe column - removing the stop-words
    ----------
     df: Dataframe Object
         Dataframe containing the information of the posts scraped
     textField: str
         textField is the name of the column in dataframe for which this 
         pre-processing needs to be applied
    """
    
    #Have a copy of the original text
    df[textField + "_copy"] = df[textField] 
    
    #Get the list of stopwords
    stop_words = list(stopwords.words('english'))
    
    #For every row from the 'textField' column, tokenize the words and 
    #remove stopwords and create the new sentence 
    df[textField] = df[textField].apply(lambda elem: " ".join([i for i in word_tokenize(elem) if not i in stop_words]))
    
    return df


if __name__ == "__main__":
    
     #Construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-n", "--filename", required=True,
    	help="Name of the file that needs to be pre-processed (csv format)")
    args = vars(ap.parse_args())
    
    #Get the user id from the terminal
    filename = args["filename"]
    
    #Read the data, pre-process and write the pre-processed data to a csv file
    data = pd.read_csv(filename + '.csv', sep = ',')
    data = preProcessData(data, 'text')
    data.to_csv(filename + '_processed.csv', sep=',')
    
"""
    Module to accept a dataframe and download the images from the url's in it
    Pre-process them and classify them into one of the categories
"""

#Importing libraries to pre-process image, converting image to array and vice-
#versa and to load a trained model

from keras.applications.inception_v3 import preprocess_input
from keras.preprocessing.image import load_img, img_to_array
from keras.models import load_model

#Importing library to perform basic functionalities
import numpy as np

#Importing library to download images given a url
from urllib.request import urlopen

#Importing library to read the image downloaded
import cv2

#Importing library to perform file operations
import os


def URL_To_Image(url):
    """
    Function to download image given a url
     ----------
     url: str
         url of the image to be downloaded
     """

    #Open the url, convert the image to array and read it
    resp = urlopen(url)
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)

    return image

def Classify_Images(dfs, path):
    """
    Function to classify images from image urls column of a dataframe
     ----------
     dfs: Dataframe objects
         dataframes that contains image urls to be analysed
     """
    #load the model and its weights
    print('Loading model for image classification')
    model = load_model(path + '/Models/Images/Final_Model.hdf5')
    model.load_weights((path + '/Models/Images/Final_Weights.hdf5'))

    for df in dfs:
        #List that contains all the classs labels considered
        print(df.columns, ' labelling started')
        classes = ['Architecture', 'Food', 'Group', 'Nature', 'Potrait', 'Transport']
        class_list=[]

        #Iterate through the image urls and classify them
        try:
            for i in list(df["image_url"]):

                #Download iamge from url and convert it to array
                try:
                    image = URL_To_Image(i)
                    cv2.imwrite("image.jpg", image)
                    image = "image.jpg"
                    img = load_img(image, target_size=(224, 224))
                    x = img_to_array(img)

                    #Pre-process image and predicr the label it
                    x = np.expand_dims(x, axis=0)
                    x = preprocess_input(x)
                    result = model.predict(x)[0]

                    #If confidence is too low, assign it 'others' category
                    if max(result) < 0.65:
                        class_list.append('Others-'+classes[list(list(np.where(result == result.max()))[0])[0]])

                    #Assign the label to the class which has the highest confidence
                    else:
                        class_list.append(classes[list(list(np.where(result == result.max()))[0])[0]])
                except:
                    class_list.append('-1.0')
            #Add the labels to the dataframe
            df['image_label'] = class_list
        except (KeyError, ValueError):
            continue
        #Remove the image downloaded
        try:
            os.remove('image.jpg')
        except FileNotFoundError:
            continue

    return dfs

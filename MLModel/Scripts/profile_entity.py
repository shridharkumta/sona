"""
Module to accept user-ids of a user across different social media
platforms, pre-process, analyze and prepare data suitable for data
visualization
"""
import os



#Importing pandas to perform dataframe operations
import pandas as pd

#Importing library that helps in preprocessing of the data
from pre_process import preProcessData

#Libraries needed to scrape data from multiple social-media platforms
from final_twitter import Get_Twitter_Data
from final_instagram import Get_Instagram_Data
from final_reddit import Get_Reddit_Data
from final_facebook import Get_Facebook_Data

#Importing library to help caption the images posted by the user
from image_classify import Classify_Images

#Importing library to do the final precition
from label import Get_Label_For_Text

#Import library to perform analysis on labelled data
from analyze import Analyse

#Import library to record execution time
import time

import argparse

def scrapeData(user_id, platform, imageAnalysis):
    """
    Function to scrape public user data from different social media platforms
    Parameters
    ----------
    user_id: str
    Unique user id provided by a specific social media platform
    platform: str
    Platform name - Twitter, Reddit, Instagram or Facebook
    imagaAnalysis: str
    'True' if analysis of image needs to be done, else it is 'False'
    """

    #Creating a empty dataframe to store the data to be scraped
    data = pd.DataFrame()

    #Check if the platform is 'facebook' and retrieve data from it
    if(platform == 'Facebook'):
        scrapedData, user_found = Get_Facebook_Data(user_id, 10000)
        data = data.append(scrapedData)

        #Check if the platform is 'twitter' and retrieve data from it
    elif(platform == 'Twitter'):
        scrapedData, user_found = Get_Twitter_Data(user_id)
        data = data.append(scrapedData)

        #Check if the platform is 'instagram' and retrieve data from it
    elif(platform == 'Instagram'):
        scrapedData, user_found = Get_Instagram_Data(user_id)
        data = data.append(scrapedData)

        #Check if the platform is 'reddit' and retrieve data from it
    elif(platform == 'Reddit'):
        scrapedData, user_found = Get_Reddit_Data(user_id, 10000)
        data = data.append(scrapedData)

    #Pre-process the data if any data is obtained
    if(data.shape[0] > 0):
        #Print the shape of the data obtained from the social media platform
        print(platform, ' data shape is ', data.shape)

        dataPreProcessed = preProcessData(data, "text")
        print("Preprocessing done")

        #Return if no data is obtained
    else:
        return data, user_found

        #Return the dataframe
    return dataPreProcessed, user_found



def main():
    #Construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()

    #Add an argument for collecting twitter id
    ap.add_argument("-t", "--twitterid", required=False,
                    help="user id of the twitter user")

    #Add an argument for collecting instagram id
    ap.add_argument("-i", "--instagramid", required=False,
                    help="user id of the instagram user")

    #Add an argument to enable/disable image analysis
    ap.add_argument("-b", "--imageAnalysis", required=True,
                    help="True if image analysis is needed (Else False)")

    #Add an argument for collecting redditid
    ap.add_argument("-r", "--redditid", required=False,
                    help="user id of the reddit user")

    #Add an argument for collecting facebook id
    ap.add_argument("-f", "--facebookid", required=False,
                    help="page id of the facebook page")
    #Add an argument for collecting user id in SoNA
    ap.add_argument("-n", "--name", required=True,
                    help="user id in SoNA")
    args = vars(ap.parse_args())
    imageAnalysis = args['imageAnalysis']
    start_time = time.time()
    #Array to store all the scraped and processed data
    scrapedData = []
    #Get the current directory
    from pathlib import Path
    path = str(Path(__file__).parent.absolute())


    #If facebook id is provided, scrape the data from facebook for given id
    if(args['facebookid'] != None):
        scrapedDataFacebook, user_found = scrapeData(args['facebookid'], 'Facebook', imageAnalysis)
        scrapedData.append(scrapedDataFacebook)
        if scrapedDataFacebook.shape[0] == 0 and user_found:
            print('No posts found')
    else:
        scrapedData.append(pd.DataFrame())

    #If instagram id is provided, scrape the data from instagram for given id
    if(args['instagramid'] != None):
        scrapedDataInstagram, user_found = scrapeData(args['instagramid'], 'Instagram', imageAnalysis)
        scrapedData.append(scrapedDataInstagram)
        if scrapedDataInstagram.shape[0] == 0 and user_found:
            print('No posts found')
    else:
        scrapedData.append(pd.DataFrame())

    #If reddit id is provided, scrape the data from reddit for given id
    if(args['redditid'] != None):
        scrapedDataReddit, user_found = scrapeData(args['redditid'], 'Reddit', imageAnalysis)
        scrapedData.append(scrapedDataReddit)
        if scrapedDataReddit.shape[0] == 0 and user_found:
            print('No posts found')
    else:
        scrapedData.append(pd.DataFrame())

    #If twitter id is provided, scrape the data from twitter for given id
    if(args['twitterid'] != None):
        scrapedDataTwitter, user_found = scrapeData(args['twitterid'], 'Twitter', imageAnalysis)
        scrapedData.append(scrapedDataTwitter)
        if scrapedDataTwitter.shape[0] == 0 and user_found:
            print('No posts found')
    else:
        scrapedData.append(pd.DataFrame())

    #Labelling the text data
    dataLabelled = Get_Label_For_Text(scrapedData, path)

    #Perform image analysis if specified
    imageLabelled = dataLabelled
    if args['imageAnalysis'] == 'True':
        imageLabelled = Classify_Images(dataLabelled, path)


    #Perform Analysis on the labelled data
    Analyse(path, imageLabelled, args)
    print("Labelling of images done")

    #Print the total time taken
    print("--- %s seconds ---" % (time.time() - start_time))

if __name__ == "__main__":
    main()

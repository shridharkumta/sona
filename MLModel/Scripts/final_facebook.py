"""
    Module to accept a facebook page-id and scrape their posts
"""


#Importing library dependencies for the facebook scraper
#!pip install facebook_scraper
import facebook_scraper

#Importing pandas to perform database operations
import pandas as pd

#Import library to command-line arguments
import argparse

#Importing library to convert datetime from string to datetime object
from datetime import datetime

def Get_Facebook_Data(facebook_page_id, pages = 100):
    """
    Function to scrape data for the given page-id of a facebook
     ----------
     facebook_page_id: str
         facebook page id to be scraped
     limit: Integer
         number of posts to be scraped, 100, by default, if not specified
     """
    
    #If such page id does exist, return a dataframe containing information 
    #about the posts and return true 
    try:
        print("Facebook scraping started")
        print("In progress...")
        names = ['text', 'created', 'image_url', 'likeCount', 'commentCount', 'shareCount']
        array = []
        
        #Iterate through the posts scraped
        for post in facebook_scraper.get_posts(facebook_page_id, pages = pages):
            
            #Extract the attributes from the post scraped
            sub_array = []
            sub_array.append(post['text'])
            sub_array.append(post['time'])
            sub_array.append(post['image'])
            sub_array.append(post['likes'])
            sub_array.append(post['comments'])
            sub_array.append(post['shares'])
            array.append(sub_array)
            
        #Convert the data to a dataframe and return it
        data = pd.DataFrame(data = array, columns = names)
        return data, True
    
    #If such page id does not exist, return an empty dataframe and return false
    except:
        print('No such page exists')
        return pd.DataFrame(columns = ['text', 'created', 'image_url', 'likeCount', 'commentCount', 'shareCount']), False
        

if __name__ == "__main__":
    
    #Construct the argument parser and parse the arguments
    ap = argparse.ArgumentParser()
    ap.add_argument("-n", "--name", required=True,
    	help="page id of the facebook page")
    args = vars(ap.parse_args())
    
    #Get the user id from the terminal
    pagename = args["name"]        
    
    #Scrape posts and write it to csv file
    data = Get_Facebook_Data(pagename)
    data.to_csv(pagename+'_facebook.csv')
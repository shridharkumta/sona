# -*- coding: utf-8 -*-
"""
Created on Sat May  9 15:48:37 2020

@author: Santosh
"""
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 25 19:54:12 2020
@author: Santosh
"""

from context import Instagram # pylint: disable=no-name-in-module
import urllib.request
import os
import pandas as pd

total = 300
instagram = Instagram()
ids= [['indianrecipes', 'indiansnacks', 'indianfood'], ['indianmonuments', 'heritageofindia', 'heritagephotography'],
      ['buildings', 'architecturelovers', 'tallbuildings'], ['naturepics', 'naturephotographer', 'naturelover'],
      ['vehicles','bikes', 'instacars'], ['groupphotoshoot', 'withhumans', 'portrait_captures'],
      ['animalworld', 'animalgram', 'animalphotos']]
topics = ['food', 'Architecture', 'Buildings', 'nature', 'vehicles', 'Photoshoot', 'animals']

index = 0
for topic in ids:
    big_array = []
    
    for sub_topic in topic:
        
        #print('Number of published posts', account.media_count)
        medias = instagram.get_medias(sub_topic, 125)
        #print(len(medias))
        cols = ['image_high_resolution_url']
        account_cols = {}
        for media in medias:
            arr = media.image_high_resolution_url
            big_array.append(arr)
            if len(big_array) == 300:
                break
        
    dataframe = pd.DataFrame(data =  big_array, columns = cols)
    j = 0
    for i in range(dataframe.shape[0]):
        newpath = "C:/Users/Santosh/Desktop/Project_2/Images/" + topics[index] 
        if not os.path.exists(newpath):
            os.makedirs(newpath)
        urllib.request.urlretrieve(dataframe.image_high_resolution_url[i], 
                                   "C:/Users/Santosh/Desktop/Project_2/Images/"+topics[index]+"/image_" + str(j) +".jpg")
        j = j + 1
    index = index + 1
    
    
from fastai.vision import download_images
download_images('C:/Users/Santosh/Desktop/Project_2/Images/food/files', 'C:/Users/Santosh/Desktop/Project_2/Images/food')

# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 18:27:12 2020

@author: Santosh
"""

import pandas as pd
import numpy as np
import re
import nltk
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from nltk.stem.porter import PorterStemmer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import chi2
import numpy as np
import argparse


def  clean_text(df, text_field):
    df[text_field] = df[text_field].str.lower()
    df[text_field] = df[text_field].str.replace("'s", "")
    df[text_field] = df[text_field].apply(lambda elem: re.sub(r"(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)|^rt|http.+?", "", elem))  
    df[text_field] = df[text_field].apply(lambda elem: re.sub(' +', ' ', elem))
    df[text_field] = df[text_field].str.lstrip()
    return df

def lemmatize_text(df, text_field):
    wordnet_lemmatizer = WordNetLemmatizer()
    lemmatized_text_list=[]
    for row in range(len(df)):
        lemmatized_list = []
        text = df.loc[row][text_field]
        text_words = text.split(" ")
        for word in text_words:
            lemmatized_list.append(wordnet_lemmatizer.lemmatize(word, pos='v'))
        lemmatized_text = " ".join(lemmatized_list)
        lemmatized_text_list.append(lemmatized_text)
    df[text_field+"_lemmatized_text"] = lemmatized_text_list
    return df

def removal_of_stopwords(df, text_field):
    nltk.download('stopwords')
    stop_words = list(stopwords.words('english'))
    df[text_field+'_lemmatized_rem_stopwords'] = df[text_field+'_lemmatized_text']
    for stop_word in stop_words:
      regex_stopword = r"\b" + stop_word + r"\b"
      df[text_field+'_lemmatized_rem_stopwords'] = df[text_field+'_lemmatized_rem_stopwords'].str.replace(regex_stopword,'')
    return df

if __name__ == "__main__":
    nltk.download('punkt')
    nltk.download('wordnet')
    nltk.download('stopwords')
    
    ap = argparse.ArgumentParser()
    ap.add_argument("-n", "--filename", required=True,
    	help="Name of the file that needs to be pre-processed (csv format)")
    args = vars(ap.parse_args())
    
    #Get the user id from the terminal
    filename = args["filename"]
    #filename = 'ishridharhegde_twitter'
    data = pd.read_csv(filename + '.csv', sep = ',')
    data = removal_of_stopwords(lemmatize_text(clean_text(data, "text"), "text"), "text")
    data.to_csv(filename + '_processed.csv', sep=',')
    print('Done')
    
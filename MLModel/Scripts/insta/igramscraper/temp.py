# -*- coding: utf-8 -*-
"""
Created on Sun Apr 26 19:24:25 2020

@author: Santosh
"""

# -*- coding: utf-8 -*-
"""
Created on Sat Apr 25 19:54:12 2020
@author: Santosh
"""

from context import Instagram # pylint: disable=no-name-in-module
import argparse
import pandas as pd
import numpy as np

if __name__ == "__main__":
    
    #Construct the argument parser and parse the arguments
    #ap = argparse.ArgumentParser()
    #ap.add_argument("-n", "--name", required=True,
    #	help="user id of the twitter user")
    #args = vars(ap.parse_args())
    
    #Get the user id from the terminal
    #username = args["name"]
    username = 'ishridharhegde'
    # If account is public you can query Instagram without auth
    instagram = Instagram()
    
    # For getting information about account you don't need to auth:
    account = instagram.get_account(username)
    
    # Available fields
    #print('Account info:')
    #print('Id', account.identifier)
    #print('Username', account.username)
    #print('Full name', account.full_name)
    #print('Biography', account.biography)
    #print('Profile pic url', account.get_profile_picture_url())
    #print('External Url', account.external_url)
    #print('Number of published posts', account.media_count)
    #print('Number of followers', account.followed_by_count)
    #print('Number of follows', account.follows_count)
    #print('Is private', account.is_private)
    #print('Is verified', account.is_verified)
    #print(account.connected_fb_page)
    #print(account.country_block)
    #print(account.followed_by_viewer)
    #print(account.follows_viewer)
    #print(account.has_requested_viewer)
    #print(account.has_blocked_viewer)
    #print(account.has_channel)
            
    medias = instagram.get_medias(username, account.media_count)
    #print(len(medias))
    cols = ['type', 'link', 'image_high_resolution_url', 'caption', 'thumbnail_src', 'likes_count', 'comments_count']
    account_cols = {}
    account_cols['id'] = account.identifier
    account_cols['Username'] = account.username
    account_cols['Full name'] = account.full_name
    account_cols['Biography'] = account.biography
    account_cols['Profile pic url'] = account.get_profile_picture_url()
    account_cols['External Url'] = account.external_url
    account_cols['Number of published posts'] = account.media_count
    account_cols['Number of followers'] = account.followed_by_count
    account_cols['Number of follows'] = account.follows_count
    account_cols['Is private'] = account.is_private
    account_cols['Is verified'] = account.is_private
    (pd.DataFrame.from_dict(data=account_cols, orient='index').to_csv(username + '_instagram_meta_data.csv', header=False))
    
    big_array = []
    for media in medias:
        arr = []
        arr.append(media.type)
        arr.append(media.link)
        arr.append(media.image_high_resolution_url)
        arr.append(media.caption)
        arr.append(media.thumbnail_src)
        arr.append(media.likes_count)
        arr.append(media.comments_count)
        big_array.append(arr)
    dataframe = pd.DataFrame(data =  big_array, columns = cols)
    #dataframe = dataframe.drop(['identifier', 'short_code', 'created_time', 'image_low_resolution_url','image_thumbnail_url', 'image_standard_resolution_url', 'carousel_media'], axis = 1)
    #dataframe = dataframe.drop(['is_ad', 'video_low_resolution_url', 'video_standard_resolution_url', 'video_low_bandwidth_url', 'video_url', 'video_views', 'owner', 'likes_count', 'location_id', 'location_name', 'comments_count'], axis = 1)
    #dataframe = dataframe.drop(['has_more_comments', 'has_more_comments', 'comments_next_page', 'location_slug', '_is_new', '_is_loaded', '_is_load_empty', '_is_fake', '_modified', '_data', 'modified', 'video_view_count'], axis = 1)
    dataframe["type"].fillna(" ", inplace = True)
    dataframe["link"].fillna(" ", inplace = True)
    dataframe["image_high_resolution_url"].fillna(" ", inplace = True)
    dataframe["caption"].fillna(" ", inplace = True)
    dataframe["text"] = dataframe["caption"]
    dataframe.drop(['caption'], axis = 1, inplace = True)
    dataframe["thumbnail_src"].fillna(" ", inplace = True)
    dataframe["likes_count"].fillna(" ", inplace = True)
    dataframe["comments_count"].fillna(" ", inplace = True)
    
    dataframe.to_csv(username+'_instagram.csv')
<?php 
	$positive = array(
		array("label" => "Sports", "y" => 144),
		array("label" => "Travel", "y" => 72),
		array("label" => "Vehicles", "y" => 29),
		array("label" => "Space", "y" => 63),
		array("label" => "Movie", "y" => 112),
		array("label" => "Fashion", "y" => 71),
		array("label" => "Medicine", "y" => 72),
		array("label" => "Business", "y" => 586),
		array("label" => "Politics", "y" => 290),
		array("label" => "Gaming", "y" => 176),
		array("label" => "Food", "y" => 202),
		array("label" => "Music", "y" => 108),
		array("label" => "Books", "y" => 47),
		array("label" => "Technology", "y" => 77)
	);
	$negative = array(
		array("label" => "Sports", "y" => 60),
		array("label" => "Travel", "y" => 396),
		array("label" => "Vehicles", "y" => 17),
		array("label" => "Space", "y" => 35),
		array("label" => "Movie", "y" => 33),
		array("label" => "Fashion", "y" => 31),
		array("label" => "Medicine", "y" => 21),
		array("label" => "Business", "y" => 195),
		array("label" => "Politics", "y" => 178),
		array("label" => "Gaming", "y" => 86),
		array("label" => "Food", "y" => 70),
		array("label" => "Music", "y" => 34),
		array("label" => "Books", "y" => 15),
		array("label" => "Technology", "y" => 30)
	);
	$neutral = array(
		array("label" => "Sports", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Technology", "y" => 0)
	);

?>
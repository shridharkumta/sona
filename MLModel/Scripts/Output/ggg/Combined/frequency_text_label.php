<?php 
	$frequency_label_text = array(
		array("label" => "Business", "y" => 781),
		array("label" => "Politics", "y" => 468),
		array("label" => "Travel", "y" => 468),
		array("label" => "Food", "y" => 272),
		array("label" => "Gaming", "y" => 262),
		array("label" => "Sports", "y" => 204),
		array("label" => "Movie", "y" => 145),
		array("label" => "Music", "y" => 142),
		array("label" => "Technology", "y" => 107),
		array("label" => "Fashion", "y" => 102),
		array("label" => "Space", "y" => 98),
		array("label" => "Medicine", "y" => 93),
		array("label" => "Books", "y" => 62),
		array("label" => "Vehicles", "y" => 46),
	);
?>
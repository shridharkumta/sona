<?php 
	$hour_frequency = array(
		array("label" => "0:00 - 1:00", "y" => 9),
		array("label" => "1:00 - 2:00", "y" => 6),
		array("label" => "2:00 - 3:00", "y" => 15),
		array("label" => "3:00 - 4:00", "y" => 33),
		array("label" => "4:00 - 5:00", "y" => 130),
		array("label" => "5:00 - 6:00", "y" => 230),
		array("label" => "6:00 - 7:00", "y" => 327),
		array("label" => "7:00 - 8:00", "y" => 235),
		array("label" => "8:00 - 9:00", "y" => 198),
		array("label" => "9:00 - 10:00", "y" => 194),
		array("label" => "10:00 - 11:00", "y" => 183),
		array("label" => "11:00 - 12:00", "y" => 142),
		array("label" => "12:00 - 13:00", "y" => 147),
		array("label" => "13:00 - 14:00", "y" => 193),
		array("label" => "14:00 - 15:00", "y" => 163),
		array("label" => "15:00 - 16:00", "y" => 241),
		array("label" => "16:00 - 17:00", "y" => 203),
		array("label" => "17:00 - 18:00", "y" => 190),
		array("label" => "18:00 - 19:00", "y" => 162),
		array("label" => "19:00 - 20:00", "y" => 125),
		array("label" => "20:00 - 21:00", "y" => 54),
		array("label" => "21:00 - 22:00", "y" => 35),
		array("label" => "22:00 - 23:00", "y" => 18),
		array("label" => "23:00 - 00:00", "y" => 17)
	);
?>
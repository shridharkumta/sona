<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 729),
		array("label" => "Business", "y" => 11440),
		array("label" => "Fashion", "y" => 1700),
		array("label" => "Food", "y" => 5491),
		array("label" => "Gaming", "y" => 3684),
		array("label" => "Medicine", "y" => 1165),
		array("label" => "Movie", "y" => 1953),
		array("label" => "Music", "y" => 2139),
		array("label" => "Politics", "y" => 7055),
		array("label" => "Space", "y" => 1245),
		array("label" => "Sports", "y" => 3930),
		array("label" => "Technology", "y" => 2023),
		array("label" => "Travel", "y" => 7524),
		array("label" => "Vehicles", "y" => 522)
	);
?>
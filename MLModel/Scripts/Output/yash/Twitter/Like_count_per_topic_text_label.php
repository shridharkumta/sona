<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 3717),
		array("label" => "Business", "y" => 125038),
		array("label" => "Fashion", "y" => 19657),
		array("label" => "Food", "y" => 159364),
		array("label" => "Gaming", "y" => 111390),
		array("label" => "Medicine", "y" => 15940),
		array("label" => "Movie", "y" => 685780),
		array("label" => "Music", "y" => 233783),
		array("label" => "Others", "y" => 34265),
		array("label" => "Politics", "y" => 139191),
		array("label" => "Space", "y" => 18991),
		array("label" => "Sports", "y" => 135787),
		array("label" => "Technology", "y" => 76357),
		array("label" => "Travel", "y" => 166204),
		array("label" => "Vehicles", "y" => 45822)
	);
?>
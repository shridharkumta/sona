<?php 
	$frequency_label_text = array(
		array("label" => "Movie", "y" => 129),
		array("label" => "Travel", "y" => 69),
		array("label" => "Food", "y" => 49),
		array("label" => "Sports", "y" => 47),
		array("label" => "Politics", "y" => 47),
		array("label" => "Music", "y" => 44),
		array("label" => "Gaming", "y" => 29),
		array("label" => "Technology", "y" => 26),
		array("label" => "Business", "y" => 24),
		array("label" => "Fashion", "y" => 11),
		array("label" => "Space", "y" => 7),
		array("label" => "Others", "y" => 6),
		array("label" => "Vehicles", "y" => 4),
		array("label" => "Books", "y" => 4),
		array("label" => "Medicine", "y" => 3),
	);
?>
<?php 
	$likeCountPerTopic = array(
		array("label" => "Fashion", "y" => 2163849),
		array("label" => "Medicine", "y" => 420094),
		array("label" => "Vehicles", "y" => 174979),
		array("label" => "Food", "y" => 6403854),
		array("label" => "Movie", "y" => 8294975),
		array("label" => "Technology", "y" => 76357),
		array("label" => "Sports", "y" => 2206287),
		array("label" => "Gaming", "y" => 3024341),
		array("label" => "Business", "y" => 1708037),
		array("label" => "Books", "y" => 673399),
		array("label" => "Travel", "y" => 3912097),
		array("label" => "Space", "y" => 155610),
		array("label" => "Music", "y" => 2502832),
		array("label" => "Politics", "y" => 5797842),
		array("label" => "Others", "y" => 993001)
	);
?>
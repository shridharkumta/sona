<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 669682),
		array("label" => "Business", "y" => 1582999),
		array("label" => "Fashion", "y" => 2144192),
		array("label" => "Food", "y" => 6244490),
		array("label" => "Gaming", "y" => 2912951),
		array("label" => "Medicine", "y" => 404154),
		array("label" => "Movie", "y" => 7609195),
		array("label" => "Music", "y" => 2269049),
		array("label" => "Others", "y" => 958736),
		array("label" => "Politics", "y" => 5658651),
		array("label" => "Space", "y" => 136619),
		array("label" => "Sports", "y" => 2070500),
		array("label" => "Travel", "y" => 3745893),
		array("label" => "Vehicles", "y" => 129157)
	);
?>
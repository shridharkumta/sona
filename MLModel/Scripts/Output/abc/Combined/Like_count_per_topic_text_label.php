<?php 
	$likeCountPerTopic = array(
		array("label" => "Medicine", "y" => 420094),
		array("label" => "Food", "y" => 6403965),
		array("label" => "Space", "y" => 155614),
		array("label" => "Business", "y" => 1708056),
		array("label" => "Travel", "y" => 3912162),
		array("label" => "Sports", "y" => 2206313),
		array("label" => "Others", "y" => 993006),
		array("label" => "Books", "y" => 673403),
		array("label" => "Technology", "y" => 76417),
		array("label" => "Fashion", "y" => 2163856),
		array("label" => "Politics", "y" => 5797906),
		array("label" => "Movie", "y" => 8295166),
		array("label" => "Vehicles", "y" => 174979),
		array("label" => "Gaming", "y" => 3024360),
		array("label" => "Music", "y" => 2502849)
	);
?>
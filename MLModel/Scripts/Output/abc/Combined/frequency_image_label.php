<?php 
	$frequency_label_image = array(
		array("label" => "-1.0", "y" => 272),
		array("label" => "Potrait", "y" => 99),
		array("label" => "Group", "y" => 77),
		array("label" => "Food", "y" => 19),
		array("label" => "Transport", "y" => 13),
		array("label" => "Others-Potrait", "y" => 7),
		array("label" => "Others-Transport", "y" => 5),
		array("label" => "Others-Group", "y" => 4),
		array("label" => "Others-Food", "y" => 2),
		array("label" => "Nature", "y" => 1),
	);
?>
<?php 
	$positive = array(
		array("label" => "Medicine", "y" => 2),
		array("label" => "Food", "y" => 25),
		array("label" => "Space", "y" => 5),
		array("label" => "Business", "y" => 23),
		array("label" => "Travel", "y" => 10),
		array("label" => "Sports", "y" => 35),
		array("label" => "Others", "y" => 0),
		array("label" => "Books", "y" => 4),
		array("label" => "Technology", "y" => 24),
		array("label" => "Fashion", "y" => 8),
		array("label" => "Politics", "y" => 33),
		array("label" => "Movie", "y" => 111),
		array("label" => "Vehicles", "y" => 3),
		array("label" => "Gaming", "y" => 22),
		array("label" => "Music", "y" => 40)
	);
	$negative = array(
		array("label" => "Medicine", "y" => 1),
		array("label" => "Food", "y" => 24),
		array("label" => "Space", "y" => 2),
		array("label" => "Business", "y" => 1),
		array("label" => "Travel", "y" => 59),
		array("label" => "Sports", "y" => 12),
		array("label" => "Others", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Technology", "y" => 2),
		array("label" => "Fashion", "y" => 3),
		array("label" => "Politics", "y" => 14),
		array("label" => "Movie", "y" => 18),
		array("label" => "Vehicles", "y" => 1),
		array("label" => "Gaming", "y" => 7),
		array("label" => "Music", "y" => 4)
	);
	$neutral = array(
		array("label" => "Medicine", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Others", "y" => 6),
		array("label" => "Books", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Music", "y" => 0)
	);

?>
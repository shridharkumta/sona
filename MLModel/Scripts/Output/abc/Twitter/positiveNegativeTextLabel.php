<?php 
	$positive = array(
		array("label" => "Politics", "y" => 24),
		array("label" => "Movie", "y" => 97),
		array("label" => "Travel", "y" => 5),
		array("label" => "Music", "y" => 34),
		array("label" => "Business", "y" => 20),
		array("label" => "Food", "y" => 18),
		array("label" => "Sports", "y" => 30),
		array("label" => "Gaming", "y" => 18),
		array("label" => "Technology", "y" => 24),
		array("label" => "Fashion", "y" => 3),
		array("label" => "Space", "y" => 4),
		array("label" => "Books", "y" => 2),
		array("label" => "Others", "y" => 0),
		array("label" => "Vehicles", "y" => 2),
		array("label" => "Medicine", "y" => 1)
	);
	$negative = array(
		array("label" => "Politics", "y" => 13),
		array("label" => "Movie", "y" => 15),
		array("label" => "Travel", "y" => 56),
		array("label" => "Music", "y" => 4),
		array("label" => "Business", "y" => 1),
		array("label" => "Food", "y" => 21),
		array("label" => "Sports", "y" => 11),
		array("label" => "Gaming", "y" => 4),
		array("label" => "Technology", "y" => 2),
		array("label" => "Fashion", "y" => 3),
		array("label" => "Space", "y" => 2),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 0),
		array("label" => "Vehicles", "y" => 1),
		array("label" => "Medicine", "y" => 1)
	);
	$neutral = array(
		array("label" => "Politics", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Others", "y" => 3),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Medicine", "y" => 0)
	);

?>
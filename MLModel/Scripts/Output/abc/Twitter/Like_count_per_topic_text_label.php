<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 3721),
		array("label" => "Business", "y" => 125054),
		array("label" => "Fashion", "y" => 19661),
		array("label" => "Food", "y" => 159468),
		array("label" => "Gaming", "y" => 111406),
		array("label" => "Medicine", "y" => 15940),
		array("label" => "Movie", "y" => 685836),
		array("label" => "Music", "y" => 233795),
		array("label" => "Others", "y" => 34269),
		array("label" => "Politics", "y" => 139235),
		array("label" => "Space", "y" => 18995),
		array("label" => "Sports", "y" => 135807),
		array("label" => "Technology", "y" => 76417),
		array("label" => "Travel", "y" => 166260),
		array("label" => "Vehicles", "y" => 45822)
	);
?>
<?php 
	$frequency_label_text = array(
		array("label" => "Books", "y" => 2),
		array("label" => "Business", "y" => 21),
		array("label" => "Fashion", "y" => 6),
		array("label" => "Food", "y" => 39),
		array("label" => "Gaming", "y" => 22),
		array("label" => "Medicine", "y" => 2),
		array("label" => "Movie", "y" => 112),
		array("label" => "Music", "y" => 38),
		array("label" => "Others", "y" => 3),
		array("label" => "Politics", "y" => 37),
		array("label" => "Space", "y" => 6),
		array("label" => "Sports", "y" => 41),
		array("label" => "Technology", "y" => 26),
		array("label" => "Travel", "y" => 61),
		array("label" => "Vehicles", "y" => 3)
	);
?>
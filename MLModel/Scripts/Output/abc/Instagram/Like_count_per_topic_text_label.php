<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 669682),
		array("label" => "Business", "y" => 1583002),
		array("label" => "Fashion", "y" => 2144195),
		array("label" => "Food", "y" => 6244497),
		array("label" => "Gaming", "y" => 2912954),
		array("label" => "Medicine", "y" => 404154),
		array("label" => "Movie", "y" => 7609330),
		array("label" => "Music", "y" => 2269054),
		array("label" => "Others", "y" => 958737),
		array("label" => "Politics", "y" => 5658671),
		array("label" => "Space", "y" => 136619),
		array("label" => "Sports", "y" => 2070506),
		array("label" => "Travel", "y" => 3745902),
		array("label" => "Vehicles", "y" => 129157)
	);
?>
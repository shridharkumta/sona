<?php 
	$positive = array(
		array("label" => "Politics", "y" => 265),
		array("label" => "Business", "y" => 509),
		array("label" => "Sports", "y" => 135),
		array("label" => "Gaming", "y" => 161),
		array("label" => "Space", "y" => 64),
		array("label" => "Music", "y" => 112),
		array("label" => "Food", "y" => 190),
		array("label" => "Travel", "y" => 48),
		array("label" => "Vehicles", "y" => 34),
		array("label" => "Movie", "y" => 102),
		array("label" => "Fashion", "y" => 60),
		array("label" => "Books", "y" => 21),
		array("label" => "Technology", "y" => 61),
		array("label" => "Medicine", "y" => 49),
		array("label" => "Others", "y" => 0)
	);
	$negative = array(
		array("label" => "Politics", "y" => 245),
		array("label" => "Business", "y" => 231),
		array("label" => "Sports", "y" => 81),
		array("label" => "Gaming", "y" => 83),
		array("label" => "Space", "y" => 43),
		array("label" => "Music", "y" => 46),
		array("label" => "Food", "y" => 102),
		array("label" => "Travel", "y" => 388),
		array("label" => "Vehicles", "y" => 33),
		array("label" => "Movie", "y" => 49),
		array("label" => "Fashion", "y" => 50),
		array("label" => "Books", "y" => 5),
		array("label" => "Technology", "y" => 39),
		array("label" => "Medicine", "y" => 54),
		array("label" => "Others", "y" => 0)
	);
	$neutral = array(
		array("label" => "Politics", "y" => 0),
		array("label" => "Business", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Food", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Others", "y" => 1)
	);

?>
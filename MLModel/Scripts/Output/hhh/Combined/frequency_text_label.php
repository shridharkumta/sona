<?php 
	$frequency_label_text = array(
		array("label" => "Business", "y" => 740),
		array("label" => "Politics", "y" => 510),
		array("label" => "Travel", "y" => 436),
		array("label" => "Food", "y" => 292),
		array("label" => "Gaming", "y" => 244),
		array("label" => "Sports", "y" => 216),
		array("label" => "Music", "y" => 158),
		array("label" => "Movie", "y" => 151),
		array("label" => "Fashion", "y" => 110),
		array("label" => "Space", "y" => 107),
		array("label" => "Medicine", "y" => 103),
		array("label" => "Technology", "y" => 100),
		array("label" => "Vehicles", "y" => 67),
		array("label" => "Books", "y" => 26),
		array("label" => "Others", "y" => 1),
	);
?>
<?php 
	$frequency_label_text = array(
		array("label" => "Business", "y" => 211),
		array("label" => "Politics", "y" => 168),
		array("label" => "Medicine", "y" => 101),
		array("label" => "Food", "y" => 72),
		array("label" => "Travel", "y" => 61),
		array("label" => "Movie", "y" => 57),
		array("label" => "Gaming", "y" => 53),
		array("label" => "Space", "y" => 52),
		array("label" => "Technology", "y" => 40),
		array("label" => "Sports", "y" => 35),
		array("label" => "Music", "y" => 32),
		array("label" => "Books", "y" => 20),
		array("label" => "Vehicles", "y" => 17),
		array("label" => "Fashion", "y" => 11),
		array("label" => "Others", "y" => 1),
	);
?>
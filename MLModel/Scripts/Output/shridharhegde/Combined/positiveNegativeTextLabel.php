<?php 
	$positive = array(
		array("label" => "Vehicles", "y" => 4),
		array("label" => "Books", "y" => 15),
		array("label" => "Medicine", "y" => 42),
		array("label" => "Movie", "y" => 36),
		array("label" => "Space", "y" => 32),
		array("label" => "Travel", "y" => 10),
		array("label" => "Others", "y" => 0),
		array("label" => "Food", "y" => 49),
		array("label" => "Politics", "y" => 92),
		array("label" => "Gaming", "y" => 34),
		array("label" => "Fashion", "y" => 8),
		array("label" => "Sports", "y" => 22),
		array("label" => "Technology", "y" => 22),
		array("label" => "Music", "y" => 23),
		array("label" => "Business", "y" => 160)
	);
	$negative = array(
		array("label" => "Vehicles", "y" => 13),
		array("label" => "Books", "y" => 5),
		array("label" => "Medicine", "y" => 59),
		array("label" => "Movie", "y" => 21),
		array("label" => "Space", "y" => 20),
		array("label" => "Travel", "y" => 51),
		array("label" => "Others", "y" => 0),
		array("label" => "Food", "y" => 23),
		array("label" => "Politics", "y" => 76),
		array("label" => "Gaming", "y" => 19),
		array("label" => "Fashion", "y" => 3),
		array("label" => "Sports", "y" => 13),
		array("label" => "Technology", "y" => 18),
		array("label" => "Music", "y" => 9),
		array("label" => "Business", "y" => 51)
	);
	$neutral = array(
		array("label" => "Vehicles", "y" => 0),
		array("label" => "Books", "y" => 0),
		array("label" => "Medicine", "y" => 0),
		array("label" => "Movie", "y" => 0),
		array("label" => "Space", "y" => 0),
		array("label" => "Travel", "y" => 0),
		array("label" => "Others", "y" => 1),
		array("label" => "Food", "y" => 0),
		array("label" => "Politics", "y" => 0),
		array("label" => "Gaming", "y" => 0),
		array("label" => "Fashion", "y" => 0),
		array("label" => "Sports", "y" => 0),
		array("label" => "Technology", "y" => 0),
		array("label" => "Music", "y" => 0),
		array("label" => "Business", "y" => 0)
	);

?>
<?php 
	$likeCountPerTopic = array(
		array("label" => "Books", "y" => 6),
		array("label" => "Business", "y" => 69),
		array("label" => "Fashion", "y" => 7),
		array("label" => "Food", "y" => 23),
		array("label" => "Gaming", "y" => 16),
		array("label" => "Medicine", "y" => 42),
		array("label" => "Movie", "y" => 21),
		array("label" => "Music", "y" => 14),
		array("label" => "Others", "y" => 0),
		array("label" => "Politics", "y" => 111),
		array("label" => "Space", "y" => 26),
		array("label" => "Sports", "y" => 13),
		array("label" => "Technology", "y" => 21),
		array("label" => "Travel", "y" => 23),
		array("label" => "Vehicles", "y" => 8)
	);
?>
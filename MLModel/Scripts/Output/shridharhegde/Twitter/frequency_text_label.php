<?php 
	$frequency_label_text = array(
		array("label" => "Books", "y" => 20),
		array("label" => "Business", "y" => 211),
		array("label" => "Fashion", "y" => 11),
		array("label" => "Food", "y" => 72),
		array("label" => "Gaming", "y" => 53),
		array("label" => "Medicine", "y" => 101),
		array("label" => "Movie", "y" => 57),
		array("label" => "Music", "y" => 32),
		array("label" => "Others", "y" => 1),
		array("label" => "Politics", "y" => 168),
		array("label" => "Space", "y" => 52),
		array("label" => "Sports", "y" => 35),
		array("label" => "Technology", "y" => 40),
		array("label" => "Travel", "y" => 61),
		array("label" => "Vehicles", "y" => 17)
	);
?>
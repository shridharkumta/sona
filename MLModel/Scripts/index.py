# -*- coding: utf-8 -*-
"""
Created on Sun Jun 28 19:53:37 2020

@author: Santosh
"""

import pandas as pd

import profile_entity

userids = pd.read_excel('Celeb_info.xlsx')
userids = userids.fillna('')

userids['facebookid'] =  userids['facebook link'].apply(lambda elem: elem.split(r'/')[3] if elem != '' else '')

userids['instagramid'] =  userids['Instagram link'].apply(lambda elem: elem.split(r'/')[3] if elem != '' else '')

userids['twitterid'] = userids['Twitter link'].apply(lambda elem: elem.split(r'/')[3] if elem != '' else '').apply(lambda elem: elem.split('?')[0])

userids['redditid'] = userids['reddit link'].apply(lambda elem: elem.split(r'/')[4] if elem != '' else '')

#for i in range(20,40):
args = {'name': 'santoshguna001',
        'twitterid': 'santoshguna001', 
        'facebookid': None,
        'redditid': None,
        'instagramid': None,
        'imageAnalysis': 'False'
        }
print(args)
profile_entity.main(args)
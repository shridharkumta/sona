# -*- coding: utf-8 -*-
"""
Created on Tue May  5 11:46:27 2020

@author: Santosh
"""

#from nltk.classify import NaiveBayesClassifier
#classifier = NaiveBayesClassifier.train(train_set)
#classifer.classify(unlabelled_instance)
#classifier.classify_many(unlabelled_instances)
#
#nltk.classify.util.accuracy(classifier, test_set)
#classifier.labels()
#classifier.show_most_informative_features()

import pandas as pd
df = pd.read_csv('updated_data_1.csv')
print(df.shape)
df_new = pd.DataFrame.drop_duplicates(df)
print(df_new.shape)

x = df_new.drop(['label'], axis = 1)
y = df_new.drop(['text', 'lemmatized_tweets', 'Lemmatized_rem_stopwords', 'score'], axis = 1)
from sklearn.model_selection import train_test_split
import re
X_train, X_test, y_train, y_test = train_test_split(x, y, 
                                                    test_size=0.30)

X_train.Lemmatized_rem_stopwords = X_train.Lemmatized_rem_stopwords.apply(lambda elem: re.sub(' +', ' ', str(elem)))
X_train.Lemmatized_rem_stopwords = X_train.Lemmatized_rem_stopwords.apply(lambda elem: elem.strip())
print(df_new.shape)
#*************************************NORMAL*************************************************

from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn import metrics
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(x, y, 
                                                    test_size=0.30)


text_clf = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', BernoulliNB()),
                     ])
text_clf.fit(X_train['Lemmatized_rem_stopwords'], y_train['label'])
predicted = text_clf.predict(X_test['text'])
print(metrics.classification_report(y_test['label'], predicted))


text_clf2 = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', MultinomialNB()),
                     ])
text_clf2.fit(X_train['Lemmatized_rem_stopwords'], y_train['label'])
predicted = text_clf2.predict(X_test['text'])
print(metrics.classification_report(y_test['label'], predicted))


from sklearn.svm import LinearSVC
text_clf1 = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', LinearSVC()),
                     ])
text_clf1.fit(X_train['Lemmatized_rem_stopwords'], y_train['label'])
predicted = text_clf1.predict(X_test['text'])
print(metrics.classification_report(y_test['label'], predicted))
#*************************************NORMAL ENDS*************************************************
#*************************************SET FORM****************************************************
import nltk
x["new"] = x.Lemmatized_rem_stopwords.apply(lambda elem: ' '.join(set(nltk.word_tokenize(elem))))
X_train, X_test, y_train, y_test = train_test_split(x, y, 
                                                    test_size=0.30)


text_clf = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', BernoulliNB()),
                     ])
text_clf.fit(X_train['new'], y_train['label'])
predicted = text_clf.predict(X_test['new'])
print(metrics.classification_report(y_test['label'], predicted))


text_clf2 = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', MultinomialNB()),
                     ])
text_clf2.fit(X_train['new'], y_train['label'])
predicted = text_clf2.predict(X_test['new'])
print(metrics.classification_report(y_test['label'], predicted))


from sklearn.svm import LinearSVC
text_clf1 = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', LinearSVC()),
                     ])
text_clf1.fit(X_train['new'], y_train['label'])
predicted = text_clf1.predict(X_test['new'])
print(metrics.classification_report(y_test['label'], predicted))
#*************************************SET FORM ENDS***********************************************
#*************************************SCORE INCLUDED**********************************************

def add_feature(X, feature_to_add):
    """
    Returns sparse feature matrix with added feature.
    feature_to_add can also be a list of features.
    """
    from scipy.sparse import csr_matrix, hstack
    return hstack([X, csr_matrix(feature_to_add).T], 'csr')

vect = CountVectorizer(min_df=5, ngram_range=(1,2)).fit(X_train["new"])
X_train_vectorized = vect.transform(X_train["new"])
model = BernoulliNB()
features = add_feature(vect.transform(X_train['new']), list(X_train["score"]))


model.fit(features, y_train['label'])
predicted = model.predict(add_feature(vect.transform(X_test['new']), list(X_test["score"])))
print(metrics.classification_report(y_test['label'], predicted))


model1 = MultinomialNB()
model1.fit(features, y_train['label'])
predicted = model1.predict(add_feature(vect.transform(X_test['new']), list(X_test["score"])))
print(metrics.classification_report(y_test['label'], predicted))


model2 = LinearSVC()
model2.fit(features, y_train['label'])
predicted = model2.predict(add_feature(vect.transform(X_test['new']), list(X_test["score"])))
print(metrics.classification_report(y_test['label'], predicted))
#*************************************SCORE INCLUDED ENDS*****************************************



x = df_new.drop(['label'], axis = 1)
y = df_new.drop(['text', 'lemmatized_tweets', 'Lemmatized_rem_stopwords', 'score'], axis = 1)
X_train, X_test, y_train, y_test = train_test_split(x, y, 
                                                    test_size=0.30)

X_train.Lemmatized_rem_stopwords = X_train.Lemmatized_rem_stopwords.apply(lambda elem: re.sub(' +', ' ', str(elem)))
X_train.Lemmatized_rem_stopwords = X_train.Lemmatized_rem_stopwords.apply(lambda elem: elem.strip())


#*************************************NORMAL*************************************************

from sklearn.naive_bayes import BernoulliNB, MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn import metrics
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(x, y, 
                                                    test_size=0.30)


text_clf = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', BernoulliNB()),
                     ])
text_clf.fit(X_train['Lemmatized_rem_stopwords'], y_train['label'])
predicted = text_clf.predict(X_test['text'])
print(metrics.classification_report(y_test['label'], predicted))


text_clf2 = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', MultinomialNB()),
                     ])
text_clf2.fit(X_train['Lemmatized_rem_stopwords'], y_train['label'])
predicted = text_clf2.predict(X_test['text'])
print(metrics.classification_report(y_test['label'], predicted))


from sklearn.svm import LinearSVC
text_clf1 = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', LinearSVC()),
                     ])
text_clf1.fit(X_train['Lemmatized_rem_stopwords'], y_train['label'])
predicted = text_clf1.predict(X_test['text'])
print(metrics.classification_report(y_test['label'], predicted))
#*************************************NORMAL ENDS*************************************************
#*************************************SET FORM****************************************************
X_train, X_test, y_train, y_test = train_test_split(x, y, 
                                                    test_size=0.30)


text_clf = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', BernoulliNB()),
                     ])

text_clf.fit(X_train['new'], y_train['label'])
predicted = text_clf.predict(X_test['new'])
print(metrics.classification_report(y_test['label'], predicted))


text_clf2 = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', MultinomialNB()),
                     ])

text_clf2.fit(X_train['new'], y_train['label'])
predicted = text_clf2.predict(X_test['new'])
print(metrics.classification_report(y_test['label'], predicted))


from sklearn.svm import LinearSVC
text_clf1 = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', LinearSVC()),
                     ])

text_clf1.fit(X_train['new'], y_train['label'])
predicted = text_clf1.predict(X_test['new'])
print(metrics.classification_report(y_test['label'], predicted))
#*************************************SET FORM ENDS***********************************************
#*************************************SCORE INCLUDED**********************************************

def add_feature(X, feature_to_add):
    """
    Returns sparse feature matrix with added feature.
    feature_to_add can also be a list of features.
    """
    from scipy.sparse import csr_matrix, hstack
    return hstack([X, csr_matrix(feature_to_add).T], 'csr')

vect = CountVectorizer(min_df=5, ngram_range=(1,2)).fit(X_train["new"])
X_train_vectorized = vect.transform(X_train["new"])
model = BernoulliNB()
features = add_feature(vect.transform(X_train['new']), list(X_train["score"]))


model.fit(features, y_train['label'])
predicted = model.predict(add_feature(vect.transform(X_test['new']), list(X_test["score"])))
print(metrics.classification_report(y_test['label'], predicted))


model1 = MultinomialNB()
model1.fit(features, y_train['label'])
predicted = model1.predict(add_feature(vect.transform(X_test['new']), list(X_test["score"])))
print(metrics.classification_report(y_test['label'], predicted))


model2 = LinearSVC()
model2.fit(features, y_train['label'])
predicted = model2.predict(add_feature(vect.transform(X_test['new']), list(X_test["score"])))
print(metrics.classification_report(y_test['label'], predicted))
#*************************************SCORE INCLUDED ENDS*****************************************


import numpy as np
feature_names = np.array(vect.get_feature_names())

sorted_tfidf_index = X_train_vectorized.max(0).toarray()[0].argsort()

print('Smallest tfidf:\n{}\n'.format(feature_names[sorted_tfidf_index[:10]]))
print('Largest tfidf: \n{}'.format(feature_names[sorted_tfidf_index[:-11:-1]]))

sorted_coef_index = model.coef_[0].argsort()

print('Smallest Coefs:\n{}\n'.format(feature_names[sorted_coef_index[:10]]))
print('Largest Coefs: \n{}'.format(feature_names[sorted_coef_index[:-11:-1]]))

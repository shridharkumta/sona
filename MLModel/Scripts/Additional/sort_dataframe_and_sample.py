# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 17:13:42 2020

@author: Santosh
"""

import pandas as pd
import re
df = pd.read_csv('C:/Users/Santosh/Desktop/Project_2/Data/data_cleaned.csv')
df.dropna(inplace = True)
print(df.shape)
df.drop(['Unnamed: 0'], axis = 1, inplace = True)
df_new = pd.DataFrame.drop_duplicates(df)


print(df_new.shape)
df_new = pd.DataFrame.drop_duplicates(df)
print(df_new.shape)


df_new.sort_values(['label', 'score'], ascending=[True, False], inplace = True)
print(df_new.shape)
dataframe = pd.DataFrame(columns = df_new.columns.values)
for i in df_new.label.unique():
    sub_df = df_new.loc[df.label == i]
    sub_df.sort_values(by='score', ascending = False, inplace = True)
    print(i, sub_df.head(19636).shape)
    dataframe = dataframe.append(sub_df.head(19636), ignore_index=True)
dataframe.to_csv('updated_data_1.csv', index = False)
print(dataframe.shape)
print(31274*14)
print(dataframe.head)
dataframe.to_csv('sampled_data.csv')

from nltk import word_tokenize
sum1 = 0
sum2 = 0
for i in range(dataframe.shape[0]):
    temp = dataframe.Lemmatized_rem_stopwords[i]
    sum1 = sum1 + len(set(word_tokenize(temp)))
    sum2 = sum2 + len(word_tokenize(temp))
print(sum1, sum2)

caption = pd.DataFrame(data = df[["label"]])
caption.columns = ["label"]
print(type(df['label'].value_counts()))
for i in range(14):
    #print(i*1.0)
    print(df.loc[df['label'] == (i*1.0)].shape[0])
    
print(df['label'].value_counts().tolist())

temp = df['label'].value_counts().rename_axis('values').reset_index(name='counts')
print(temp)
temp.sort_values('values', ascending = True, inplace = True)
print (temp)

df_xyz = dataframe
df = df_xyz

df = df.dropna()
x = df.drop(['text', 'lemmatized_tweets', 'score', 'label'], axis = 1)
y = df.drop(['text', 'lemmatized_tweets', 'Lemmatized_rem_stopwords', 'score'], axis = 1)


from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn import metrics
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(x, y, 
                                                    test_size=0.30)

X_train.Lemmatized_rem_stopwords = X_train.Lemmatized_rem_stopwords.apply(lambda elem: re.sub(' +', ' ', str(elem)))
X_train.Lemmatized_rem_stopwords = X_train.Lemmatized_rem_stopwords.apply(lambda elem: elem.strip())

X_test.Lemmatized_rem_stopwords = X_test.Lemmatized_rem_stopwords.apply(lambda elem: re.sub(' +', ' ', str(elem)))
X_test.Lemmatized_rem_stopwords = X_test.Lemmatized_rem_stopwords.apply(lambda elem: elem.strip())


text_clf = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', MultinomialNB()),
                     ])
print(X_train.shape, X_test.shape)
print(y_train.shape, y_test.shape)

text_clf.fit(X_train['Lemmatized_rem_stopwords'], y_train['label'])

from sklearn import model_selection
predicted = text_clf.predict(X_test['Lemmatized_rem_stopwords'])
print(metrics.classification_report(y_test['label'], predicted))
cv_results = model_selection.cross_val_predict(text_clf, X_train["Lemmatized_rem_stopwords"], y_train["label"], cv = 5)
print(cv_results)
print(metrics.classification_report(y_train['label'], cv_results))
print(cv_results.shape)


from sklearn.naive_bayes import BernoulliNB
text_clf = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', BernoulliNB()),
                     ])
print(X_train.shape, X_test.shape)
print(y_train.shape, y_test.shape)

text_clf.fit(X_train['Lemmatized_rem_stopwords'], y_train['label'])

from sklearn import model_selection
predicted = text_clf.predict(X_test['Lemmatized_rem_stopwords'])
print(metrics.classification_report(y_test['label'], predicted))
cv_results = model_selection.cross_val_predict(text_clf, X_train["Lemmatized_rem_stopwords"], y_train["label"], cv = 5)
print(cv_results)
print(metrics.classification_report(y_train['label'], cv_results))
print(cv_results.shape)


df = pd.read_csv(r'C:\Users\Santosh\Desktop\Project_2\Data\Data Based on Profiles\formatted.csv')
from nltk import word_tokenize
df.text = df.text.apply(lambda elem: " ".join([i for i in word_tokenize(str(elem)) if len(i) > 2]))
from sklearn.svm import LinearSVC
text_clf = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', MultinomialNB()),
                     ])

text_clf.fit(df.text, df.label)

import joblib
#!pip3 install google-api-python-client==1.7.3 oauth2client==4.1.2 progressbar2==3.38.0 httplib2==0.15.0
filename1 = 'Models/Text/LinearSVCUpdated1.joblib'
joblib.dump(text_clf, open(filename1, 'wb'))


filename1 = 'LinearSVCUpdated.joblib'
joblib.dump(text_clf1, open(filename1, 'wb'))

print(dataframe.shape)
print(sub_df.Lemmatized_rem_stopwords.unique())
new_dataframe = pd.read_csv('sample.csv')
print(new_dataframe.text_lemmatized_rem_stopwords.unique())

import re
new_dataframe.text_lemmatized_rem_stopwords = new_dataframe.text_lemmatized_rem_stopwords.apply(lambda elem: re.sub(' +', ' ', str(elem)))
new_dataframe.text_lemmatized_rem_stopwords = new_dataframe.text_lemmatized_rem_stopwords.apply(lambda elem: elem.strip())
print(new_dataframe.text_lemmatized_rem_stopwords.unique())

print(int(temp.iloc[0]['counts']), int(temp.iloc[0]['values']))

dictionary = {
0 : 'Advertising',
1 : 'Culture',
2 : 'Food',
3 : 'Gadgets',
4 : 'Gaming',
5 : 'Health',
6 : 'Memes',
7 : 'MentalHealth',
8 : 'Movies',
9 : 'Music',
10 : 'News',
11 : 'Sports',
12 : 'Travel',
13 : 'Politics',
}
print(dictionary.get(-1))



code = '<?php \n'
code = code + '\t$dataPoints = array(\n'
for i in range(temp.shape[0]):
    if i == temp.shape[0] - 1:
        code = code + '\t\tarray("label"=> "'+ dictionary.get(i) + '", "y"=> ' + str(int(temp.iloc[i]['counts'])) + ')\n'
    else:
        code = code + '\t\tarray("label"=> "'+ dictionary.get(i) + '", "y"=> ' + str(int(temp.iloc[i]['counts'])) + '),\n'
code = code + '\t);\n?>'
print(code)

text_file = open("Output.php", "w")
text_file.write(code)
text_file.close()

import pandas as pd
import re
dataframe = pd.read_csv('updated_data.csv')
dataframe.drop(['Unnamed: 0'], axis = 1, inplace = True)
dataframe.describe()
dataframe.columns
dataframe.Lemmatized_rem_stopwords.str.len()
import nltk
temp = dataframe.Lemmatized_rem_stopwords.apply(lambda elem: ' '.join(set(nltk.word_tokenize(elem))))
print(list(map(len,temp)))


x = pd.DataFrame({'text':temp.astype(str)})
x.text = x.text.astype(str)
x.describe()
y = dataframe.drop(['text', 'lemmatized_tweets', 'Lemmatized_rem_stopwords', 'score'], axis = 1)

from sklearn.naive_bayes import BernoulliNB
from sklearn.pipeline import Pipeline
from sklearn import metrics
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(x, y, 
                                                    test_size=0.30)


text_clf = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', BernoulliNB()),
                     ])
print(X_train.shape, X_test.shape)
print(y_train.shape, y_test.shape)

text_clf.fit(X_train['text'], y_train['label'])

from sklearn import model_selection
predicted = text_clf.predict(X_test['text'])
print(metrics.classification_report(y_test['label'], predicted))

from sklearn.svm import LinearSVC
text_clf1 = Pipeline([('vect', CountVectorizer()),
                     ('tfidf', TfidfTransformer()),
                     ('clf', LinearSVC()),
                     ])
    
def add_feature(X, feature_to_add):
    """
    Returns sparse feature matrix with added feature.
    feature_to_add can also be a list of features.
    """
    from scipy.sparse import csr_matrix, hstack
    return hstack([X, csr_matrix(feature_to_add).T], 'csr')
    
text_clf1.fit(X_train['text'], y_train['label'])

predicted = text_clf1.predict(X_test['text'])
print(metrics.classification_report(y_test['label'], predicted))


text_clf1.fit(features, y_train['label'])

from sklearn import model_selection
test_features = add_feature(vect.transform(X_test["Lemmatized_rem_stopwords"]), list(X_test["score"]))
predicted = text_clf1.predict(test_features)
print(metrics.classification_report(y_test['label'], predicted))

from nltk import pos_tag, word_tokenize
def lemmatize_text(df, textField):
    from nltk.stem import WordNetLemmatizer
    wordnet_lemmatizer = WordNetLemmatizer()
    lemmatized_text_list=[]
    
    #Iterate through the rows of the dataframe provided
    for row in range(len(df)):
        lemmatized_list = []
        
        #Extracting the text from the specified column
        text = df.loc[row][textField]
        
        #Loop through the words and lemmatize it, finally adding it to a list
        for word, tag in pos_tag(word_tokenize(text)):

            if tag.startswith("NN"):
               lemmatized_list.append(wordnet_lemmatizer.lemmatize(word, pos='n'))
            elif tag.startswith('VB'):
                lemmatized_list.append(wordnet_lemmatizer.lemmatize(word, pos='v'))
            elif tag.startswith('JJ'):
                lemmatized_list.append(wordnet_lemmatizer.lemmatize(word, pos='a'))
            else:
                lemmatized_list.append(word)
        
        #Join the words to one single sentence seperated by a white space
        lemmatized_text = " ".join(lemmatized_list)
        
        #Append all the lemmatized sentences into one array
        lemmatized_text_list.append(lemmatized_text)
        
    #Replace the column with the array of lemmatized sentences
    df[textField] = lemmatized_text_list
    
    return df

def clean(df, textField):
    import wordninja
    from nltk import pos_tag, word_tokenize
    consider = ['FW', 'JJ', 'NN', 'NNP', 'RB', 'RP', 'VB']
    from nltk.corpus import stopwords
    stopwords = stopwords.words('english')
    df[textField] = df[textField].fillna('')
    df[textField] = df[textField].str.lower()
    df[textField] = df[textField].str.replace("'s", "")
    df[textField] = df[textField].apply(lambda elem: re.sub(r"(@[A-Za-z0-9]+)|([^0-9#A-Za-z \t])|(\w+:\/\/\S+)|^rt|http.+?", "", elem))  
    df[textField] = df[textField].apply(lambda elem: " ".join(wordninja.split(elem)))
    df[textField] = df[textField].apply(lambda elem: " ".join([i[0] for i in pos_tag(word_tokenize(elem)) if len(i[0]) > 2 and  i[1] in consider and i[0] not in stopwords]))
    df = lemmatize_text(df, textField)
    df[textField] = df[textField].apply(lambda elem: " ".join([i for i in word_tokenize(elem) if len(i) > 2 and i not in stopwords]))
    df[textField] = df[textField].apply(lambda elem: re.sub(' +', ' ', elem))
    df[textField] = df[textField].str.strip()
    return df

test = pd.DataFrame({'text': ['Let me binge watch that series now', 'Get new notifications about things that interest you', 'Send few sample strings for testing', 'Can even give some effects to cards like shadow, rounded corners etc.', "A new OnePlus device with Snapdragon 765G has appeared on Geekbench, could very well be the OnePlus Z. https://t.co/gvf60l0LS0 #oneplus #oneplusz https://t.co/1py4aRrJnj", 'Protests against police brutality resume for the sixth day in D.C.', 'When it came to alerting the nation on Corona, it was RG. When it comes to sharing vision with top economists to deal with economic slump, it\'s RG. No matter the BJP\'s IT cell schemes to tarnish him 24×7 into 365 days. Someone is simply jealous. Simply insecured!']})
t1 = clean(test, 'text')
print(text_clf.predict(t1.text))
print(text_clf1.predict(t1.text))



import joblib
filename1 = 'Desktop/MNB_SHIVA.joblib'
joblib.dump(text_clf, open(filename1, 'wb'))


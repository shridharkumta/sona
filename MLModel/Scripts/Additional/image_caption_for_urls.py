import requests
import pandas as pd
import argparse
from pre_process import preProcessData
def get_results(url):
    return_string = ''
    if url == ' ':
        return return_string
    #print(type(url), url)
    r = requests.post(
        "https://api.deepai.org/api/densecap",
        data={
            'image': url
            },
        headers={'api-key': 'f334c091-c243-4285-9494-4c07844ed5ae'}
    )
    #print(r.json())
    temp = r.json().get('output', 'None')
    if temp == 'None':
        return return_string
    result = r.json()['output']
    result = result['captions']
    #print(result)
    return_string = ''
    for i in range(len(result)):
        caption = result[i]
        return_string = return_string + caption['caption'] + '. '
        #print(caption['caption'])
    return return_string

def Caption_Images(data):
    print('Started image captioning')
    array = data["image_url"].tolist()
    captions = []
    count = 0
    for i in array:
        captions.append(get_results(i))
        count = count + 1
    data["image_captions"] = captions
    print("Captioning done")
    data = preProcessData(data, "image_captions")
    print("Pre processing of captions done")
    return data
        
if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("-n", "--filename", required=True,
    	help="Name of the file where images needs to be captioned")
    args = vars(ap.parse_args())
    
    #Get the user id from the terminal
    filename = args["filename"]
    #filename = 'ishridharhegde_twitter'
    #filename = 'ishridharhegde_twitter'
    data = pd.read_csv(filename + '.csv', sep = ',')
    array = data["image_url"].tolist()
    captions = []
    for i in array:
        captions.append(get_results(i))
    data["image_captions"] = captions
    data = preProcessData(data, "image_captions")
    data.to_csv(filename + '_captioned.csv', sep=',')